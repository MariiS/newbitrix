<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
CModule::IncludeModule('concept.kraken');

global $KRAKEN_TEMPLATE_ARRAY;


$left = false;
$right = false;
	

if(isset($arResult["PROPERTIES"]["TARIFF_DETAIL_TEXT"]["VALUE"]["TEXT"]) || !empty($arResult["PROPERTIES"]["TARIFF_PRICES"]["VALUE"]) || !empty($arResult["PROPERTIES"]["TARIFF_GALLERY"]["VALUE"]))
	$left = true;


if(!empty($arResult["PROPERTIES"]["TARIFF_INCLUDE"]["VALUE"]) || !empty($arResult["PROPERTIES"]["TARIFF_NOT_INCLUDE"]["VALUE"]) || strlen($arResult["PROPERTIES"]["TARIFF_PRICE"]["VALUE"]) > 0 || strlen($arResult["PROPERTIES"]["TARIFF_OLD_PRICE"]["VALUE"]) > 0 || strlen($arResult["PROPERTIES"]["TARIFF_BUTTON_NAME"]["VALUE"]) > 0)
	$right = true;
?>

<div class="tariff-detail-block <?=($left)?"isset-left":"no-isset-left"?>">
	<div class="tariff-detail-block-inner">
	
		<div class="header">

			<?if(isset($arResult["PICTURE_SRC"])):?>

				<div class="header-col wr-image">
					<div class="wr-image-inner">
						<img src="<?=$arResult["PICTURE_SRC"]?>" alt="<?=$arResult["PICTURE_ALT"]?>">
						<?if($arResult["PROPERTIES"]["TARIFF_HIT"]["VALUE"] =="Y"):?><span class="star"></span><?endif;?>
					</div>
				</div>

			<?endif;?>


			<?if(isset($arResult["PROPERTIES"]["TARIFF_NAME"]["VALUE"]{0})
				|| isset($arResult["PROPERTIES"]["TARIFF_PREVIEW_TEXT"]["VALUE"]{0})
				|| isset($arResult["PROPERTIES"]["SERVICE_PRICE"]["VALUE"]{0})

			):?>

				<div class="header-col wr-text">

					<?if(isset($arResult["PROPERTIES"]["TARIFF_NAME"]["VALUE"]{0})):?>

			            <div class="name main1">
			                <?=str_replace(array("<br>","<br/>","<br />"), array(" ", " ", " "), $arResult["PROPERTIES"]["TARIFF_NAME"]["~VALUE"])?>
			            </div>

					<?endif?>

					<?if(isset($arResult["PROPERTIES"]["TARIFF_PREVIEW_TEXT"]["VALUE"]{0})):?>

			            <div class="text">
			                <?=$arResult["PROPERTIES"]["TARIFF_PREVIEW_TEXT"]["~VALUE"]?>
			            </div>

					<?endif?>
				</div>

			<?endif;?>
			
		</div>

		<div class="body">

			<?if($left):?>
			
				<div class="body-col left <?if($right):?>col-sm-8 col-xs-12<?else:?>col-xs-12<?endif;?>">
					
					<?if(isset($arResult["PROPERTIES"]["TARIFF_DETAIL_TEXT"]["VALUE"]["TEXT"])):?>
						<div class="text-content">
							<?=$arResult["PROPERTIES"]["TARIFF_DETAIL_TEXT"]["~VALUE"]["TEXT"]?>
						</div>
					<?endif;?>

					<?if(!empty($arResult["PROPERTIES"]["TARIFF_PRICES"]["VALUE"])):?>
		                
	                    <div class="wr-list-char">
	                    
	                        <?if(strlen($arResult["PROPERTIES"]["TARIFF_PRICES_TITLE"]["VALUE"]) > 0):?>
	                            <div class="section-title main1"><?=$arResult["PROPERTIES"]["TARIFF_PRICES_TITLE"]["~VALUE"]?></div>
	                        <?endif;?>


	                        <ul class="list-char">
	                            
	                            <?foreach($arResult["PROPERTIES"]["TARIFF_PRICES"]["~VALUE"] as $k=>$val):?>
	                                <li class="clearfix">
	                                
	                                    <table class="mobile-break">
	                                        <tr>
	                                            <td class="left">
	                                                <div><?=$val?></div>
	                                            </td>
	                                            
	                                            <td class="dotted">
	                                                <div></div>
	                                            </td>
	                                            
	                                            <td class="right">
	                                                <div class="main1"><?=$arResult["PROPERTIES"]["TARIFF_PRICES"]["~DESCRIPTION"][$k]?></div>
	                                            </td>
	                                        </tr>
	                                    </table>
	                                
	                                </li>
	                            <?endforeach;?>

	                        </ul>
	                    </div>
	                
	                <?endif;?>


	                <?if(isset($arResult["GALLERY"])):?>

	                	<?
	                		$colls_gal = "col-lg-3 col-md-3 col-sm-3 col-xs-4";
	                        if(!$right)
	                        	$colls_gal = "col-lg-2 col-md-2 col-sm-2 col-xs-4";
	                	?>

						<?if(isset($arResult["PROPERTIES"]["TARIFF_GALLERY_TITLE"]["VALUE"]{0})):?>
				            <div class="section-title main1">
				                <?=$arResult["PROPERTIES"]["TARIFF_GALLERY_TITLE"]["~VALUE"]?>
				            </div>
			            <?endif;?>



						<div class="gallery <?if($arResult["PROPERTIES"]["TARIFF_GALLERY_BORDER"]["VALUE"] == "Y"):?>border-on<?endif;?>">


				            <div class="row clearfix">

								<?foreach($arResult["GALLERY"] as $key => $arItem):?>

									<div class="<?=$colls_gal?>">
					                	<div class="wr-img">

					                    	<a data-gallery="service-detail-<?=$arResult["ID"]?>" href="<?=$arItem["BIG_SRC"]?>" title="<?=$arItem["TITLE"]?>" class="cursor-loop">

			                        		<img class="img-responsive" src="<?=$arItem["SMALL_SRC"]?>" alt="<?=$arItem["ALT"]?>"/></a>
					                    </div>
					                </div>


					                <?
					                	if(!$right)
						                {
						                	if(($k+1) % 6 == 0)
						                		echo "<span class='clearfix hidden-xs'></span>";

						                	if(($k+1) % 3 == 0)
						                		echo "<span class='clearfix visible-xs'></span>";

						                }
						                else
						                {
						                	if(($k+1) % 4 == 0)
						                		echo "<span class='clearfix hidden-xs'></span>";

						                	if(($k+1) % 3 == 0)
						                		echo "<span class='clearfix visible-xs'></span>";
						                }
					                ?>

								<?endforeach;?>

							</div>

						</div>

					<?endif;?>

					
				</div>

			<?endif;?>

			<?if($right):?>

				<div class="body-col right <?=($left)?"col-md-4 col-sm-4":""?> col-xs-12">
					<?if(!empty($arResult["PROPERTIES"]["TARIFF_INCLUDE"]["VALUE"]) || !empty($arResult["PROPERTIES"]["TARIFF_NOT_INCLUDE"]["VALUE"])):?>
			                                                    
	                    <ul class='adv-plus-minus'>
	                        
	                        <?if(is_array($arResult["PROPERTIES"]["TARIFF_INCLUDE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["TARIFF_INCLUDE"]["VALUE"])):?>
	                            
	                            <?foreach($arResult["PROPERTIES"]["TARIFF_INCLUDE"]["~VALUE"] as $val):?>
	                                <li class="point-green"><?=$val?></li>
	                            <?endforeach;?>
	                            
	                        <?endif;?>
	                        
	                        <?if(is_array($arResult["PROPERTIES"]["TARIFF_NOT_INCLUDE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["TARIFF_NOT_INCLUDE"]["VALUE"])):?>
	                            
	                            <?foreach($arResult["PROPERTIES"]["TARIFF_NOT_INCLUDE"]["~VALUE"] as $val):?>
	                                <li><?=$val?></li>
	                            <?endforeach;?>
	                            
	                        <?endif;?>
	                        
	                    </ul>
	                
	                <?endif;?>

	                <?if(strlen($arResult["PROPERTIES"]["TARIFF_PRICE"]["VALUE"]) > 0 || strlen($arResult["PROPERTIES"]["TARIFF_OLD_PRICE"]["VALUE"]) > 0):?>


			        	<div class="price-wrap">

			        		<?if(strlen($arResult["PROPERTIES"]["TARIFF_OLD_PRICE"]["VALUE"]) > 0):?>
				            	<div class="old-price main2"><?=$arResult["PROPERTIES"]["TARIFF_OLD_PRICE"]["~VALUE"]?></div>
				            <?endif?>

				        	<?if(strlen($arResult["PROPERTIES"]["TARIFF_PRICE"]["VALUE"]) > 0):?>


				            	<div class="price main1"><?=$arResult["PROPERTIES"]["TARIFF_PRICE"]["~VALUE"]?></div>

				            <?endif?>

				        </div>

			        <?endif?>


		        	<?if(strlen($arResult["PROPERTIES"]["TARIFF_BUTTON_NAME"]["VALUE"]) > 0):?>

						<?if(strlen($arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"]) <= 0):?>
		                    <?$arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"] = "form";?>
		                <?endif;?>

				
				        <div class="button-wrap">

				        	<?
							    $arClass = array();
							    $arClass=array(
							        "XML_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"],
							        "FORM_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_FORM"]["VALUE"],
							        "MODAL_ID"=> $arResult["PROPERTIES"]["TARIFF_MODAL"]["VALUE"],
							        "QUIZ_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_QUIZ"]["VALUE"]
							    );
							    
							    $arAttr=array();
							    $arAttr=array(
							        "XML_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"],
							        "FORM_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_FORM"]["VALUE"],
							        "MODAL_ID"=> $arResult["PROPERTIES"]["TARIFF_MODAL"]["VALUE"],
							        "LINK"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_LINK"]["VALUE"],
							        "BLANK"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_BLANK"]["VALUE_XML_ID"],
							        "HEADER"=> $block_name,
							        "QUIZ_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_QUIZ"]["VALUE"],
							        "LAND_ID"=> $arResult["PROPERTIES"]["TARIFF_BUTTON_LAND"]["VALUE"]
							    );

							    $b_options = array(
	                                "MAIN_COLOR" => "main-color",
	                                "STYLE" => ""
	                            );

	                            if(strlen($arResult["PROPERTIES"]["TARIFF_BUTTON_BG_COLOR"]["VALUE"]))
	                            {

	                                $b_options = array(
	                                    "MAIN_COLOR" => "btn-bgcolor-custom",
	                                    "STYLE" => "background-color: ".$arResult["PROPERTIES"]["TARIFF_BUTTON_BG_COLOR"]["VALUE"].";"
	                                );

	                            }
							?>
				        	

				            <a class="button-def <?=$KRAKEN_TEMPLATE_ARRAY["BTN_VIEW"]['VALUE'];?> <?=$b_options["MAIN_COLOR"]?> big from-modal from-tariff more-modal-info <?=CKraken::buttonEditClass ($arClass)?>" data-element-id="<?=$arResult["ID"]?>" data-element-type = "TRF"

				            	<?if(strlen($b_options["STYLE"])):?>
	                                style = "<?=$b_options["STYLE"]?>"
	                            <?endif;?>

				            	<?=CKraken::buttonEditAttr($arAttr)?> title='<?=$arResult["PROPERTIES"]["TARIFF_BUTTON_NAME"]["VALUE"]?>'>

				            	<?if($arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"] == "add_to_cart"):?>

								    <?
								    
								        $btn_name2 = GetMessage("KRAKEN_TEMPLATES_NEWS_DETAIL_TARIFF_BTN_ADDED_NAME");

								        if(strlen($KRAKEN_TEMPLATE_ARRAY["CART_BTN_ADDED_NAME"]["~VALUE"]) > 0)
								            $btn_name2 = $KRAKEN_TEMPLATE_ARRAY["CART_BTN_ADDED_NAME"]["~VALUE"];
								    ?>

								    <span class="first">
								       <?=$arResult["PROPERTIES"]["TARIFF_BUTTON_NAME"]["~VALUE"]?>
								    </span>

								    <span class="second">
								        <?=$btn_name2?>
								    </span> 

								<?else:?>

				            		<?=$arResult["PROPERTIES"]["TARIFF_BUTTON_NAME"]["~VALUE"]?>

				            	<?endif;?>
				            		
				            </a>

			
				        </div>
				 

			        <?endif?>
	                
	                <?if(!empty($arResult["PROPERTIES"]["TARIFF_COMMENT"]["VALUE"])):?>
	                        
	                    <div class="tariff-comment"><?=$arResult["PROPERTIES"]["TARIFF_COMMENT"]["~VALUE"]["TEXT"]?></div>
	                 
	                <?endif;?>
				</div>

			<?endif;?>

			<div class="clearfix"></div>

		</div>

	</div>
</div>