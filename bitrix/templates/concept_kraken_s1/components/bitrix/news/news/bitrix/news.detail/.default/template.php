<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
global $KRAKEN_TEMPLATE_ARRAY;
CKraken::includeCustomMessages();
?>
<?$show_setting = $KRAKEN_TEMPLATE_ARRAY["MODE_FAST_EDIT"]['VALUE'][0];?>
<?$admin_active = ($USER->isAdmin() || $APPLICATION->GetGroupRight("concept.kraken") > "R");?>


<?
    $block_name = $arResult['~NAME'];

    if(strlen($arItem["PROPERTIES"]["HEADER"]["VALUE"]) > 0)
        $block_name .= " (".$arResult["PROPERTIES"]["HEADER"]["~VALUE"].")";

    $block_name = htmlspecialcharsEx(strip_tags(html_entity_decode($block_name)));
?>


    <div class="new-detail">

     

        <div class="top-info">

            <div class="row clearfix"> 

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                	<?$frame = $this->createFrame()->begin("");?>

	                    <div class="date">
	                        

	                            <?if($arResult["IBLOCK_CODE"] == "concept_kraken_site_action_".SITE_ID):?>
	                                                                                
	                                <?if(getmicrotime() > MakeTimeStamp($arResult["ACTIVE_TO"]) && strlen($arResult["ACTIVE_TO"])>0):?>

	                                    <span class="off"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_ACT_OFF")?></span>

	                                <?else:?>

	                                    <?if(strlen($arResult["ACTIVE_TO"])>0):?>

	                                        <span class="to"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_ACT_ON_TO")?><?echo CIBlockFormatProperties::DateFormat($KRAKEN_TEMPLATE_ARRAY['DATE_FORMAT']['VALUE'], MakeTimeStamp($arResult["ACTIVE_TO"], CSite::GetDateFormat()));?></span>

	                                    <?else:?>

	                                        <span><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_ACT_ON")?></span>

	                                    <?endif;?>

	                                <?endif;?>

	                            <?else:?>

	                                <?if(strlen($arResult["ACTIVE_FROM"])>0):?>
	                                    <?echo CIBlockFormatProperties::DateFormat($KRAKEN_TEMPLATE_ARRAY['DATE_FORMAT']['VALUE'], MakeTimeStamp($arResult["ACTIVE_FROM"], CSite::GetDateFormat()));?>
	                                <?endif;?>

	                            <?endif;?>

	                        

	                    </div>

	                    <div class="count_views">

	                        <?=$arResult["SHOW_COUNTER"]?>
	                        
	                    </div> 
                    <?$frame->end();?>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?=CKraken::shares(
                        array(
                            "VK" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                                "IMG" => $arResult["SHARE_IMG"],
                                "DESCRIPTION" => $arResult["SHARE_DESCRIPTION"],
                            ),
                            "FB" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "IMG" => $arResult["SHARE_IMG"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                                "DESCRIPTION" => $arResult["SHARE_DESCRIPTION"],
                            ),
                            "TW" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                            ),
                            "OK" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                            ),
                            "MAILRU" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                            ),
                            "WTSAPP" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                            ),
                            "TELEGRAM" => array(
                                "URL" => $arResult["SHARE_URL"],
                                "TITLE" => $arResult["SHARE_TITLE"],
                            ),
                            "SKYPE" => array(
                                "URL" => $arResult["SHARE_URL"],
                            ),
                            "GPLUS" => array(
                                "URL" => $arResult["SHARE_URL"],
                            ),
                        ),
                        "view-1"
                    )?>
                </div>

            </div>
        </div>

        <div class="new-detail-content  text-content">
          

        	<?if(strlen($arResult["~DETAIL_TEXT"])>0):?>

        		<?=$arResult["~DETAIL_TEXT"]?>

        	<?endif;?>
            
        </div>

        <?if(strlen($arResult["PROPERTIES"]["BUTTON_TYPE"]["VALUE_XML_ID"]) <= 0):?>
                <?$arResult["PROPERTIES"]["BUTTON_TYPE"]["VALUE_XML_ID"] = "form";?>
        <?endif;?>
            
        <?if(strlen($arResult["PROPERTIES"]["BUTTON_NAME"]["VALUE"]) > 0 && strlen($arResult["PROPERTIES"]["BUTTON_TYPE"]["VALUE_XML_ID"]) > 0):?>

            <div class="main-button-wrap">

                <?
                    if($arResult["PROPERTIES"]["BUTTON_FORM"]["VALUE"] > 0)
                        $form_id = $arResult["PROPERTIES"]["BUTTON_FORM"]["VALUE"];

                    if($arResult["PROPERTIES"]["BUTTON_TYPE"]["VALUE_XML_ID"] == "fast_order")
                    {
                        $form_id = $KRAKEN_TEMPLATE_ARRAY['FORMS']['VALUE_CATALOG'];

                        if($arResult["PROPERTIES"]["BUTTON_FORM"]["VALUE"] > 0)
                            $form_id = $arResult["PROPERTIES"]["BUTTON_FORM"]["VALUE"];
                    }

                    $arClass = array();
                    $arClass=array(
                        "XML_ID"=> $arResult["PROPERTIES"]["BUTTON_TYPE"]["VALUE_XML_ID"],
                        "FORM_ID"=> $form_id,
                        "MODAL_ID"=> $arResult["PROPERTIES"]["BUTTON_MODAL"]["VALUE"],
                        "QUIZ_ID"=> $arResult["PROPERTIES"]["BUTTON_QUIZ"]["VALUE"],
                    );
                    
                    $arAttr=array();
                    $arAttr=array(
                        "XML_ID"=> $arResult["PROPERTIES"]["BUTTON_TYPE"]["VALUE_XML_ID"],
                        "FORM_ID"=> $form_id,
                        "MODAL_ID"=> $arResult["PROPERTIES"]["BUTTON_MODAL"]["VALUE"],
                        "LINK"=> $arResult["PROPERTIES"]["BUTTON_LINK"]["VALUE"],
                        "BLANK"=> $arResult["PROPERTIES"]["BUTTON_BLANK"]["VALUE_XML_ID"],
                        "HEADER"=> $block_name,
                        "QUIZ_ID"=> $arResult["PROPERTIES"]["BUTTON_QUIZ"]["VALUE"],
                        "LAND_ID"=> $arResult["PROPERTIES"]["BUTTON_LAND"]["VALUE"]
                    );
                ?>

                <a
                <?
	                if(strlen($arResult["PROPERTIES"]["BUTTON_ONCLICK"]["VALUE"])>0) 
	                {
	                	$str_onclick = str_replace("'", "\"", $arResult["PROPERTIES"]["BUTTON_ONCLICK"]["VALUE"]);

	                	echo "onclick='".$str_onclick."'";
	                }
                ;?> class="big button-def <?=$KRAKEN_TEMPLATE_ARRAY["BTN_VIEW"]['VALUE'];?> <?=CKraken::buttonEditClass($arClass)?> <?if($arResult["PROPERTIES"]["BUTTON_VIEW"]["VALUE_XML_ID"] == "empty"):?> secondary <?elseif($arResult["PROPERTIES"]["BUTTON_VIEW"]["VALUE_XML_ID"] == "shine"):?> shine main-color <?else:?> main-color <?endif;?>" <?=CKraken::buttonEditAttr($arAttr)?>><?=$arResult["PROPERTIES"]["BUTTON_NAME"]["~VALUE"]?></a>
                        
            </div>

        <?endif;?>

    </div>


    <?if(isset($arResult["FILES"])):?>

        <?if(isset($arResult["PROPERTIES"]["TITLE_BLOCK_FILES"]["VALUE"]{0})):?>   
            <div class="text-content">      
                <h2><?=$arResult["PROPERTIES"]["TITLE_BLOCK_FILES"]["~VALUE"]?></h2>
            </div>   
        <?endif;?>
        
        <div class="files-list">
            <div class="row">

                <?foreach ($arResult["FILES"] as $arFile):?>
                    <div class="col-md-3 col-6">
                    
                        <div class="item">

                            <a target="_blank" href="<?=$arFile['PATH']?>">
                                <div class="row">

                                    <div class="col-sm-3 col-xs-2 wr-icon">
                                        <div class="icon"></div>
                                    </div>

                                    <?if(isset($arFile['DESC']{0})):?>

                                        <div class="col-sm-9 col-xs-10 wr-desc">
                                            <div class="desc">
                                                <?=$arFile['DESC']?>
                                            </div>

                                            <?if(isset($arFile['SUB_DESC']{0})):?>

                                                <div class="subdesc">
                                                    <?=$arFile['SUB_DESC']?>
                                                </div>

                                            <?endif;?>
                                            
                                        </div>
                                    <?endif;?>
                                </div>

                            </a>
                            
                        </div>

                    </div>

                <?endforeach;?>


            </div>
        </div>

    <?endif;?>



    <?if(isset($arResult["GALLERY"])):?>
        

        <?if(strlen($arResult["PROPERTIES"]["NEWS_GALLERY_TITLE"]["~VALUE"])>0):?>   
            <div class="text-content">      
                <h2><?=$arResult["PROPERTIES"]["NEWS_GALLERY_TITLE"]["~VALUE"]?></h2>
            </div>   
        <?endif;?>

        
        <div class="gallery-block<?if($arResult["PROPERTIES"]["NEWS_GALLERY_BORDER"]["VALUE"] == "Y"):?> border-img-on<?endif;?>">

            <div class="row clearfix">
                
                <?foreach($arResult["GALLERY"] as $k => $arImage):?>


                    <div class="col-sm-3 col-xs-4 middle">
                        <div class="gallery-img">

                            <a data-gallery="detnews<?=$arResult["ID"]?>" href="<?=$arImage["SRC_LG"]?>" title="<?=CKraken::prepareText($arImage["DESC"])?>" class="cursor-loop">

                                
                                <div class="corner-line"></div>
                                

                            <img class="img-responsive lazyload" data-src="<?=$arImage["SRC_XS"]?>" alt=""/></a>
                        </div>
                    </div>

                    <?if(($k+1) % 4 == 0):?>
                        <span class="clearfix hidden-xs"></span>
                    <?endif;?>
                    <?if(($k+1) % 3 == 0):?>
                        <span class="clearfix visible-xs"></span>
                    <?endif;?>


                <?endforeach;?>

            </div>
                        
                            
        </div>
        
    <?endif;?>



    <?if(!empty($arResult["ELEMENTS"])):?>

        <?if(strlen($arResult["PROPERTIES"]["NEWS_TITLE_NBA"]["~VALUE"])>0):?>
            <div class="text-content">
                <h2><?=$arResult["PROPERTIES"]["NEWS_TITLE_NBA"]["~VALUE"]?></h2>
            </div>
        <?endif;?>

        <div class="news flat">
            
            <div class="row">

                <div class="wrap-elements">  
                    <?$class = "col-md-4 col-sm-6 col-xs-12";?>
                            
                    <?foreach($arResult["ELEMENTS"] as $k=>$arNews):?>

                        <div class="<?=$class?>">
                            <div class="wrap-element">

                                <div class="element">
                                
                                    <?CKraken::admin_setting($arNews, false, $admin_active, $show_setting)?>

                                    <?/*<a href='<?=$arNews["DETAIL_PAGE_URL"]?>' class="wrap-link"></a>*/?>
                                 

                                    <table>
                                        <tr>
                                            <td>
                                                <?$img["src"] = "";?>

                                                <?if($arNews["PREVIEW_PICTURE"]>0):?>
                                                    <?$img = CFile::ResizeImageGet($arNews["PREVIEW_PICTURE"], array('width'=>600, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, false);?>
                                                <?endif;?>

                                                <a href='<?=$arNews["DETAIL_PAGE_URL"]?>' class='hover_shine img-wrap'>
                                                    <div class='bg-img lazyload' <?if($arNews["PREVIEW_PICTURE"]>0):?>data-src="<?=$img["src"]?>"<?endif;?>>

                                                        <div class="new-dark-shadow"></div>
                                                  
                                                    </div>
                                                    <div class="shine"></div>
                                                </a>
                                         
                                            </td>
                                        </tr>
                                    </table>


                                    <div class="wrap-text">
                                        

                                        <?if($arNews["IBLOCK_CODE"] != "concept_kraken_site_action_".SITE_ID && $arResult["PARENT_ON"] == "Y"):?>
                                            <div class="section" title='<?=CKraken::prepareText($arNews['SECTION_NAME'])?>'>

                                                <?

                                                    $name = "";
                                                    $link_news = "";

                                                    if(strlen($arResult['BNA'][$arNews['IBLOCK_SECTION_ID']]['NAME'])>0)
                                                    {
                                                        
                                                        $name = $arResult['BNA'][$arNews['IBLOCK_SECTION_ID']]['~NAME'];
                                                        $link_news = $arResult['BNA'][$arNews['IBLOCK_SECTION_ID']]['SECTION_PAGE_URL'];
                                                    }
                                                    else
                                                    {
                                                        
                                                        $name_def = "NEWS";
                                                        $link_news = SITE_DIR."news/";
                                                        
                                                        if($arNews["IBLOCK_CODE"] == "concept_kraken_site_history_".SITE_ID)
                                                        {
                                                            $name_def = "BLOG";
                                                            $link_news = SITE_DIR."blog/";
                                                        }

                                                        $name = GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_DEF_".$name_def);

                                                    }

                                                ?>

                                                <a href='<?=$link_news?>' class="wrap-link-sect"><?=$name?></a>
                                            </div>

                                        
                                        <?endif;?>
                                

                                        <?if($arNews["IBLOCK_CODE"] == "concept_kraken_site_action_".SITE_ID):?>

                                            <div class="date-action">

                                                <?$frame = $this->createFrame()->begin("");?>
                                                
                                                    <?if(getmicrotime() > MakeTimeStamp($arNews["ACTIVE_TO"]) && strlen($arNews["ACTIVE_TO"])>0):?>

                                                        <span class="off"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_ACT_OFF")?></span>

                                                    <?else:?>

                                                        <?if(strlen($arNews["ACTIVE_TO"])>0):?>

                                                            <span class="to"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_ACT_ON_TO")?><?echo CIBlockFormatProperties::DateFormat($KRAKEN_TEMPLATE_ARRAY['DATE_FORMAT']['VALUE'], MakeTimeStamp($arNews["ACTIVE_TO"], CSite::GetDateFormat()));?></span>

                                                        <?else:?>

                                                            <span><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_DETAIL_ACT_ON")?></span>

                                                        <?endif;?>

                                                    <?endif;?>

                                                <?$frame->end();?>

                                            </div>

                                        <?endif;?>

                                        <a href='<?=$arNews["DETAIL_PAGE_URL"]?>'>
                                            <div class="new-name bold"><?=$arNews['~NAME']?></div>
                                        </a>


                                        
                                        <?if(strlen($arNews["ACTIVE_FROM"]) > 0):?>
                                   
                                        
                                            <div class="date">
                                                
                                                <?if($arNews["IBLOCK_CODE"] != "concept_kraken_site_action_".SITE_ID):?>

                                                    <?if(strlen($arNews["ACTIVE_FROM"])>0):?>
                                                        <?echo CIBlockFormatProperties::DateFormat($KRAKEN_TEMPLATE_ARRAY['DATE_FORMAT']['VALUE'], MakeTimeStamp($arNews["ACTIVE_FROM"], CSite::GetDateFormat()));?>
                                                    <?endif;?>

                                                <?endif;?>
                                            </div>
                                      
                                            
                                        <?endif;?>
                                            
                                  


                                        <?if(strlen($arNews["PROPERTIES"]["NEWS_DETAIL_TEXT"]["~VALUE"]['TEXT'])>0):?>
                                            <a href='<?=$arNews["DETAIL_PAGE_URL"]?>'>
                                                <div class="new-text"><?=$arNews["PROPERTIES"]["NEWS_DETAIL_TEXT"]["~VALUE"]['TEXT']?></div>
                                            </a>
                                        <?endif;?>

                                    </div>

                                </div>

                                <div class="new-shadow"></div>

                            </div>

                       </div>

            

                        <?if(($k+1) % 3 == 0):?>
                            <span class="clearfix hidden-sm"></span>
                        <?endif;?>

                        <?if(($k+1) % 2 == 0):?>
                            <span class="clearfix visible-sm"></span>
                        <?endif;?>




                       

                    <?endforeach;?>

                                
                </div> 
            
            </div>

        </div>

    <?endif;?>
