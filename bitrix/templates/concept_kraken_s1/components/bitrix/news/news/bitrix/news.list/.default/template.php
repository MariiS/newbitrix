<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
global $KRAKEN_TEMPLATE_ARRAY;
CKraken::includeCustomMessages();
?>
<?$show_setting = $KRAKEN_TEMPLATE_ARRAY["MODE_FAST_EDIT"]['VALUE'][0];?>
<?$admin_active = ($USER->isAdmin() || $APPLICATION->GetGroupRight("concept.kraken") > "R");?>


<div class="news flat">
                
    <div class="row">

        <div class="wrap-elements">  

        	<?
                $class = "col-md-3 col-sm-6 col-xs-12";

                if($arParams["MENU_ON"] == "Y")
                    $class = "col-md-4 col-sm-6 col-xs-12";

                $i=0;
            ?> 



			<?foreach($arResult["ITEMS"] as $k => $arItem):?>

				<div class="<?=$class?>">
                    <div class="wrap-element">

                        <div class="element">

                            <?CKraken::admin_setting($arItem, false, $admin_active, $show_setting)?>
                        
                            <?/*<a href='<?=$arItem["DETAIL_PAGE_URL"]?>' class="wrap-link"></a>*/?>
                         
                            <table>
                                <tr>
                                    <td>

                                        <?$img["src"] = "";?>

                                        <?if($arItem["PREVIEW_PICTURE"]["ID"]>0):?>
                                            <?$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>600, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, false);?>
                                        <?endif;?>

                                        <a href='<?=$arItem["DETAIL_PAGE_URL"]?>' class='hover_shine img-wrap'>
                                            <div class='bg-img lazyload' <?if($arItem["PREVIEW_PICTURE"]["ID"]>0):?>data-src="<?=$img["src"]?>"<?endif;?>>

                                                <div class="new-dark-shadow"></div>
                                          
                                            </div>
                                            <div class="shine"></div>
                                        </a>
                                 
                                    </td>
                                </tr>
                            </table>


                            <div class="wrap-text">


                                <?if($arItem["IBLOCK_CODE"] != "concept_kraken_site_action_".SITE_ID && $arResult["PARENT_ON"] == "Y" && $arParams["HIDE_SECTIONS"] != "Y"):?>
                                    <div class="section" title='<?=CKraken::prepareText($arItem['SECTION_NAME'])?>'>

                                        <?

                                            $name = "";
                                            $link_news = "";

                                            if($arItem['IBLOCK_SECTION_ID'] > 0 && strlen($arResult['BNA'][$arItem['IBLOCK_SECTION_ID']]['NAME']))
                                            {
                                                
                                                $name = $arResult['BNA'][$arItem['IBLOCK_SECTION_ID']]['~NAME'];
                                                $link_news = $arResult['BNA'][$arItem['IBLOCK_SECTION_ID']]['SECTION_PAGE_URL'];
                                            }
                                            else
                                            {
                                                
                                                $name_def = "NEWS";
                                                $link_news = SITE_DIR."news/";
                                                
                                                if($arItem["IBLOCK_CODE"] == "concept_kraken_site_history_".SITE_ID)
                                                {
                                                    $name_def = "BLOG";
                                                    $link_news = SITE_DIR."blog/";
                                                }

                                                $name = GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_LIST_DEF_".$name_def);

                                            }

                                        ?>

                                        <a href='<?=$link_news?>' class="wrap-link-sect"><?=$name?></a>

                                    </div>

                                
                                <?endif;?>


                 

                                <?if($arItem["IBLOCK_CODE"] == "concept_kraken_site_action_".SITE_ID):?>

                                    <div class="date-action">

                                        <?$frame = $this->createFrame()->begin("");?>
                                        
                                            <?if(getmicrotime() > MakeTimeStamp($arItem["ACTIVE_TO"]) && strlen($arItem["ACTIVE_TO"])>0):?>

                                                <span class="off"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_LIST_ACT_OFF")?></span>

                                            <?else:?>

                                                <?if(strlen($arItem["ACTIVE_TO"])>0):?>

                                                    <span class="to"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_LIST_ACT_ON_TO")?><?echo CIBlockFormatProperties::DateFormat($KRAKEN_TEMPLATE_ARRAY['DATE_FORMAT']['VALUE'], MakeTimeStamp($arItem["ACTIVE_TO"], CSite::GetDateFormat()));?></span>

                                                <?else:?>

                                                    <span><?=GetMessage("KRAKEN_TEMPLATES_NEWS_NEWS_LIST_ACT_ON")?></span>

                                                <?endif;?>

                                            <?endif;?>

                                        <?$frame->end();?>

                                    </div>

                                <?endif;?>

                                <a href='<?=$arItem["DETAIL_PAGE_URL"]?>'>
                                    <div class="new-name bold"><?=$arItem['~NAME']?></div>
                                </a>


                                <?if($arItem["IBLOCK_CODE"] != "concept_kraken_site_action_".SITE_ID && $KRAKEN_TEMPLATE_ARRAY["ITEMS"][$arParams["TYPE"]."_HIDE_DATE"]["VALUE"]["ACTIVE"] != "Y"):?>

                                    <?if(strlen($arItem["ACTIVE_FROM"]) > 0):?>
                                    
                                        <div class="date">

                                            <?echo CIBlockFormatProperties::DateFormat($KRAKEN_TEMPLATE_ARRAY['DATE_FORMAT']['VALUE'], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));?>
                                           
                                        </div>
                                        
                                    <?endif;?>

                                <?endif;?>


                                <?if(!empty($arItem["PROPERTIES"]["NEWS_DETAIL_TEXT"]["~VALUE"])):?>
                                    <a href='<?=$arItem["DETAIL_PAGE_URL"]?>'>
                                        <div class="new-text"><?=$arItem["PROPERTIES"]["NEWS_DETAIL_TEXT"]["~VALUE"]['TEXT']?></div>
                                    </a>
                                <?endif;?>

                            </div>

                        </div>

                        <div class="new-shadow"></div>

                    </div>

               </div>

                <?if($arParams["MENU_ON"] == "Y"):?>

                    <?if(($i+1) % 3 == 0):?>
                        <span class="clearfix hidden-sm"></span>
                    <?endif;?>

                    <?if(($i+1) % 2 == 0):?>
                        <span class="clearfix visible-sm"></span>
                    <?endif;?>

                    

                <?else:?>

                    <?if(($i+1) % 4 == 0):?>
                        <span class="clearfix hidden-sm"></span>
                    <?endif;?>

                    <?if(($i+1) % 2 == 0):?>
                        <span class="clearfix visible-sm"></span>
                    <?endif;?>

                <?endif;?>

                <?$i++?>

				
			<?endforeach;?>

            <?unset($i);?>

		</div> 
    
    </div>


</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?endif;?>