<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $KRAKEN_TEMPLATE_ARRAY;
global $DB;
?>
<?$frame = $this->createFrame()->begin("");?>

<div class="other-news">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        
        <?unset($img);?>

        <?if(strlen($arItem["PREVIEW_PICTURE"]["ID"])>0):?>
            <?$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>600, 'height'=>600), BX_RESIZE_IMAGE_PROPORTIONAL, false);?>
        <?endif;?>

        <div class="item lazyload" data-src="<?=$img["src"]?>">
            <div class="frameshadow"></div>

    
            <div class="new-dark-shadow"></div>

            <div class="cont">
                <div class="name bold"><?=$arItem["~NAME"]?></div>
            </div>
            <a class="wrap-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
      
        </div>

    <?endforeach;?>
    
</div>

<!--script>
    $('.lazyload').Lazy();
</script-->

<?$frame->end();?>
