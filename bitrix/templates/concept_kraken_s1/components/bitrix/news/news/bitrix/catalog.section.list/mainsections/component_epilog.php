<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>


<?\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("ajax");

$cur_page = $_SERVER["REQUEST_URI"];
$cur_page_no_index = $APPLICATION->GetCurPage(false);
$menu = 0;
?>


<?if(!empty($arResult["MAIN"]["SECTIONS"])):?>

    <script type="text/javascript">
    
        <?foreach($arResult["MAIN"]["SECTIONS"] as $arSection):?>


        
            <?if(CMenu::IsItemSelected($arSection["SECTION_PAGE_URL"], $cur_page, $cur_page_no_index)):?>
                $("div#navigation.menu-navigation li[data-id='<?=$arSection["ID"]?>']").addClass("active");
                <?$menu = 1;?>
            <?endif;?>
            
        <?endforeach;?>

        <?if($menu < 1):?>
    		$("div#navigation.menu-navigation li.main").addClass("active");
        <?endif;?>
        
    </script>

<?endif;?>


<?\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("ajax");?>