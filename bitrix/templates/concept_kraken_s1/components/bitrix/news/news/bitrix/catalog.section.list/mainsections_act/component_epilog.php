<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
CKraken::includeCustomMessages();

$arPage["ACT"] = SITE_DIR."action/";

$arResult["SECTIONS"] = array();

$on = false;
$off = false;

$arSelect = Array("ID");
$arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "ACTIVE_DATE" => "Y", "<DATE_ACTIVE_FROM" => ConvertTimeStamp(time(),"FULL"));
$arFilter2 = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y","ACTIVE_DATE"=>"", "<=DATE_ACTIVE_TO" => ConvertTimeStamp(time(),"FULL"));
$res1 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, false, $arSelect);

while($ob1 = $res1->GetNextElement())
{
    if($on)
        break;

    $on = true;
    

    $arResult["SECTIONS"][] = array("NAME" => GetMessage("KRAKEN_TEMPLATES_NEWS_CATALOG_SECTION_LIST_MAINSECTIONS_ACT_ACTION_ON"),
        "SECTION_PAGE_URL" => $arPage["ACT"]."?action=on",
        "ID" => "action_on");
}


while($ob2 = $res2->GetNextElement())
{
    if($off)
        break;

    $off = true;

    
    $arResult["SECTIONS"][] = array("NAME" => GetMessage("KRAKEN_TEMPLATES_NEWS_CATALOG_SECTION_LIST_MAINSECTIONS_ACT_ACTION_OFF"),
        "SECTION_PAGE_URL" => $arPage["ACT"]."?action=off",
        "ID" => "action_off");

}


if(strlen($arResult["SECTION"]["SECTION_PAGE_URL"]) > 0)
{
    $arResult["SECTION_BACK"] = $arResult["SECTION"]["SECTION_PAGE_URL"];
}
else
    $arResult["SECTION_BACK"] = $arPage[$arParams["TYPE"]];

$arResult["SECTION_MAIN"] = $arPage[$arParams["TYPE"]];

?>
<?\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("ajax");?>
<?if($arParams["ELEMENT"] != "Y"):?>
    <ul class="nav">
        <li class="main">
            <a href="<?=$arResult["SECTION_MAIN"]?>"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_CATALOG_SECTION_LIST_MAINSECTIONS_ACT_BACK")?></a>
        </li> 

        <?foreach($arResult["SECTIONS"] as $arSection):?>
           
        
            <li data-id="<?=$arSection["ID"]?>">
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
            </li>
        
        <?endforeach;?>
        
    </ul>

<?else:?>

    <ul class="new-detail">

        <li class="back"><a href="<?=$arResult["SECTION_BACK"]?>"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_CATALOG_SECTION_LIST_MAINSECTIONS_ACT_SECTION_BACK");?></a></li>

    </ul>

<?endif;?>



<?
$cur_page = $_SERVER["REQUEST_URI"];
$cur_page_no_index = $APPLICATION->GetCurPage(false);
$menu = 0;
?>




<script type="text/javascript">
    <?if(!empty($arResult["SECTIONS"])):?>
        <?foreach($arResult["SECTIONS"] as $arSection):?>


        
            <?if(CMenu::IsItemSelected($arSection["SECTION_PAGE_URL"], $cur_page, $cur_page_no_index)):?>
                $("div#navigation.menu-navigation li[data-id='<?=$arSection["ID"]?>']").addClass("active");
                <?$menu = 1;?>
            <?endif;?>
            
        <?endforeach;?>
    <?endif;?>
    <?if($menu < 1):?>
    
		$("div#navigation.menu-navigation li.main").addClass("active");
    <?endif;?>
    
</script>




<?\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("ajax");?>