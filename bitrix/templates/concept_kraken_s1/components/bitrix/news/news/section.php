<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>

<?global $KRAKEN_TEMPLATE_ARRAY;?>


<?
$arSelect = Array("ID", "NAME", "PICTURE", "DESCRIPTION", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "UF_*");
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["SECTION_CODE"], "ACTIVE" => "Y");
$db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);


if(intval($db_list->SelectedRowsCount())>0)
{
	$ar_result = $db_list->GetNext();
}
else
{
	
	if (!defined("ERROR_404"))
	   define("ERROR_404", "Y");

		\CHTTP::setStatus("404 Not Found");
		   
		if ($APPLICATION->RestartWorkarea()) {
		   require(\Bitrix\Main\Application::getDocumentRoot()."/404.php");
		   die();
					}
}

$GLOBALS["KRAKEN_CURRENT_DIR"] = "section";
$GLOBALS["KRAKEN_CURRENT_SECTION_ID"] = $ar_result["ID"];
$GLOBALS["KRAKEN_CURRENT_TMPL"] = $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL_ENUM"]["XML_ID"];

if(strlen($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL"]) > 0)
{
    $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL_ENUM"] = CUserFieldEnum::GetList(array(), array(
        "ID" => $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL"],
    ))->GetNext();
}

if(strlen($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL_ENUM"]["XML_ID"]) <= 0)
    $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL_ENUM"]["XML_ID"] = "default";


if(strlen($ar_result["UF_KRAKEN_BNRS_VIEW"]) > 0)
{
    $ar_result["UF_KRAKEN_BNRS_VIEW_ENUM"] = CUserFieldEnum::GetList(array(), array(
        "ID" => $ar_result["UF_KRAKEN_BNRS_VIEW"],
    ))->GetNext();
}

if(strlen($ar_result["UF_KRAKEN_BNRS_VIEW_ENUM"]["XML_ID"]) <= 0)
    $ar_result["UF_KRAKEN_BNRS_VIEW_ENUM"]["XML_ID"] = "none";
   
   

if(strlen($ar_result["UF_KRAKEN_TXT_P"]) > 0)
{
    $ar_result["UF_KRAKEN_TXT_P_ENUM"] = CUserFieldEnum::GetList(array(), array(
        "ID" => $ar_result["UF_KRAKEN_TXT_P"],
    ))->GetNext();
}

if(strlen($ar_result["UF_KRAKEN_TXT_P_ENUM"]["XML_ID"]) <= 0)
    $ar_result["UF_KRAKEN_TXT_P_ENUM"]["XML_ID"] = "short";
   
  

$header_back = "";

if($ar_result["PICTURE"] > 0)
{
    $img = CFile::ResizeImageGet($ar_result["PICTURE"], array('width'=>2560, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, false);  
    $header_back = $img["src"];   
}


$arResult["BANNERS_RIGHT"] = Array();

if(!empty($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_BNNRS"]) && $ar_result["UF_KRAKEN_BNRS_VIEW_ENUM"]["XML_ID"] == "own")
    $arResult["BANNERS_RIGHT"] = $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_BNNRS"];



if($ar_result["UF_KRAKEN_BNRS_VIEW_ENUM"]["XML_ID"] == "parent")
{
    if(empty($arResult["BANNERS_RIGHT"]))
        $arResult["BANNERS_RIGHT"] = $KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"].'_BANNERS']['VALUE'];
}


$arSelect1 = Array("UF_KRAKEN_MAIN_".$arParams["TYPE"]);
$arFilter1 = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"]);
$db_list1 = CIBlockSection::GetList(Array(), $arFilter1, false, $arSelect1);

$menu_on = "";

while ($ar_result1 = $db_list1->GetNext())
{
	if($menu_on == "Y")
		break;

	if($ar_result["UF_KRAKEN_MAIN_".$arParams["TYPE"]])
    	$menu_on = "Y";
}
?>

<?if($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL_ENUM"]["XML_ID"] == "default"):?>
	<?$GLOBALS["IS_CONSTRUCTOR"] = false;?>


	<?if($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES_MODE"]["VALUE"] == "custom" && isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]{0})):?>
	    <style>
	        @media (max-width: 767.98px){
	            div.header-page{
	                background-image: url('<?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]?>') !important;
	            }
	        }
	    </style>
	<?endif;?>


	<div class="header-page new-first-block section cover parent-scroll-down <?=$KRAKEN_TEMPLATE_ARRAY["HEAD_TONE"]["VALUE"]?> kraken-firsttype-<?=$KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"]?> <?=($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES_MODE"]["VALUE"] == "custom" && !isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]{0})) ? "def-bg-xs" : "";?>" <?if(strlen($header_back) > 0):?>style="background-image: url('<?=$header_back?>');"<?endif;?>>
	    <div class="shadow"></div>
	    <div class="top-shadow"></div>

	    <div class="container">
	        <div class="row">


	            <div class="new-first-block-table clearfix">
	                                    
	                <div class="new-first-block-cell text-part col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                
	                    <div class="head">

	                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
	                                "COMPONENT_TEMPLATE" => ".default",
	                        		"START_FROM" => "0",
	                        		"PATH" => "",
	                        		"SITE_ID" => SITE_ID,
	                        		"COMPOSITE_FRAME_MODE" => "A",
	                        		"COMPOSITE_FRAME_TYPE" => "AUTO",
	                        	),
	                        	$component
	                        );?>

	                     

	                        
	                        <div class="title main1"><h1><?$APPLICATION->ShowTitle(false);?></h1></div>
	                  
  
	                        <?if(strlen($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_PRTXT"]) > 0):?>
                                <div class="subtitle"><?=$ar_result["~UF_KRAKEN_".$arParams["TYPE"]."_PRTXT"]?></div>
                            <?endif;?>
	                                                                        
	                    </div>
	                    
	                </div>

	                <div class="new-first-block-cell col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">

	                   <div class="wrap-scroll-down hidden-xs">
	                        <div class="down-scrollBig">
	                            <i class="fa fa-chevron-down"></i>
	                        </div>
	                    </div>
	                    
	                </div>

	            </div>
	
				<?include("search.php")?>


	        </div>

	    </div>
	                                        
	</div>

	<div class="news-list-wrap page_pad_bot <?if($menu_on != "Y"):?>no_menu<?endif;?>">

	    <div class="<?if($menu_on == "Y"):?>container<?endif;?>">

            <div class="<?if($menu_on == "Y"):?>row clearfix<?endif;?>">


	        	<?
	            	$class2 = "";

	            	if($menu_on == "Y")
	            	{
	            		$class = "col-lg-3 col-md-3 hidden-sm hidden-xs col-lg-push-9 col-md-push-9 col-sm-push-9 col-xs-push-9";
	            		$class2 = "col-lg-9 col-md-9 col-sm-12 col-xs-12 content-inner col-lg-pull-3 col-md-pull-3 col-sm-pull-0 col-xs-pull-0";

	            	}
	            ?>

	            <?if($menu_on == "Y"):?>
	        
		            <div class="<?=$class?>">
		                <div class="menu-navigation static" id="navigation">

		                    <div class="menu-navigation-wrap">

		                        <div class="menu-navigation-inner">

									<?$APPLICATION->IncludeComponent(
	                                	"bitrix:catalog.section.list",
	                                	"mainsections",
	                                	array(
	                                		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	                                		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	                                		"SECTION_ID" => $ar_result["IBLOCK_SECTION_ID"],
	                                		"SECTION_CODE" => "",
	                                		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	                                		"CACHE_TIME" => $arParams["CACHE_TIME"],
	                                		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	                                		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
	                                		"TOP_DEPTH" => 1,
	                                		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	                                		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
	                                		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
	                                		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
	                                		"ADD_SECTIONS_CHAIN" => "N",
	                                		"TYPE"=> $arParams["TYPE"]
	                                	),
	                                	$component,
	                                	array("HIDE_ICONS" => "Y")
	                                );                  
                                	?>

		                            <?if(!empty($arResult["BANNERS_RIGHT"]) > 0):?>
		                                
		                                <?$GLOBALS["arrBannersFilter"]["ID"] = $arResult["BANNERS_RIGHT"];?>
		                                <?CKraken::getIblockIDs(array("CODES"=>array("concept_kraken_site_banners_".SITE_ID),"SITE_ID"=>SITE_ID));?>
		                                
		                                <?$APPLICATION->IncludeComponent(
		                                	"bitrix:news.list", 
		                                	"banners-left", 
		                                	array(
		                                		"COMPONENT_TEMPLATE" => "banners-left",
		                                		"IBLOCK_TYPE" => $KRAKEN_TEMPLATE_ARRAY['BANNERS']["IBLOCK_TYPE"],
                                    			"IBLOCK_ID" => $KRAKEN_TEMPLATE_ARRAY['BANNERS']["IBLOCK_ID"],
		                                		"NEWS_COUNT" => "20",
		                                		"SORT_BY1" => "SORT",
		                                		"SORT_ORDER1" => "ASC",
		                                		"SORT_BY2" => "SORT",
		                                		"SORT_ORDER2" => "ASC",
		                                		"FILTER_NAME" => "arrBannersFilter",
		                                		"FIELD_CODE" => array(
		                                			0 => "DETAIL_PICTURE",
		                                			1 => "PREVIEW_PICTURE",
		                                		),
		                                		"PROPERTY_CODE" => array(
		                                			0 => "",
		                                			1 => "BANNER_BTN_TYPE",
		                                			2 => "BANNER_ACTION_ALL_WRAP",
		                                			3 => "BANNER_USER_BG_COLOR",
		                                			4 => "BANNER_UPTITLE",
		                                			5 => "BANNER_BTN_NAME",
		                                			6 => "BANNER_TITLE",
		                                			7 => "BANNER_BTN_BLANK",
		                                			8 => "BANNER_BORDER",
		                                			9 => "BANNER_DESC",
		                                			10 => "BANNER_TEXT",
		                                			11 => "BANNER_LINK",
		                                			12 => "BANNER_COLOR_TEXT",
		                                			13 => "",
		                                		),
		                                		"CHECK_DATES" => "Y",
		                                		"DETAIL_URL" => "",
		                                		"AJAX_MODE" => "N",
		                                		"AJAX_OPTION_JUMP" => "N",
		                                		"AJAX_OPTION_STYLE" => "Y",
		                                		"AJAX_OPTION_HISTORY" => "N",
		                                		"AJAX_OPTION_ADDITIONAL" => "",
		                                		"CACHE_TYPE" => "A",
		                                		"CACHE_TIME" => "36000000",
		                                		"CACHE_FILTER" => "Y",
		                                		"CACHE_GROUPS" => "Y",
		                                		"PREVIEW_TRUNCATE_LEN" => "",
		                                		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		                                		"SET_TITLE" => "N",
		                                		"SET_BROWSER_TITLE" => "N",
		                                		"SET_META_KEYWORDS" => "N",
		                                		"SET_META_DESCRIPTION" => "N",
		                                		"SET_LAST_MODIFIED" => "N",
		                                		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		                                		"ADD_SECTIONS_CHAIN" => "N",
		                                		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		                                		"PARENT_SECTION" => "",
		                                		"PARENT_SECTION_CODE" => "",
		                                		"INCLUDE_SUBSECTIONS" => "N",
		                                		"STRICT_SECTION_CHECK" => "N",
		                                		"DISPLAY_DATE" => "N",
		                                		"DISPLAY_NAME" => "N",
		                                		"DISPLAY_PICTURE" => "N",
		                                		"DISPLAY_PREVIEW_TEXT" => "N",
		                                		"COMPOSITE_FRAME_MODE" => "A",
		                                		"COMPOSITE_FRAME_TYPE" => "AUTO",
		                                		"PAGER_TEMPLATE" => ".default",
		                                		"DISPLAY_TOP_PAGER" => "N",
		                                		"DISPLAY_BOTTOM_PAGER" => "N",
		                                		"PAGER_TITLE" => "",
		                                		"PAGER_SHOW_ALWAYS" => "N",
		                                		"PAGER_DESC_NUMBERING" => "N",
		                                		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		                                		"PAGER_SHOW_ALL" => "N",
		                                		"PAGER_BASE_LINK_ENABLE" => "N",
		                                		"SET_STATUS_404" => "N",
		                                		"SHOW_404" => "N",
		                                		"MESSAGE_404" => ""
		                                	),
		                                	$component
		                                );?>
		                            
		                            <?endif;?>
		                            
		                        </div>

		                    </div>

		                </div>
		            </div>

	            <?endif;?>

	            <div class="<?=$class2?> page">

	                <div class="block<?if($menu_on == "Y"):?> small <?endif;?> padding-on">

	                	<?if(strlen($ar_result["UF_KRAKEN_SEO_TOP"]) > 0):?>
			
							<div class="top-description text-content">
								<div class="<?if($menu_on != "Y"):?>container<?endif;?>">
			        				
				
										<?=$ar_result["~UF_KRAKEN_SEO_TOP"]?>

							
					        	</div>
							
				    		</div>

				    	<?endif;?>

				    	<div class="<?if($menu_on != "Y"):?>container<?endif;?>">
			        		<div class="<?if($menu_on != "Y"):?>row<?endif;?>">
		
							<?$GLOBALS['arActionfilter'] = Array();?>

							<?if($arParams["TYPE"] != "ACT"):?>
									
								<?$GLOBALS['arActionfilter']["ACTIVE"] = "Y";?>
		                    	<?$GLOBALS['arActionfilter']["SECTION_GLOBAL_ACTIVE"] = "Y";?>
		                    	<?//$GLOBALS['arActionfilter']["SECTION_ACTIVE"] = "Y";?>
		                    	<?$GLOBALS['arActionfilter']["SECTION_SCOPE"] = "IBLOCK";?>
	                  		
	                  		<?endif;?>

							<?$section = $APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"",
								Array(
									"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"NEWS_COUNT" => $KRAKEN_TEMPLATE_ARRAY["ITEMS"][$arParams["TYPE"]."_NEWS_COUNT"]["VALUE"],
									"SORT_BY1" => $arParams["SORT_BY1"],
									"SORT_ORDER1" => $arParams["SORT_ORDER1"],
									"SORT_BY2" => $arParams["SORT_BY2"],
									"SORT_ORDER2" => $arParams["SORT_ORDER2"],
									"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
									"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
									"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
									"SET_TITLE" => $arParams["SET_TITLE"],
									"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
									"MESSAGE_404" => $arParams["MESSAGE_404"],
									"SET_STATUS_404" => $arParams["SET_STATUS_404"],
									"SHOW_404" => $arParams["SHOW_404"],
									"FILE_404" => $arParams["FILE_404"],
									"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
									"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
									"CACHE_TYPE" => $arParams["CACHE_TYPE"],
									"CACHE_TIME" => $arParams["CACHE_TIME"],
									"CACHE_FILTER" => $arParams["CACHE_FILTER"],
									"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
									"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
									"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
									"PAGER_TITLE" => $arParams["PAGER_TITLE"],
									"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
									"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
									"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
									"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
									"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
									"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
									"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
									"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
									"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
									"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
									"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
									"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
									"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
									"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
									"FILTER_NAME" => 'arActionfilter',
									"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
									"CHECK_DATES" => $arParams["CHECK_DATES"],
									"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
									"PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
									"PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
									"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
									"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
									"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
									"MENU_ON" => $menu_on,
									"TYPE" => $arParams["TYPE"],
									"HIDE_SECTIONS" => "Y"
								),
								$component
							);?>

							
							
							</div>
			        	</div>




						<?if(strlen($ar_result["DESCRIPTION"]) > 0 && $ar_result["UF_KRAKEN_TXT_P_ENUM"]["XML_ID"] == "short" && $menu_on == "Y"):?>
			

							<div class="bottom-description text-content">

							
								<?=$ar_result["~DESCRIPTION"]?>


				    		</div>

				    	<?endif;?>

						

						

					</div>
	             
		    	</div>

			</div>
		</div>
		
		

	</div>

	<?if(strlen($ar_result["DESCRIPTION"])>0 && ($ar_result["UF_KRAKEN_TXT_P_ENUM"]["XML_ID"]=="long" || $menu_on != "Y") ):?>
			

		<div class="bottom-description-full text-content">
			<div class="container">
				<?=$ar_result["~DESCRIPTION"]?>
			</div>

		</div>

	<?endif;?>



<?endif;?>


<?if($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TMPL_ENUM"]["XML_ID"] == "landing"):?>
    
    
    <?if($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_T_ID"] > 0):?>
   
        <?
	        $arFilter = Array("ID" => $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_T_ID"]);
	        $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
	        $ar_res = $db_list->GetNext();
        ?> 
    
    	<?if($ar_res["ACTIVE"] == "Y"):?>

    		<?$GLOBALS["IS_CONSTRUCTOR"] = true;?>
    		
	        <?$section = $APPLICATION->IncludeComponent(
	        	"concept:kraken.one.page", 
	        	"", 
	        	array(
	        		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	        		"CACHE_TIME" => $arParams["CACHE_TIME"],
	        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	        		"CHECK_DATES" => $arParams["CHECK_DATES"],
	        		"IBLOCK_ID" => $ar_res["IBLOCK_ID"],
	        		"IBLOCK_TYPE" => $ar_res["IBLOCK_TYPE_ID"],
	        		"PARENT_SECTION" => $ar_res["ID"],
	        		"SET_TITLE" => $arParams["SET_TITLE"],
	        		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
	        		"MESSAGE_404" => $arParams["MESSAGE_404"],
	        		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
	        		"SHOW_404" => $arParams["SHOW_404"],
	        		"FILE_404" => $arParams["FILE_404"],
	        		"COMPONENT_TEMPLATE" => ""
	        	),
	        	$component
	        );?>
	    
	        <?$GLOBALS["KRAKEN_CURRENT_SECTION_ID"] = $section;?>
	     <?else:?>

        	<?
        	if (!defined("ERROR_404"))
			   define("ERROR_404", "Y");

				\CHTTP::setStatus("404 Not Found");
				   
				if ($APPLICATION->RestartWorkarea()) {
				   require(\Bitrix\Main\Application::getDocumentRoot()."/404.php");
				   die();
				}
	

        	?>

        <?endif;?>
    
    <?endif;?>

<?endif;?>

