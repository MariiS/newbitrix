<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?

if($arResult["FOLDER"] !== $APPLICATION->GetCurPage(false))
{
	if (!defined("ERROR_404"))
	   define("ERROR_404", "Y");

	\CHTTP::setStatus("404 Not Found");
	   
	if ($APPLICATION->RestartWorkarea()) 
	{
	   require(\Bitrix\Main\Application::getDocumentRoot()."/404.php");
	   die();
	}
}

?>

<?

$menu_on = "";

if($arParams["TYPE"] == "ACT")
{
	$menu_on = "Y";
}
else
{
	$arSelect = Array("UF_KRAKEN_MAIN_".$arParams["TYPE"]);
	$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"]);
	$db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
	

	while ($ar_result = $db_list->GetNext())
	{
		if($menu_on == "Y")
			break;

		if($ar_result["UF_KRAKEN_MAIN_".$arParams["TYPE"]])
	    	$menu_on = "Y";
	}

}

?>
<?global $KRAKEN_TEMPLATE_ARRAY;?>



<?if(strlen($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BG_PIC"]["VALUE"])>0):?>

<?
	$img = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BG_PIC"]["VALUE"], array('width'=>2560, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, false);

	$header_back = $img["src"];
?>

<?endif;?>


<?if($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES_MODE"]["VALUE"] == "custom" && isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]{0})):?>
    <style>
        @media (max-width: 767.98px){
            div.header-page{
                background-image: url('<?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]?>') !important;
            }
        }
    </style>
<?endif;?>

	<div class="header-page new-first-block sections cover parent-scroll-down <?=$KRAKEN_TEMPLATE_ARRAY["HEAD_TONE"]["VALUE"]?> kraken-firsttype-<?=$KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"]?> <?=($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES_MODE"]["VALUE"] == "custom" && !isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]{0})) ? "def-bg-xs" : "";?>" <?if(strlen($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BG_PIC"]["VALUE"])>0):?>style="background-image: url('<?=$header_back?>');"<?endif;?>>
	    <div class="shadow"></div>
	    <div class="top-shadow"></div>

	    <div class="container">
	        <div class="row">


	            <div class="new-first-block-table clearfix">
	                                    
	                <div class="new-first-block-cell text-part col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                
	                    <div class="head">

	                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
	                                "COMPONENT_TEMPLATE" => ".default",
	                        		"START_FROM" => "0",
	                        		"PATH" => "",
	                        		"SITE_ID" => SITE_ID,
	                        		"COMPOSITE_FRAME_MODE" => "A",
	                        		"COMPOSITE_FRAME_TYPE" => "AUTO",
	                        	),
	                        	$component
	                        );?>
	                        
	                        <div class="title main1"><h1><?$APPLICATION->ShowTitle(false);?></h1></div>

	                        <?if(strlen($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_DESC"]["VALUE"]) > 0):?>
                                <div class="subtitle"><?=$KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_DESC"]["~VALUE"]?></div>
                            <?endif;?>
	                                                                        
	                    </div>
	                    
	                </div>

	                <div class="new-first-block-cell col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">

	                   <div class="wrap-scroll-down hidden-xs">
	                        <div class="down-scrollBig">
	                            <i class="fa fa-chevron-down"></i>
	                        </div>
	                    </div>
	                    
	                </div>

	            </div>

	            <?include("search.php");?>



	        </div>

	    </div>
	                                        
	</div>

	<div class="news-list-wrap page_pad_bot <?if($menu_on != "Y"):?>no_menu<?endif;?>">

        <div class="<?if($menu_on == "Y"):?>container<?endif;?>">

            <div class="<?if($menu_on == "Y"):?>row clearfix<?endif;?>">

            	<?
	            	$class2 = "no-menu";

	            	if($menu_on == "Y")
	            	{
	            		$class = "col-lg-3 col-md-3 hidden-sm hidden-xs col-lg-push-9 col-md-push-9 col-sm-push-9 col-xs-push-9";
	            		$class2 = "col-lg-9 col-md-9 col-sm-12 col-xs-12 content-inner col-lg-pull-3 col-md-pull-3 col-sm-pull-0 col-xs-pull-0";

	            	}
	            ?>

            	<?if($menu_on == "Y"):?>
	                <div class="<?=$class?>">
	                    <div class="menu-navigation static on-scroll" id="navigation">

	                        <div class="menu-navigation-wrap">

	                            <div class="menu-navigation-inner">

	                            	
	                            	<?
	                                    $tmpl = "";

	                                    if($arParams["TYPE"] == "ACT")
	                                        $tmpl = "_act";
	                                    
	                                ?>
	                                    
	                                <?$APPLICATION->IncludeComponent(
	                                        "bitrix:catalog.section.list",
	                                        "mainsections".$tmpl,
	                                        array(
	                                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	                                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
	                                            "SECTION_ID" => $arFields["IBLOCK_SECTION_ID"],
	                                            "SECTION_CODE" => "",
	                                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
	                                            "CACHE_TIME" => $arParams["CACHE_TIME"],
	                                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	                                            "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
	                                            "TOP_DEPTH" => 1,
	                                            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	                                            "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
	                                            "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
	                                            "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
	                                            "ADD_SECTIONS_CHAIN" => "N",
	                                            "TYPE"=> $arParams["TYPE"],
	                                           
	                                        ),
	                                        $component,
	                                        array("HIDE_ICONS" => "Y")
	                                    );                  
	                                ?>



	                                <?if($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"].'_BANNERS']["VALUE"] > 0):?>

	                                	<?CKraken::getIblockIDs(array("CODES"=>array("concept_kraken_site_banners_".SITE_ID),"SITE_ID"=>SITE_ID));?>
		                                
		                                <?$GLOBALS["arrBannersFilter"]["ID"] = $KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"].'_BANNERS']["VALUE"];?>
		                                
		                                <?$APPLICATION->IncludeComponent(
		                                	"bitrix:news.list", 
		                                	"banners-left", 
		                                	array(
		                                		"COMPONENT_TEMPLATE" => "banners-left",
		                                		"IBLOCK_TYPE" => $KRAKEN_TEMPLATE_ARRAY['BANNERS']["IBLOCK_TYPE"],
                                    			"IBLOCK_ID" => $KRAKEN_TEMPLATE_ARRAY['BANNERS']["IBLOCK_ID"],
		                                		"NEWS_COUNT" => "20",
		                                		"SORT_BY1" => "SORT",
		                                		"SORT_ORDER1" => "ASC",
		                                		"SORT_BY2" => "SORT",
		                                		"SORT_ORDER2" => "ASC",
		                                		"FILTER_NAME" => "arrBannersFilter",
		                                		"FIELD_CODE" => array(
		                                			0 => "DETAIL_PICTURE",
		                                			1 => "PREVIEW_PICTURE",
		                                		),
		                                		"PROPERTY_CODE" => array(
		                                			0 => "",
		                                			1 => "BANNER_BTN_TYPE",
		                                			2 => "BANNER_ACTION_ALL_WRAP",
		                                			3 => "BANNER_USER_BG_COLOR",
		                                			4 => "BANNER_UPTITLE",
		                                			5 => "BANNER_BTN_NAME",
		                                			6 => "BANNER_TITLE",
		                                			7 => "BANNER_BTN_BLANK",
		                                			8 => "BANNER_BORDER",
		                                			9 => "BANNER_DESC",
		                                			10 => "BANNER_TEXT",
		                                			11 => "BANNER_LINK",
		                                			12 => "BANNER_COLOR_TEXT",
		                                			13 => "",
		                                		),
		                                		"CHECK_DATES" => "Y",
		                                		"DETAIL_URL" => "",
		                                		"AJAX_MODE" => "N",
		                                		"AJAX_OPTION_JUMP" => "N",
		                                		"AJAX_OPTION_STYLE" => "Y",
		                                		"AJAX_OPTION_HISTORY" => "N",
		                                		"AJAX_OPTION_ADDITIONAL" => "",
		                                		"CACHE_TYPE" => "A",
		                                		"CACHE_TIME" => "36000000",
		                                		"CACHE_FILTER" => "Y",
		                                		"CACHE_GROUPS" => "Y",
		                                		"PREVIEW_TRUNCATE_LEN" => "",
		                                		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		                                		"SET_TITLE" => "N",
		                                		"SET_BROWSER_TITLE" => "N",
		                                		"SET_META_KEYWORDS" => "N",
		                                		"SET_META_DESCRIPTION" => "N",
		                                		"SET_LAST_MODIFIED" => "N",
		                                		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		                                		"ADD_SECTIONS_CHAIN" => "N",
		                                		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		                                		"PARENT_SECTION" => "",
		                                		"PARENT_SECTION_CODE" => "",
		                                		"INCLUDE_SUBSECTIONS" => "N",
		                                		"STRICT_SECTION_CHECK" => "N",
		                                		"DISPLAY_DATE" => "N",
		                                		"DISPLAY_NAME" => "N",
		                                		"DISPLAY_PICTURE" => "N",
		                                		"DISPLAY_PREVIEW_TEXT" => "N",
		                                		"COMPOSITE_FRAME_MODE" => "A",
		                                		"COMPOSITE_FRAME_TYPE" => "AUTO",
		                                		"PAGER_TEMPLATE" => ".default",
		                                		"DISPLAY_TOP_PAGER" => "N",
		                                		"DISPLAY_BOTTOM_PAGER" => "N",
		                                		"PAGER_TITLE" => "",
		                                		"PAGER_SHOW_ALWAYS" => "N",
		                                		"PAGER_DESC_NUMBERING" => "N",
		                                		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		                                		"PAGER_SHOW_ALL" => "N",
		                                		"PAGER_BASE_LINK_ENABLE" => "N",
		                                		"SET_STATUS_404" => "N",
		                                		"SHOW_404" => "N",
		                                		"MESSAGE_404" => ""
		                                	),
		                                	$component
		                                );?>
		                            
		                            <?endif;?>
	                         

	                            </div>

	                        </div>


	                    </div>
	                </div>
                <?endif;?>

                <div class="<?=$class2?> page">

                    <div class="block<?if($menu_on == "Y"):?> small <?endif;?> padding-on">

                    	<?if(strlen($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_TOP_TEXT"]["~VALUE"])>0):?>

			        		<div class="top-description text-content">

			        			<div class="<?if($menu_on != "Y"):?>container<?endif;?>">
			        				
	        						<?=$KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_TOP_TEXT"]["~VALUE"]?>
					        	
					        	</div>
			        				

		        			</div>

			        	<?endif;?>

			        	<div class="<?if($menu_on != "Y"):?>container<?endif;?>">
			        		<div class="<?if($menu_on != "Y"):?>row<?endif;?>">
                    	
		                    	<?$GLOBALS['arActionfilter'] = Array();?>


		                    	<?
		                    		if($arParams["TYPE"] == "ACT")
			                    	{
			                    		$GLOBALS['arActionfilter'] = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE_DATE" => "", "<DATE_ACTIVE_FROM" => ConvertTimeStamp(time(),"FULL"));

			                    		if($_REQUEST["action"]=="on")
			                    			$GLOBALS['arActionfilter'] = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE_DATE" => "Y", "<DATE_ACTIVE_FROM" => ConvertTimeStamp(time(),"FULL"));

			                    		if($_REQUEST["action"]=="off")
			                    			$GLOBALS['arActionfilter'] = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE_DATE"=>"", "<=DATE_ACTIVE_TO" => ConvertTimeStamp(time(),"FULL"));
			                    	}

		                    	?>
								
								<?if($arParams["TYPE"] != "ACT"):?>
									
									<?$GLOBALS['arActionfilter']["ACTIVE"] = "Y";?>
			                    	<?$GLOBALS['arActionfilter']["SECTION_GLOBAL_ACTIVE"] = "Y";?>
			                    	<?//$GLOBALS['arActionfilter']["SECTION_ACTIVE"] = "Y";?>
			                    	<?$GLOBALS['arActionfilter']["SECTION_SCOPE"] = "IBLOCK";?>
			                    	<?$GLOBALS['arActionfilter']["SECTION_ID"] = "0";?>

		                  		
		                  		<?endif;?>


								<?

								$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"",
									Array(
										"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
										"IBLOCK_ID" => $arParams["IBLOCK_ID"],
										"NEWS_COUNT" => $KRAKEN_TEMPLATE_ARRAY["ITEMS"][$arParams["TYPE"]."_NEWS_COUNT"]["VALUE"],
										"SORT_BY1" => $arParams["SORT_BY1"],
										"SORT_ORDER1" => $arParams["SORT_ORDER1"],
										"SORT_BY2" => $arParams["SORT_BY2"],
										"SORT_ORDER2" => $arParams["SORT_ORDER2"],
										"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
										"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
										"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
										"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
										"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
										"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
										"SET_TITLE" => $arParams["SET_TITLE"],
										"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
										"MESSAGE_404" => $arParams["MESSAGE_404"],
										"SET_STATUS_404" => $arParams["SET_STATUS_404"],
										"SHOW_404" => $arParams["SHOW_404"],
										"FILE_404" => $arParams["FILE_404"],
										"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
										"CACHE_TYPE" => $arParams["CACHE_TYPE"],
										"CACHE_TIME" => $arParams["CACHE_TIME"],
										"CACHE_FILTER" => $arParams["CACHE_FILTER"],
										"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
										"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
										"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
										"PAGER_TITLE" => $arParams["PAGER_TITLE"],
										"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
										"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
										"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
										"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
										"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
										"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
										"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
										"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
										"DISPLAY_DATE" => "Y",
										"DISPLAY_NAME" => "Y",
										"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
										"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
										"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
										"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
										"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
										"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
										"FILTER_NAME" => "arActionfilter",
										"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
										"CHECK_DATES" => "Y",
										"ADD_SECTIONS_CHAIN" => "Y",
										"MENU_ON" => $menu_on,
										"TYPE" => $arParams["TYPE"],
										"SHOW_ALL_WO_SECTION" => "Y"
		                          
									),
									$component
								);?>

								<?$GLOBALS["KRAKEN_CURRENT_SECTION_ID"] = 0;?>
								<?$GLOBALS["KRAKEN_CURRENT_DIR"] = "main";?>

							</div>
			        	</div>

						<?if(strlen($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BOT_TEXT"]["~VALUE"])>0 && $KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"].'_BOT_TEXT_POS']['VALUE'] == "short" && $menu_on == "Y"):?>

		        			<div class="bottom-description text-content">

				        		<?=$KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BOT_TEXT"]["~VALUE"]?>
				        			
				        	</div>

			        	<?endif;?>

					</div>
	                 
	                    
	                   
			    </div>

			    


			</div>

		</div>

		

	</div>

	<?if(strlen($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BOT_TEXT"]["~VALUE"])>0 && ($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"].'_BOT_TEXT_POS']['VALUE'] == "long" || $menu_on != "Y") ):?>
			

		<div class="bottom-description-full text-content">
			<div class="container">
				<?=$KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BOT_TEXT"]["~VALUE"]?>
			</div>

		</div>

	<?endif;?>
