<?$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION"] = "Каталог &ndash; Главная страница";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_DEF"] = "Главная страница";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_new"] = "Новинки";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_act"] = "Акции и скидки";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_pop"] = "Популярное";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_rec"] = "Рекомендуем";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_BTN_ADD_NAME"] = "Добавить в корзину";
$MESS["KRAKEN_TEMPLATES_CATALOG_SECTION_LABELS_SECTION_BTN_ADDED_NAME"] = "Товар добавлен";