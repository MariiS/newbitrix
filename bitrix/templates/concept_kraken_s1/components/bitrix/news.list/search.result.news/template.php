<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
global $KRAKEN_TEMPLATE_ARRAY;
CKraken::includeCustomMessages();
?>


<?
$page = array();
$page["BLOG"] = $KRAKEN_TEMPLATE_ARRAY["SEARCH"]["SEARCH_PAGE"]["VALUE"].$KRAKEN_TEMPLATE_ARRAY["SEARCH"]["URLS"]["BLOG"]."/";
$page["NEWS"] = $KRAKEN_TEMPLATE_ARRAY["SEARCH"]["SEARCH_PAGE"]["VALUE"].$KRAKEN_TEMPLATE_ARRAY["SEARCH"]["URLS"]["NEWS"]."/";
$page["ACTIONS"] = $KRAKEN_TEMPLATE_ARRAY["SEARCH"]["SEARCH_PAGE"]["VALUE"].$KRAKEN_TEMPLATE_ARRAY["SEARCH"]["URLS"]["ACTIONS"]."/";
?>

<div class="col-xs-12">
    <div class="section-block show-hidden-parent" id="to-<?=$arParams["TYPE_CODE"]?>">
        <div class="section-head">
            <div class="title-wrap">
                <div class="title"><?=$arParams["TITLE"]?> <span class="title-count">(<?=$arParams["SEARCH_RESULT"]["COUNT_TOTAL"]?>)</span></div>
            </div>

            <?if( $arParams["SEARCH_RESULT"]["COUNT_TOTAL"] > 4 && $arParams["VIEW"] == "short" ):?>
            	<a href="<?=$page[$arParams["TYPE_CODE"]]?>?q=<?=$arParams["QUERY"]?>" target="_blank" class="btn-trasparent"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_NEWS_SHOW_ALL")?></a>
            <?endif;?>

        </div>

        <div class="section-block-content">
            <div class="news-chrono-flat">

                <div class="row">

                	<?foreach($arResult["ITEMS"] as $key=>$arItem):?>

                        <?if( ($key+1) > 4 && $arParams["VIEW"] == "short" )
                            break;?>

	                    <div class="col-md-3 col-sm-4 col-xs-12">
	                        <div class="element">

	                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank">
                            	<?$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>400, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL, false);?>
                                    <div class="wrap-img lazyload" data-src="<?=$img['src']?>"></div>
                                </a>

                                <?if($arParams["TYPE_CODE"] == "ACTIONS"):?>


                                    <div class="date-action">
                                        <?$frame = $this->createFrame()->begin("");?>
                                            
                                            <?if(getmicrotime() > MakeTimeStamp($arItem["ACTIVE_TO"]) && strlen($arItem["ACTIVE_TO"])>0):?>

                                                <span class="off"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_NEWS_ACT_OFF")?></span>

                                            <?else:?>

                                                <?if(strlen($arItem["ACTIVE_TO"])>0):?>

                                                    <span class="to"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_NEWS_ACT_ON_TO")?><?echo CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arItem["ACTIVE_TO"], CSite::GetDateFormat()));?></span>

                                                <?else:?>

                                                    <span><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_NEWS_ACT_ON")?></span>

                                                <?endif;?>

                                            <?endif;?>

                                        <?$frame->end();?>
                                    </div>

                                <?endif;?>

                                <div class="name bold">
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><?=$arItem["NAME"]?></a>
                                </div>

								<div class="text">
                                    <?=$arItem["PROPERTIES"]["NEWS_DETAIL_TEXT"]["~VALUE"]["TEXT"]?>
                                </div>

								<?if(strlen($KRAKEN_TEMPLATE_ARRAY["NW_MORE"]["~VALUE"])):?>
	                                <div class="btn-detail-wrap">
	                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank">
	                                        <i class="ic-style fa fa-info" aria-hidden="true"></i><span class='bord-bot'><?=$KRAKEN_TEMPLATE_ARRAY["NW_MORE"]["~VALUE"]?></span>
	                                    </a>
	                                </div>
                                <?endif;?>

	                        </div>
	                    </div>

	                    <?

		                	if( ($key+1) % 4 == 0 )
		                	{
		                		echo "<div class='clearfix hidden-sm hidden-xs'></div>";
		                	}

		                	if( ($key+1) % 3 == 0 )
		                	{
		                		echo "<div class='clearfix visible-sm'></div>";
		                	}

		                ?>

	                <?endforeach;?>
	                    
                </div>
                
            </div>

            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                <div class="clearfix"></div>
                <?=$arResult["NAV_STRING"]?>
            <?endif;?>
        </div>
    </div>
</div>