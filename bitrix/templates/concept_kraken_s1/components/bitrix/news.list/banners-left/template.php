<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?global $KRAKEN_TEMPLATE_ARRAY;?>
<?$show_setting = $KRAKEN_TEMPLATE_ARRAY["MODE_FAST_EDIT"]['VALUE'][0];?>
<?$admin_active = ($USER->isAdmin() || $APPLICATION->GetGroupRight("concept.kraken") > "R");?>
<?if(!empty($arResult["ITEMS"])):?>

<?
$firstItem = array_shift($arResult["ITEMS"]);
if(empty($arResult["ITEMS"]))
    $arResult["ITEMS"] = array();

array_unshift($arResult["ITEMS"], $firstItem);
?>

    <div class="img-for-lazyload-parent">
        <img class="lazyload img-for-lazyload slider-start" data-src="<?=SITE_TEMPLATE_PATH?>/images/one_px.png" data-id="<?=$firstItem["ID"]?>">

        <div class="menu-banners menu-banner-slider parent-slider-item-js">

            <?foreach($arResult["ITEMS"] as $k => $banner):?>
                <div class="<?=($k!=0)?'noactive-slide-lazyload':'';?>">
                    <?

                        $onlyIMG = "";

                        if(!isset($banner['PROPERTIES']['BANNER_UPTITLE']['~VALUE']{0})
                            && !isset($banner['PROPERTIES']['BANNER_TITLE']['~VALUE']{0})
                            && empty($banner['PROPERTIES']['BANNER_TEXT']['VALUE'])
                            && !isset($banner['PROPERTIES']['BANNER_BTN_NAME']['~VALUE']{0})

                            && !isset($banner['PROPERTIES']['BANNER_DESC']['~VALUE']{0}))
                            $onlyIMG = "only-img";

                        
                        $imgBG['src'] = '';

                        if(strlen($banner['DETAIL_PICTURE']["ID"]) > 0)
                            $imgBG = CFile::ResizeImageGet($banner['DETAIL_PICTURE']["ID"], array('width'=>600, 'height'=>800), BX_RESIZE_IMAGE_EXACT, true);


                        $arClass = array();
                        $arClass=array(
                            "XML_ID"=> $banner["PROPERTIES"]["BANNER_BTN_TYPE"]["VALUE_XML_ID"],
                            "FORM_ID"=> $banner["PROPERTIES"]["BANNER_BTN_FORM"]["VALUE"],
                            "MODAL_ID"=> $banner["PROPERTIES"]["BANNER_BTN_MODAL"]["VALUE"],
                            "QUIZ_ID"=> $banner["PROPERTIES"]["BANNER_BTN_QUIZ"]["VALUE"],
                        );

                        $arAttr=array();
                        $arAttr=array(
                            "XML_ID"=> $banner["PROPERTIES"]["BANNER_BTN_TYPE"]["VALUE_XML_ID"],
                            "FORM_ID"=> $banner["PROPERTIES"]["BANNER_BTN_FORM"]["VALUE"],
                            "MODAL_ID"=> $banner["PROPERTIES"]["BANNER_BTN_MODAL"]["VALUE"],
                            "LINK"=> $banner["PROPERTIES"]["BANNER_LINK"]["VALUE"],
                            "BLANK"=> $banner["PROPERTIES"]["BANNER_BTN_BLANK"]["VALUE_XML_ID"],
                            "HEADER"=> $banner['NAME'],
                            "QUIZ_ID"=> $banner["PROPERTIES"]["BANNER_BTN_QUIZ"]["VALUE"],
                            "LAND_ID"=> $banner["PROPERTIES"]["BANNER_BTN_LAND"]["VALUE"]
                        );
                            
                    ?>
                    
                    <div class="item <?=$banner['PROPERTIES']['BANNER_COLOR_TEXT']['VALUE_XML_ID']?> <?=$banner['PROPERTIES']['BANNER_BORDER']['VALUE_XML_ID']?> <?=(isset($imgBG['src']{0}))?"lazyload":""?> <?=$onlyIMG?>" <?=(isset($imgBG['src']{0}))?"data-src=\"".$imgBG['src']."\"":""?> <?=(isset($banner['PROPERTIES']['BANNER_USER_BG_COLOR']['VALUE']{0}))?"style=\"background-color: ".$banner['PROPERTIES']['BANNER_USER_BG_COLOR']['VALUE'].";\"":""?>>

                        <?CKraken::admin_setting($banner, false, $admin_active, $show_setting)?>

                        

                        <?if(strlen($banner['PREVIEW_PICTURE']["ID"]) > 0):?>
                        
                            <img data-src="<?=$banner['PREVIEW_PICTURE']["SRC"]?>" alt="<?=$banner['PREVIEW_PICTURE']["ALT"]?>" class="img lazyload">

                        <?endif;?>

                        <?if(strlen($banner['PROPERTIES']['BANNER_UPTITLE']['~VALUE']) > 0):?>
                            <div class="uptitle"><?=$banner['PROPERTIES']['BANNER_UPTITLE']['~VALUE']?></div>
                        <?endif;?>

                        <?if(strlen($banner['PROPERTIES']['BANNER_TITLE']['~VALUE']) > 0):?>

                            <div class="name bold"><?=$banner['PROPERTIES']['BANNER_TITLE']['~VALUE']?></div>

                        <?endif;?>


                        <?if(!empty($banner['PROPERTIES']['BANNER_TEXT']['VALUE'])):?>

                            <div class="desc"><?=$banner['PROPERTIES']['BANNER_TEXT']['~VALUE']['TEXT']?></div>

                        <?endif;?>

                        <?if(strlen($banner['PROPERTIES']['BANNER_BTN_NAME']['~VALUE']) > 0):?>

                            <a class="button-def main-color <?=$KRAKEN_TEMPLATE_ARRAY["BTN_VIEW"]['VALUE']?> <?=CKraken::buttonEditClass ($arClass)?>" <?=CKraken::buttonEditAttr ($arAttr)?>><?=$banner['PROPERTIES']['BANNER_BTN_NAME']['~VALUE']?></a>

                        <?endif;?>
                        

                        <?if($banner['PROPERTIES']['BANNER_ACTION_ALL_WRAP']['VALUE'] == 'Y'):?>
                            <a class='menu-banner-wrap <?=CKraken::buttonEditClass ($arClass)?>' <?=CKraken::buttonEditAttr ($arAttr)?>></a>
                        <?endif;?>
                       
                    </div>

                    <?if(strlen($banner['PROPERTIES']['BANNER_DESC']['~VALUE']) > 0):?>

                        <div class="more-desc italic"><?=$banner['PROPERTIES']['BANNER_DESC']['~VALUE']?></div>

                    <?endif;?>
                </div>

            <?endforeach;?>
        </div>

        <img class="lazyload img-for-lazyload slider-finish" data-src="<?=SITE_TEMPLATE_PATH?>/images/one_px.png" data-id="<?=$firstItem["ID"]?>">
    </div>

  

<?endif;?>