<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
global $KRAKEN_TEMPLATE_ARRAY;
CKraken::includeCustomMessages();
?>

<?
    $two_cols = false;

    if($KRAKEN_TEMPLATE_ARRAY["CATALOG_VIEW_XS"]["VALUE"] == "")
        $KRAKEN_TEMPLATE_ARRAY["CATALOG_VIEW_XS"]["VALUE"] = "6";
        
    
    if($KRAKEN_TEMPLATE_ARRAY["CATALOG_VIEW_XS"]["VALUE"] == "6")
        $two_cols = true;
?>

<div class="col-xs-12">

    <div class="section-block show-hidden-parent" id="to-CATALOG">
        <div class="section-head">
            <div class="title-wrap">
                <div class="title"><?=$arParams["TITLE"]?></div>
            </div>
            <?if( ( ($arParams["SEARCH_RESULT"]["COUNT_ELEMENTS"] > 4) || ($arParams["SEARCH_RESULT"]["COUNT_SECTIONS"] > 6) ) && $arParams["VIEW"] == "short" ):?>
            	<a href="<?=$KRAKEN_TEMPLATE_ARRAY["SEARCH"]["SEARCH_PAGE"]["VALUE"].$KRAKEN_TEMPLATE_ARRAY["SEARCH"]["URLS"]["CATALOG"]."/"?>?q=<?=$arParams["QUERY"]?>" target="_blank" class="btn-trasparent"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_CATALOG_SHOW_ALL")?></a>
            <?endif;?>
        </div>


        <?if( !empty($arResult["SEARCH_ITEMS_ID"]) ):?>

	        <div class="desc-mini-wrap">

	            <table class="desc-mini">
	                <tr>
	                    <td><div class="desc-count"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_CATALOG_ITEMS")?>(<?=$arParams["SEARCH_RESULT"]["COUNT_ELEMENTS"]?>)</div></td>

	                </tr>
	            </table>

	        </div>


        
	        <div class="section-block-content">


	        	<?
	        		$GLOBALS['arFilterSearchrCatalog'] = array("ID" => $arResult["SEARCH_ITEMS_ID"]);

			      $APPLICATION->IncludeComponent(
			        "bitrix:catalog.section",
			        "main",
			        array(
			        	'SEARCH_ELEMENTS_ID' => $arParams["SEARCH_RESULT"]["ITEMS_ID"],
			            "FILTER_NAME" => "arFilterSearchrCatalog",
			            "ELEMENT_SORT_FIELD" => "sort",
			            "ELEMENT_SORT_ORDER" => "asc",
			            "ELEMENT_SORT_FIELD2" => "id",
			            "ELEMENT_SORT_ORDER2" => "asc",
			            "PAGE_ELEMENT_COUNT" => $arResult["PAGE_ELEMENT_COUNT"],
			            "IBLOCK_TYPE" => $KRAKEN_TEMPLATE_ARRAY['CATALOG']["IBLOCK_TYPE"],
			            "IBLOCK_ID" => $KRAKEN_TEMPLATE_ARRAY['CATALOG']["IBLOCK_ID"],
			            "SIDE_MENU"=>false
			            
			        ),
			        $component
			      );
	        	?>


	        </div>

	        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<div class="clearfix"></div>
			    <?=$arResult["NAV_STRING"]?>
			<?endif;?>



        <?endif;?>

        <?if( !empty($arResult["SECTIONS"]) ):?>

	        <div class="section-block-mini">

	            <div class="desc-mini-wrap">

		            <table class="desc-mini">
		                <tr>
		                    <td><div class="desc-count"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_LIST_SEARCH_RESULT_CATALOG_SECTIONS")?> (<?=$arParams["SEARCH_RESULT"]["COUNT_SECTIONS"]?>)</div></td>

		                </tr>
		            </table>

		        </div>

	          

	            <div class="category">

	            	<?foreach($arResult["SECTIONS"] as $key=>$arItem):?>

	            		<?if( ($key+1) > 12 )
                            break;?>

	            		<div class="col-md-2 col-sm-4 col-xs-6">

		                    <div class="category-item-wrap">

		                        <a href="<?=$arItem['SECTION_PAGE_URL']?>" target="_blank" class="general-link-wrap"></a>
		                    
		                        <table class="category-item">

		                            <tr>
		                                <td class="pic">

		                                	<div class="pic-wrap">
		                                		<?if($arItem["UF_KRAKEN_MENU_PICT"] !== NULL):?>
		                                		
			                                		<?$img = CFile::ResizeImageGet($arItem["UF_KRAKEN_MENU_PICT"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_EXACT, false);?>
			                                    
			                                    	<img class="lazyload" data-src="<?=$img['src']?>" alt=""/>

			                                    <?elseif($arItem["DETAIL_PICTURE"] !== NULL):?>

			                                    	<?$img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_EXACT, false);?>
			                                    
			                                    	<img class="lazyload" data-src="<?=$img['src']?>" alt=""/>

			                                    <?endif;?>
		                                	</div>
		                                    
		                                </td>
		                            </tr>
		                            <tr>
		                                <td class="name"><?=$arItem["NAME"]?></td>
		                            </tr>
		                        </table>

		                    </div>

		                </div>

		                <?

		                	if( ($key+1) % 6 == 0 )
		                	{
		                		echo "<div class='clearfix hidden-sm hidden-xs'></div>";
		                	}

		                	if( ($key+1) % 3 == 0 )
		                	{
		                		echo "<div class='clearfix visible-sm'></div>";
		                	}

		                	if( ($key+1) % 2 == 0 )
		                	{
		                		echo "<div class='clearfix visible-xs'></div>";
		                	}

		                ?>

	            	<?endforeach;?>
	                    
	                <div class="clearfix"></div>

	            </div>
	        </div>

        	<div class="clearfix"></div>

        <?endif;?>

        <div class="clearfix"></div>


    </div>

    <div class="clearfix"></div>

</div>

<div class="clearfix"></div>