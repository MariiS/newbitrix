<?
$MESS["KRAKEN_SETTINGS_LIST_PAGE_ON"] = "Лендинг включен";
$MESS["KRAKEN_SETTINGS_LIST_PAGE_OFF"] = "Лендинг отключен";

$MESS["KRAKEN_SETTINGS_LIST_BUTTON_TIP"] = "Режим редактирования";
$MESS["KRAKEN_SETTINGS_MAIN_cart"] = "Настройки интернет-магазина";

$MESS["KRAKEN_SETTINGS_MAIN_news"] = "Настройки главной страницы новостей";
$MESS["KRAKEN_SETTINGS_SECT_news"] = "Настройки текущего раздела новостей";
$MESS["KRAKEN_SETTINGS_ELEM_news"] = "Настройки текущей новости";
$MESS["KRAKEN_SETTINGS_ADD_news"] = "Добавить новость";

$MESS["KRAKEN_SETTINGS_MAIN_blog"] = "Настройки главной страницы блога";
$MESS["KRAKEN_SETTINGS_SECT_blog"] = "Настройки текущего раздела блога";
$MESS["KRAKEN_SETTINGS_ELEM_blog"] = "Настройки текущей статьи";
$MESS["KRAKEN_SETTINGS_ADD_blog"] = "Добавить статью";

$MESS["KRAKEN_SETTINGS_MAIN_action"] = "Настройки главной страницы акций";
$MESS["KRAKEN_SETTINGS_SECT_action"] = "Настройки текущего раздела акций";
$MESS["KRAKEN_SETTINGS_ELEM_action"] = "Настройки текущей акции";
$MESS["KRAKEN_SETTINGS_ADD_action"] = "Добавить акцию";

$MESS["KRAKEN_SETTINGS_MAIN_catalog"] = "Настройки главной страницы каталога";
$MESS["KRAKEN_SETTINGS_SECT_catalog"] = "Настройки текущего раздела каталога";
$MESS["KRAKEN_SETTINGS_ELEM_catalog"] = "Настройки текущего товара";
$MESS["KRAKEN_SETTINGS_ADD_catalog"] = "Добавить товар";

$MESS["KRAKEN_SETTINGS_LIST_SEO_PAGE"] = "SEO-оптимизация страницы";
$MESS["KRAKEN_SETTINGS_CLOSE_LIST_SET"] = "Закрыть настройки";
$MESS["KRAKEN_SETTINGS_LIST_ADDPAGE"] = "Ваши лендинги";
$MESS["KRAKEN_SETTINGS_LIST_EDIT_SETS"] = "Общие настройки сайта";
$MESS["KRAKEN_SETTINGS_LIST_ADDBLOCK"] = "Добавить блок";
$MESS["KRAKEN_SETTINGS_LIST_SEO"] = "SEO-оптимизация лендинга";
$MESS["KRAKEN_SETTINGS_LIST_NEWPAGE"] = "Создание лендинга за 30 секунд";

$MESS["KRAKEN_SETTINGS_LIST_MODAL"] = "Модальные окна";
$MESS["KRAKEN_SETTINGS_LIST_FORMS"] = "Формы захвата";
$MESS["KRAKEN_SETTINGS_LIST_PAGE"] = "Настройки лендинга";
$MESS["KRAKEN_SETTINGS_LIST_SECTEDIT"] = "Добавить раздел";
$MESS["KRAKEN_SETTINGS_LIST_ADDEDIT"] = "Добавить раздел";
$MESS["KRAKEN_SETTINGS_IBLIST"] = "Другие разделы";