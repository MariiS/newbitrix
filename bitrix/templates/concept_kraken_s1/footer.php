<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG"]["SRC"]{0}) || $KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG_CLR"]["VALUE"] !="transparent"):?>
    <style>
        body{
            <?if(isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG"]["SRC"]{0})):?>background-image: url(<?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG"]["SRC"]?>);<?endif;?>
            background-attachment: <?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG_POS"]["VALUE"]?>;
            background-repeat: <?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG_REPEAT"]["VALUE"]?>;
            background-position: top center;
            background-color: <?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["BODY_BG_CLR"]["VALUE"]?>;
        }
    </style>
<?endif;?>

    <?if($KRAKEN_TEMPLATE_ARRAY["FOOTER_ON"]["VALUE"][0] == "Y"):?>
        <?
        $footer_style = "";
        $footer_attr = "";

        if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_BG"]["VALUE"])>0)
            {
                $footer_bg = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY["FOOTER_BG"]["VALUE"], array('width'=>1600, 'height'=>1200), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $footer_attr .= "data-src ='".$footer_bg['src']."'";
            }

            if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COLOR_BG"]['VALUE'])>0)
            {
                $arColor = $KRAKEN_TEMPLATE_ARRAY["FOOTER_COLOR_BG"]['VALUE'];
                $percent = 1;

                if($KRAKEN_TEMPLATE_ARRAY["FOOTER_COLOR_BG"]['VALUE'] == "transparent")
                    $footer_style .= 'background-color: transparent; ';

             
                else if(preg_match('/^\#/', $arColor))
                {
                    $arColor = CKraken::Krakenhex2rgb($arColor);
                    $arColor = implode(',',$arColor);

                    if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_OPACITY_BG"]['VALUE'])>0)
                    $percent = (100 - $KRAKEN_TEMPLATE_ARRAY["FOOTER_OPACITY_BG"]['VALUE'])/100;

                
                    $footer_style .= ' background-color: rgba('.$arColor.', '.$percent.')";';
                }

                else
                    $footer_style .= 'background-color: '.$arColor.'; ';
            }
        ?>
        

        <footer class="tone-<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_TONE"]["VALUE"]?> lazyload <?if(strlen($footer_style)<=0):?>default_bg<?endif;?>" <?=$footer_attr?> <?if(strlen($footer_style)>0):?> style="<?=$footer_style?>" <?endif;?>>

            <div class="shadow"></div>

            <div class="footer-menu-wrap">


                <div class="container">
                    <div class="row">


                        <?$collsCenter = 'col-lg-6 col-md-6 col-sm-8 col-xs-12';?>

                        <?if( ($KRAKEN_TEMPLATE_ARRAY["SHOW_CALLBACK"]["VALUE"][0] != "Y") && empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"]) && empty($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"]) && $KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][2] != 'Y'):?>
                            
                            <?$collsCenter = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';?>

                        <?elseif($KRAKEN_TEMPLATE_ARRAY["SHOW_CALLBACK"]["VALUE"][0] != "Y" && empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"])):?>

                            <?$collsCenter = 'col-lg-9 col-md-9 col-sm-12 col-xs-12';?>

                        <?elseif(empty($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"]) && $KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][2] != 'Y'):?>

                            <?$collsCenter = 'col-lg-9 col-md-9 col-sm-8 col-xs-12';?>

                        <?endif;?>



                        <?if(($KRAKEN_TEMPLATE_ARRAY["SHOW_CALLBACK"]["VALUE"][0] == "Y" && $KRAKEN_TEMPLATE_ARRAY["FORMS"]["VALUE_CALLBACK"] != "N") || !empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"])):?>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 left">

                                <?if(!empty($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"])):?>
                                    <div class="email"><a href="mailto:<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?>"><span class="bord-bot white"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?></span></a></div>
                                <?endif;?>

                                <br>

                                <?if(!empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"])):?>
                                    <div class="phone">
                                        <?foreach($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"] as $keyPhone => $arPnone):?>
                                            <div><div class="phone-value"><?=$arPnone['name']?></div></div>
                                            <?if($keyPhone >= 1) break;?>
                                        <?endforeach;?>
                                    </div>
                                <?endif;?>

                                <?if($KRAKEN_TEMPLATE_ARRAY["SHOW_CALLBACK"]["VALUE"][0] == "Y" && $KRAKEN_TEMPLATE_ARRAY["FORMS"]["VALUE_CALLBACK"] != "N"):?>
                                    <div class="button-wrap">
                                        <a class="button-def main-color <?=$KRAKEN_TEMPLATE_ARRAY["BTN_VIEW"]['VALUE']?> call-modal callform" data-call-modal="form<?=$KRAKEN_TEMPLATE_ARRAY["FORMS"]["VALUE_CALLBACK"]?>"><?=$KRAKEN_TEMPLATE_ARRAY["CALLBACK_NAME"]["VALUE"]?></a>
                                    </div>
                                <?endif;?>
                            </div>

                        <?endif;?>

                        <div class="<?=$collsCenter?> center">
                            <div class="copyright-text">

                                <?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_DESC"]["VALUE"])>0):?>
                                    <div class="top-text"><?=$KRAKEN_TEMPLATE_ARRAY["FOOTER_DESC"]["~VALUE"]?></div>
                                <?endif;?>
                                
                                

                                <?if(!empty($KRAKEN_TEMPLATE_ARRAY['AGREEMENT_FOOTER'])):?>
                                    <div class="political">
                                        
                        
                                        <?foreach($KRAKEN_TEMPLATE_ARRAY['AGREEMENT_FOOTER'] as $arAgr):?>

                                            <a class="call-modal callagreement" data-call-modal="agreement<?=$arAgr['ID']?>"><span class="bord-bot"><?=$arAgr['NAME']?></span></a>

                                            
                                        <?endforeach;?>
                                       
                                    </div>
                                <?endif;?>

                                

                            </div>
                        </div>
                            
                      
                       
                        <?if(!empty($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"]) || $KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][2] == 'Y'):?>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 right">

                                <?if($KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][2] == 'Y'):?>
                                    <?=CKraken::CreateSoc($KRAKEN_TEMPLATE_ARRAY)?>
                                <?endif;?>
                                

                                <?/*if(!empty($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"])):?>
                                    <div class="email"><a href="mailto:<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?>"><span class="bord-bot white"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?></span></a></div>
                                <?endif;*/?>

                                
                                <a<?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_URL"]["VALUE"])>0):?> href="<?=$KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_URL"]["VALUE"]?>" target="_blank"<?endif;?> class="copyright vedita-link">

                                    <table>
                                        <tr>
                                            

                                            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_DESC"]["VALUE"])>0):?>

                                                <td>

                                                    <span class="text"><?=$KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_DESC"]["~VALUE"]?></span>

                                                </td>

                                            <?endif;?>

                                            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_PIC"]["VALUE"])>0):?>

                                                 <td>

                                                    <?$logotypeResize = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_PIC"]["VALUE"], array('width'=>100, 'height'=>40), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                                
                                                    <img class="img-responsive" src="<?=$logotypeResize['src']?>" alt="logotype" />

                                                </td>

                                            <?endif;?>

                                            
                                        </tr>
                                    </table>

                                </a>



                            </div>

                        <?endif;?>
                    </div>
                </div>
                
            </div>



            <div class="footer-bot">


                <div class="container">
                    <div class="row">

                        <?
                            $class = "col-lg-12 col-md-12 col-sm-12 col-xs-12";
                            $class2 = "";

                            if($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_ON"]["VALUE"][0] == "Y")
                            {
                                $class = "col-lg-9 col-md-9 col-sm-7 col-xs-12";
                                $class2 = "col-lg-3 col-md-3 col-sm-5 col-xs-12";
                            }

                        ?>

                        <div class="<?=$class?> left">


                            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_INFO"]["VALUE"])>0):?>

                                <div class="top-text"><?=$KRAKEN_TEMPLATE_ARRAY["FOOTER_INFO"]["~VALUE"]?></div>

                            <?endif;?>


                        </div>
                        

                        <?if($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_ON"]["VALUE"][0] == "Y"):?>
                            <div class="<?=$class2?> right">

                                <?if($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_TYPE"]["VALUE"] == "user"):?>

                                    <!--<a<?/*if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_URL"]["VALUE"])>0):?> href="<?=$KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_URL"]["VALUE"]?>" target="_blank"<?endif;?> class="copyright">

                                    <table>
                                        <tr>
                                            

                                            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_DESC"]["VALUE"])>0):?>

                                                <td>

                                                    <span class="text"><?=$KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_DESC"]["~VALUE"]?></span>

                                                </td>

                                            <?endif;?>

                                            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_PIC"]["VALUE"])>0):?>

                                                 <td>

                                                    <?$logotypeResize = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY["FOOTER_COPYRIGHT_USER_PIC"]["VALUE"], array('width'=>100, 'height'=>40), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                                
                                                    <img class="img-responsive" src="<?=$logotypeResize['src']?>" alt="logotype" />

                                                </td>

                                            <?endif;*/?>

                                            
                                        </tr>
                                    </table>

                                    </a>-->

                                <?else:?>

                                    <a href="http://marketplace.1c-bitrix.ru/solutions/concept.kraken/" target="_blank" class="copyright">

                                        <table>
                                            <tr>
                                                <td>

                                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/copyright_kraken.png" alt="copyright">

                                                <td>
                                            </tr>
                                        </table>

                                    </a>
                                <?endif;?>

                            </div>

                        <?endif;?>

                    </div>
                </div>
            </div>

        </footer>

    <?endif;?>

    
    <?if($KRAKEN_TEMPLATE_ARRAY["WIDGET_FAST_CALL_ON"]["VALUE"][0]):?>
        <div id="callphone-mob" class="callphone-wrap">
            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["WIDGET_FAST_CALL_DESC"]["VALUE"])>0):?>
                <span class="callphone-desc"><?=$KRAKEN_TEMPLATE_ARRAY["WIDGET_FAST_CALL_DESC"]["VALUE"]?></span>
            <?endif;?>
            <a class='callphone' href='tel:<?=$KRAKEN_TEMPLATE_ARRAY["WIDGET_FAST_CALL_NUM"]["VALUE"]?>'></a>
        </div>
    <?endif;?>

</div> <!-- /wrapper -->







<?
if($KRAKEN_TEMPLATE_ARRAY["CART_ON"]["VALUE"][0] == "Y")
{
    $APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/include/cart.php",
        array(),
        array("MODE"=>"text")
    );
}
?>

<?\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("set-area");?>

    <?if($USER->isAdmin() || $kraken_rights > "R"):?>

        <?\Bitrix\Main\Data\StaticHtmlCache::getInstance()->markNonCacheable();?>

        <?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/include/alert_errors.php",
            array(),
            array("MODE"=>"text")
        );?>

        <?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/include/settings_panel.php",
            array(),
            array("MODE"=>"text")
        );?>

    <?endif;?>




<?
    $url = basename($_SERVER['REQUEST_URI']);
    $url_components = parse_url($url); 
    parse_str($url_components['query'], $params);


    if(isset($params['utm_source']) || isset($params['utm_medium']) || isset($params['utm_campaign']) || isset($params['utm_content']) || isset($params['utm_term']))
    {
        $APPLICATION->set_cookie("UTM_SOURCE_".SITE_ID, "", -1, "/", false,false,true,"kraken");

        $APPLICATION->set_cookie("UTM_MEDIUM_".SITE_ID, "", -1, "/", false,false,true,"kraken");

        $APPLICATION->set_cookie("UTM_CAMPAIGN_".SITE_ID, "", -1, "/", false,false,true,"kraken");

        $APPLICATION->set_cookie("UTM_CONTENT_".SITE_ID, "", -1, "/", false,false,true,"kraken");

        $APPLICATION->set_cookie("UTM_TERM_".SITE_ID, "", -1, "/", false,false,true,"kraken");
    }

    if(isset($params['utm_source']))
        $APPLICATION->set_cookie("UTM_SOURCE_".SITE_ID, $params['utm_source'], time()+60*60*24*14, "/", false,false,true,"kraken");

    if(isset($params['utm_medium']))
        $APPLICATION->set_cookie("UTM_MEDIUM_".SITE_ID, $params['utm_medium'], time()+60*60*24*14, "/", false,false,true,"kraken");

    if(isset($params['utm_campaign']))
        $APPLICATION->set_cookie("UTM_CAMPAIGN_".SITE_ID, $params['utm_campaign'], time()+60*60*24*14, "/", false,false,true,"kraken");

    if(isset($params['utm_content']))
        $APPLICATION->set_cookie("UTM_CONTENT_".SITE_ID, $params['utm_content'], time()+60*60*24*14, "/", false,false,true,"kraken");

    if(isset($params['utm_term']))
        $APPLICATION->set_cookie("UTM_TERM_".SITE_ID, $params['utm_term'], time()+60*60*24*14, "/", false,false,true,"kraken");


?>



<?\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("set-area");?>



<div class="shadow-detail"></div>


<div class="modalArea shadow-modal-wind-contact"> 
    <div class="shadow-modal"></div>

    <div class="kraken-modal window-modal">

        <div class="kraken-modal-dialog">
            
            <div class="dialog-content">
                <a class="close-modal wind-close"></a>

                <div class="content-in">
                    <div class="list-contacts-modal">
                        <table>

                            <?foreach($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"] as $key => $val):?>
                            
                                <tr>
                                    <td>
                                        <div class="phone bold">
                                            <a href="tel:<?=$val['name']?>"><?=$val['name']?></a>
                                        </div>
                                        <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][$key]["desc"]) > 0):?>
                                            <div class="desc"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][$key]["desc"]?></div>
                                        <?endif;?>
                                    </td>
                                </tr>

                            <?endforeach;?>
                            <?foreach($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"] as $key => $val):?>

                                <tr>
                                    <td>
                                        <div class="email"><a href="mailto:<?=$val['name']?>"><span class="bord-bot"><?=$val['name']?></span></a></div>
                                        <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][$key]["desc"]) > 0):?>
                                            <div class="desc"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][$key]["desc"]?></div>
                                        <?endif;?>
                                    </td>
                                </tr>


                            <?endforeach;?>
                            
                            <?if($KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][0] == 'Y' && (strlen($KRAKEN_TEMPLATE_ARRAY["VK"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["FB"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["TW"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["YOUTUBE"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["INST"]['VALUE'])>0)):?>
                                <tr>
                                    <td>
                                        <?CKraken::CreateSoc($KRAKEN_TEMPLATE_ARRAY)?>
                                    </td>
                                </tr>
                            
                            <?endif;?>

                                
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


<a href="#body" class="up scroll"></a>


<?
    $arMask["rus"] = "+7 (999) 999-99-99";
    $arMask["ukr"] = "+380 (99) 999-99-99";
    $arMask["blr"] = "+375 (99) 999-99-99";
    $arMask["kz"] = "+7 (999) 999-99-99";
    $arMask["user"] = $KRAKEN_TEMPLATE_ARRAY['USER_MASK']["VALUE"];
?>

<script type="text/javascript">
    $(document).on("focus", "form input.phone", 
        function()
        { 
            /*if(!device.android())*/
                $(this).mask("<?=$arMask[$KRAKEN_TEMPLATE_ARRAY['MAIN_MASK']["VALUE"]]?>");
        }
    );
</script>

<input class="domen-url-for-cookie" type="hidden" value="<?=CKraken::getHostTranslit()?>">

<?for($i=1;$i<=10;$i++):?>
    <input type="hidden" id="custom-input-<?=$i?>" name="custom-input-<?=$i?>" value="">
<?endfor;?>


<?if($KRAKEN_TEMPLATE_ARRAY["USE_RUB"]["VALUE"][0]=="Y"):?>

    <script>
        $(window).on("load", function()
        {
            $("body").append("<link href=\"https://fonts.googleapis.com/css?family=PT+Sans+Caption&amp;display=swap&amp;subset=latin-ext\" type=\"text/css\" rel=\"stylesheet\">");
        });
        
    </script>

<?endif;?>


<?if($KRAKEN_TEMPLATE_ARRAY["LAZY_SERVICE"]["VALUE"][0] == "Y" && isset($KRAKEN_TEMPLATE_ARRAY["LAZY_SERVICES_JS"]{0})):?>
    
    <script>

        $(window).on("load", function()
        {
            var timerService = setTimeout(function()
            {
                $("body").append('<?=str_replace(array("/","'", "\r\n"), array("\/", '"', ""), $KRAKEN_TEMPLATE_ARRAY["LAZY_SERVICES_JS"])?>');
                clearTimeout(timerService);
            },  <?=intval($KRAKEN_TEMPLATE_ARRAY["LAZY_SERVICE_TIME"]["VALUE"])?> * 1000);

        });
    </script>

<?endif;?>

<?if(isset($KRAKEN_TEMPLATE_ARRAY["LAZY_SCRIPTS"]['VALUE']{0})):?>

    <script>
        $(window).on("load", function()
        {
            var timerJs = setTimeout(function()
            {
                $("body").append('<?=str_replace(array("/","'", "\r\n"), array("\/", '"', ""), $KRAKEN_TEMPLATE_ARRAY["LAZY_SCRIPTS"]["~VALUE"])?>');
                clearTimeout(timerJs);
            },  <?=intval($KRAKEN_TEMPLATE_ARRAY["LAZY_SCRIPTS_TIME"]['VALUE'])?> * 1000);

        });
    </script>

<?endif;?>

<script>
    var globalGoals = <?=CUtil::PhpToJSObject($KRAKEN_TEMPLATE_ARRAY['GOALS_JS'], false, true)?>;
</script>


<?=$KRAKEN_TEMPLATE_ARRAY["INCLOSEBODY"]['~VALUE'];?>

<?$APPLICATION->IncludeComponent("concept:kraken.seo", "", Array());?>

<?if(strlen($KRAKEN_TEMPLATE_ARRAY['STYLES']['VALUE'])>0)
{?>
    <style type="text/css">
    <?=$KRAKEN_TEMPLATE_ARRAY['STYLES']['~VALUE']?>
    </style>
    
<?}
    /*if(strlen($KRAKEN_TEMPLATE_ARRAY["INCLOSEBODY"]["VALUE"]) > 0)
        echo $KRAKEN_TEMPLATE_ARRAY["INCLOSEBODY"]["~VALUE"];*/

    if(strlen($KRAKEN_TEMPLATE_ARRAY['SCRIPTS']['VALUE'])>0)
    {?>
        <script type='text/javascript'>
            <?=$KRAKEN_TEMPLATE_ARRAY['SCRIPTS']['~VALUE'];?>
        </script>
    <?}

    

    $APPLICATION->ShowViewContent("service_close_body");

    
?>
<?if($KRAKEN_TEMPLATE_ARRAY['CART_ADD_ON']['VALUE'][0]=="Y"):?>
    <div class="first-click-show-basket"></div>
<?endif;?>

</body>
</html>
<? 
	$GLOBALS["KEYWORDS"] = "";
	$APPLICATION->SetPageProperty("keywords", ""); 
?>