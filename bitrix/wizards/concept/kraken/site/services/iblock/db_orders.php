<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
global $DB;

$strSql = "SHOW TABLES LIKE 'kraken_shop_orders_".WIZARD_SITE_ID."'";
$res = $DB->Query($strSql, false, $err_mess.__LINE__);

if($res->SelectedRowsCount() <= 0)
{
    $strSql = 
    "
        CREATE TABLE
            `kraken_shop_orders_".WIZARD_SITE_ID."` (
                `ID` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `DATETIME` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `SUM` FLOAT NULL DEFAULT NULL,
                `CURRENCY` CHAR(255) NULL DEFAULT NULL,
                `STATUS` INT(11) NULL DEFAULT NULL,
                `PROPERTIES` TEXT NULL DEFAULT NULL,
                `NAME` CHAR(255) NULL DEFAULT NULL,
                `EMAIL` CHAR(255) NULL DEFAULT NULL,
                `PHONE` CHAR(255) NULL DEFAULT NULL,
                `PAYED` CHAR(1) NOT NULL DEFAULT 'N',
                `DELIVERY` INT(11) NULL DEFAULT NULL,
                `DELIVERY_SUM` FLOAT NULL DEFAULT NULL,
                `DELIVERY_COMMENT` TEXT NULL DEFAULT NULL,
                `PAYMENT` INT(11) NULL DEFAULT NULL,
                `FILES` CHAR(255) NULL DEFAULT NULL
            )            
    ";
    
    $DB->Query($strSql, false, $err_mess.__LINE__);
    
}
?>