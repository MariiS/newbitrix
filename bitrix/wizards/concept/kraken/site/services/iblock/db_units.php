<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
global $DB;

$strSql = "SHOW TABLES LIKE 'kraken_shop_units'";
$res = $DB->Query($strSql, false, $err_mess.__LINE__);

if($res->SelectedRowsCount() <= 0)
{
    $strSql = 
    "
        CREATE TABLE
            `kraken_shop_units` (
                `ID` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `NAME` CHAR(255) NULL DEFAULT NULL,
                `SYM_PRICE` CHAR(255) NULL DEFAULT NULL,
                `SORT` INT(11) NOT NULL DEFAULT '500',
                `SYM_MAIN` CHAR(255) NULL DEFAULT NULL
            )            
    ";
    
    $DB->Query($strSql, false, $err_mess.__LINE__);
    




    include("lang/ru/db_units_lang.php");
    
    $strSql = "INSERT INTO kraken_shop_units (NAME, SYM_PRICE, SORT, SYM_MAIN) VALUES ('".$MESS["UNIT_1_NAME"]."','".$MESS["UNIT_1_SYM_PRICE"]."', 10, '".$MESS["UNIT_1_SYM"]."')";
    $DB->Query($strSql, false, $err_mess.__LINE__);
    
    $strSql = "INSERT INTO kraken_shop_units (NAME, SYM_PRICE, SORT, SYM_MAIN) VALUES ('".$MESS["UNIT_2_NAME"]."','".$MESS["UNIT_2_SYM_PRICE"]."', 20, '".$MESS["UNIT_2_SYM"]."')";
    $DB->Query($strSql, false, $err_mess.__LINE__);
    
    $strSql = "INSERT INTO kraken_shop_units (NAME, SYM_PRICE, SORT, SYM_MAIN) VALUES ('".$MESS["UNIT_3_NAME"]."','".$MESS["UNIT_3_SYM_PRICE"]."', 30, '".$MESS["UNIT_3_SYM"]."')";
    $DB->Query($strSql, false, $err_mess.__LINE__);
    
    $strSql = "INSERT INTO kraken_shop_units (NAME, SYM_PRICE, SORT, SYM_MAIN) VALUES ('".$MESS["UNIT_4_NAME"]."','".$MESS["UNIT_4_SYM_PRICE"]."', 40, '".$MESS["UNIT_4_SYM"]."')";
    $DB->Query($strSql, false, $err_mess.__LINE__);
    
    $strSql = "INSERT INTO kraken_shop_units (NAME, SYM_PRICE, SORT, SYM_MAIN) VALUES ('".$MESS["UNIT_5_NAME"]."','".$MESS["UNIT_5_SYM_PRICE"]."', 50, '".$MESS["UNIT_5_SYM"]."')";
    $DB->Query($strSql, false, $err_mess.__LINE__);
    
    $strSql = "INSERT INTO kraken_shop_units (NAME, SYM_PRICE, SORT, SYM_MAIN) VALUES ('".$MESS["UNIT_6_NAME"]."','".$MESS["UNIT_6_SYM_PRICE"]."', 60, '".$MESS["UNIT_6_SYM"]."')";
    $DB->Query($strSql, false, $err_mess.__LINE__);

}
?>