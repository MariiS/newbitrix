<?
$MESS["pdd_news_TYPE_NAME"] = "Автошкола: новости";
$MESS["pdd_news_ELEMENT_NAME"] = "Элементы";
$MESS["pdd_news_SECTION_NAME"] = "Разделы";

$MESS["pdd_feedback_TYPE_NAME"] = "Автошкола: обратная связь";
$MESS["pdd_feedback_ELEMENT_NAME"] = "Элементы";
$MESS["pdd_feedback_SECTION_NAME"] = "Разделы";

$MESS["pdd_TYPE_NAME"] = "Автошкола: справочники";
$MESS["pdd_ELEMENT_NAME"] = "Элементы";
$MESS["pdd_SECTION_NAME"] = "Разделы";

$MESS["pdd_gallery_TYPE_NAME"] = "Автошкола: галереи";
$MESS["pdd_gallery_ELEMENT_NAME"] = "Элементы";
$MESS["pdd_gallery_SECTION_NAME"] = "Разделы";

$MESS["MESSAGE"] = "Текст сообщения";
$MESS["PASSPORT"] = "Паспортные данные";

$MESS["EMAIL"] = "E-mail";
$MESS["NAME"] = "ФИО";
$MESS["PHONE"] = "Телефон";
$MESS["IP"] = " IP пользователя";

$MESS["EVENT_NAME_1"] = "Вопрос - ответ";
$MESS["EVENT_NAME_2"] = "Запись на курсы";
/*-----------------------------------------------------------*/

$MESS["el_news_TYPE_NAME"] = "Новости";
$MESS["el_news_ELEMENT_NAME"] = "Элементы";
$MESS["el_news_SECTION_NAME"] = "Разделы";

$MESS["el_products_TYPE_NAME"] = "Товары";
$MESS["el_products_ELEMENT_NAME"] = "Элементы";
$MESS["el_products_SECTION_NAME"] = "Разделы";

$MESS["concept_remont2_TYPE_NAME"] = "Ремонт 2.0";
$MESS["concept_remont2_ELEMENT_NAME"] = "Элементы";
$MESS["concept_remont2_SECTION_NAME"] = "Разделы";



?>