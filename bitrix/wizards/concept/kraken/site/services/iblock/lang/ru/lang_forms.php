<?
$MESS["KRAKEN_SECTION_1"] = "Раздел";
$MESS["KRAKEN_SECTION_2"] = "Сортировка";
$MESS["KRAKEN_SECTION_3"] = "*Название";
$MESS["KRAKEN_SECTION_4"] = "Описание";




$MESS["KRAKEN_ELEMENT_HINT_FORM_BUTTON"] = "Поле поддерживает любые надписи";
$MESS["KRAKEN_ELEMENT_HINT_FORM_INPUTS_REQ"] = "Форма не будет отправлена, пока человек их не заполнит";
$MESS["KRAKEN_ELEMENT_HINT_FORM_THANKS"] = "Этот текст будет показан после отправки формы";
$MESS["KRAKEN_ELEMENT_HINT_FORM_TO_LINK"] = "Вставьте в это поле полный адрес страницы (включая http://).  Посетитель будет автоматически перенаправлен туда после  заполнения и отправки формы.";
$MESS["KRAKEN_ELEMENT_HINT_FORM_THEME"] = "Не забывайте, что для отправки письма нужен e-mail посетителя,  поэтому, выберите в настройках формы поле «e-mail»";
$MESS["KRAKEN_ELEMENT_HINT_FORM_FILES"] = "Не прикрепляйте больших файлов (больше 2 МБ). Не забывайте,  что для отправки файла нужен e-mail посетителя, поэтому,  выберите в настройках формы захвата поле «e-mail»";
$MESS["KRAKEN_ELEMENT_HINT_YANDEX_GOAL"] = "Этот ID нужно использовать при создании цели с «JavaScript-  событием» в Яндекс.Метрике. Используйте латиницу";
$MESS["KRAKEN_ELEMENT_HINT_FORM_RADIOCHECK"] = "«Радиобаттоны» - можно выбрать только что-то одно.  «Чекбоксы» - можно выбрать несколько.";
$MESS["KRAKEN_ELEMENT_HINT_FORM_LIST"] = "Это свойство даёт возможность предложить пользователю  выбрать один или несколько вариантов из предложенных  вами";
$MESS["KRAKEN_ELEMENT_HINT_GOOGLE_GOAL_CATEGORY"] = "Значение поля Category «цели-события» в  Google Analytics. Используйте латиницу";
$MESS["KRAKEN_ELEMENT_HINT_GTM_GOAL_CATEGORY"] = "Значение поля Category «цели-события»  Google Tag Manager. Используйте латиницу";
$MESS["KRAKEN_ELEMENT_HINT_GOOGLE_GOAL_ACTION"] = "Значение поля Action «цели-события» в  Google Analytics. Используйте латиницу";
$MESS["KRAKEN_ELEMENT_HINT_GTM_GOAL_ACTION"] = "Значение поля Action «цели-события» Google Tag Manager. Используйте латиницу";
$MESS["KRAKEN_ELEMENT_HINT_COVER"] = "Эта настройка масштабирует весь фон под дополнительной информацией";
$MESS["KRAKEN_ELEMENT_HINT_GTM_EVENT"] = "Значение поля Event «цели-события» Google Tag Manager. Используйте латиницу";
$MESS["KRAKEN_ELEMENT_HINT_FORM_ADMIN"] = "Инструкция по созданию индивидуального набора полей - https://goo.gl/ffvH6d";
$MESS["KRAKEN_ELEMENT_HINT_FORM_PROP_INPUTS"] = "Инструкция по созданию индивидуального набора полей - https://goo.gl/ffvH6d";
$MESS["KRAKEN_ELEMENT_HINT_FORM_BGC"] = "Указывайте код цвета в формате RGB – «#FFAA55». Генератор цветов - https://goo.gl/8EW7uf ";
$MESS["KRAKEN_ELEMENT_HINT_FORM_JS_AFTER_SEND"] = "js код, который сработает после отправки формы. Тег <script> указывать НЕ нужно";
$MESS["KRAKEN_ELEMENT_HINT_FORM_BUTTON_BG_COLOR"] = "Можно указать код цвета в цифровом формате (пример — #000), можно в формате RGBa (пример — rgba(0,0,0,0.5)";
$MESS["KRAKEN_ELEMENT_HINT_EMAIL_TO"] = "На этот адрес будут приходить заявки (можно указывать через запятую несколько адресов). Если поле оставить пустым, то заявки будут отправлены на адреса, указанные в общих настройках сайта.";
$MESS["KRAKEN_ELEMENT_HINT_DUBLICATE"] = "Письма будут доставлены, в том числе, на почтовые адреса, указанные в общих настройках сайта";
?>
