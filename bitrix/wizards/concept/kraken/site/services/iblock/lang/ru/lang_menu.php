<?
$MESS["KRAKEN_SECTION_1"] = "Настройки пункта меню";
$MESS["KRAKEN_SECTION_2"] = "Показывать на сайте";
$MESS["KRAKEN_SECTION_3"] = "*Название пункта меню";
$MESS["KRAKEN_SECTION_4"] = "Вид меню";
$MESS["KRAKEN_SECTION_5"] = "Порядок пункта в меню";
$MESS["KRAKEN_SECTION_6"] = "*Действие при нажатии";
$MESS["KRAKEN_SECTION_7"] = "Выберите лендинг";
$MESS["KRAKEN_SECTION_8"] = "Выберите форму захвата";
$MESS["KRAKEN_SECTION_9"] = "Выберите модальное окно";
$MESS["KRAKEN_SECTION_10"] = "Укажите ID ЛИД-опроса";
$MESS["KRAKEN_SECTION_11"] = "Укажите ссылку для перехода";
$MESS["KRAKEN_SECTION_12"] = "Открывать в новом окне";
$MESS["KRAKEN_SECTION_13"] = "Настройки для первого уровня меню";
$MESS["KRAKEN_SECTION_14"] = "Цвет пункта меню";
$MESS["KRAKEN_SECTION_15"] = "Иконка для пункта меню";
$MESS["KRAKEN_SECTION_16"] = "Своя иконка";
$MESS["KRAKEN_SECTION_17"] = "В какую колонку во всплывающем меню поместить этот пункт?";
$MESS["KRAKEN_SECTION_18"] = "Настройки для пунктов меню второго уровня при \"виде 2\"";
$MESS["KRAKEN_SECTION_19"] = "Картинка для анонса страницы раздела";


$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_LINK"] = "При указании ссылки внутри сайта указывайте только содержимое, которое идёт после названия домена - /catalog/. Ссылка на другой сайт должна начинаться с http:// или https://";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_COL"] = "Всплывающее меню появляется при нажатии на иконку «бургера» (три полосочки)";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_COLOR"] = "Указывайте код цвета в формате RGB – «#FFAA55». Генератор цветов - https://goo.gl/8EW7uf ";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_ICON"] = "Коллекция векторных иконок - https://goo.gl/NuRJZf";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_IC_US"] = "Рекомендуем использовать иконку на прозрачном фоне";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_TYPE"] = "Используйте вариант «перейти по ссылке» только в крайнем случае, так он может привести к возникновению 404 ошибок в будущем.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_M_FORM"] = "Формы добавляются и редактируются в разделе \"Формы захвата\"";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_M_MODAL"] = "Модальные окна добавляются и редактируются в разделе \"Модальные окна\"";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_M_BLANK"] = "Используйте для открытия ссылок на другие сайты";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_M_QUIZ"] = "Конструктор ЛИД-опросов приобретается отдельно - https://goo.gl/n3xa1s ";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_LAND"] = "Лендинги создаются и редактируются в разделе «Конструктор лендингов» или в публичной части сайта";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MENU_PICT"] = "Применяется для пунктов меню, у которых первый уровень не является формируемым из разделов инфоблоков";


$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_LINK"] = "Укажите ссылку для перехода";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_COL"] = "В какую колонку во всплывающем меню поместить этот пункт?";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_COLOR"] = "Цвет пункта меню";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_ICON"] = "ID иконки из коллекции";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_IC_US"] = "Своя иконка";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_TYPE"] = "Действие при нажатии";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_M_FORM"] = "Выберите форму захвата";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_M_MODAL"] = "Выберите модальное окно";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_M_BLANK"] = "Открывать в новом окне";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_M_QUIZ"] = "Укажите ID ЛИД-опроса";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_LAND"] = "Выберите лендинг";
$MESS["KRAKEN_SECTION_NAME_UF_MENU_VIEW"] = "Вид меню";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_PICT"] = "Картинка для меню";


$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_KRAKEN_M_BLANK"] = "0";
?>