<?
$MESS["KRAKEN_SECTION_1"] = "Раздел";
$MESS["KRAKEN_SECTION_2"] = "Раздел активен";
$MESS["KRAKEN_SECTION_3"] = "*Название";
$MESS["KRAKEN_SECTION_4"] = "Сортировка";






?>

<?


$MESS["KRAKEN_ELEMENT_HINT_BUTTON_FORM"] = "Формы добавляются и редактируются в разделе «Формы захвата»";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_LINK"] = "Для перехода к другому блоку, укажите в этом поле  ID блока (только цифру), куда должен произойти переход. Ссылка на другой сайт должна обязательно начинаться с протокола http:// или https:// ";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_MODAL"] = "Модальные окна добавляются и редактируются в разделе «Модальные окна»";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_QUIZ"] = "Работает при установленном модуле \"Лид-Опросы\" https://goo.gl/n3xa1s ";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_ONCLICK"] = "Только для профессионалов. В это поле вы можете добавить полный код события \"onclick\" для создания цели \"нажатия на кнопку\" для систем аналитики";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_CATALOG"] = "Для действия \"Быстрый заказ\" или \"Добавить в корзину\"";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_OFFER_ID"] = "Необходимо указать основной товар в предыдущем поле, к которому относится торговое предложение";
?>