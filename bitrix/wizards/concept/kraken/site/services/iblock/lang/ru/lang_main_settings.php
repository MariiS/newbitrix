<?
$MESS["KRAKEN_DEF_RUB"] = "руб";
$MESS["KRAKEN_DEF_USD"] = "USD";
$MESS["KRAKEN_DEF_EUR"] = "EUR";
$MESS["KRAKEN_DEF_BYR"] = "руб";
$MESS["KRAKEN_DEF_UAH"] = "грн";
$MESS["KRAKEN_DEF_KZT"] = "тнг";

$MESS["KRAKEN_DEF_CALLBACK_NAME"] = "Заказать звонок";
$MESS["KRAKEN_DEF_CTLG_BTN"] = "Быстрый заказ";

$MESS["KRAKEN_DEF_NW_MORE"] = "Подробнее";
$MESS["KRAKEN_DEF_CTLG_MORE"] = "Узнать больше";

$MESS["KRAKEN_DEF_DESCRIPTION"] = "Конструктор лендинговых сайтов";
$MESS["KRAKEN_DEF_POLITIC_DESC"] = "Даю своё согласие на законную обработку персональных данных. ";


$MESS["KRAKEN_DEF_PHONE"] = "+0 000 000-00-00";
$MESS["KRAKEN_DEF_PHONE_DESC"] = "Консультации по решению";
$MESS["KRAKEN_DEF_EMAIL"] = "myemail@sendme.ru";
$MESS["KRAKEN_DEF_EMAIL_DESC"] = "Техническая поддержка";
$MESS["KRAKEN_DEF_CATALOG_DESC"] = "Неограниченное количество категорий, уровней каталога и товаров. Автоматическая SEO-оптимизация принесёт бесплатных посетителей. Страницы категорий и товаров могут быть лендингами с индивидуальных дизайном, которые превратят посетителей в покупателей.";

$MESS["KRAKEN_DEF_BLOG_DESC"] = "Этот раздел отлично способствует SEO-продвижению, тем более, что статьи могут быть оформлены в виде красивых и удобных лендингов с отличными поведенческими факторами.";
$MESS["KRAKEN_DEF_NEWS_DESC"] = "Самые значимые события компании, обновления ассортимента и просто корпоративная жизнь";
$MESS["KRAKEN_DEF_FOOTER_DESC"] = "Конструктор лендинговых сайтов КРАКЕН позволяет без программиста создавать многостраничные сайты, на которых абсолютно любая страница может быть полноценным лендингом с уникальным дизайном.&lt;br&gt;&lt;br&gt;";
$MESS["KRAKEN_DEF_FOOTER_INFO"] = "© Готовое решение для 1С-Битрикс";

$MESS["KRAKEN_DEF_MAIL"] = "noreply@sendmail.com";
$MESS["KRAKEN_DEF_POLITIC_DESC"] = "Даю своё согласие на законную обработку персональных данных. ";


$MESS["KRAKEN_DEF_SITE"] = "Кракен";

$MESS["KRAKEN_DEF_WIDGET_FAST_CALL_NUM"] = "+79991112392";
$MESS["KRAKEN_DEF_WIDGET_FAST_CALL_DESC"] = "Нажмите для звонка";


$MESS["KRAKEN_DEF_CART_HEAD_TITLE"] = "Ваш заказ готов к оформлению";

$MESS["KRAKEN_DEF_CART_BTN_ADD_NAME"] = "Добавить в корзину";
$MESS["KRAKEN_DEF_CART_BTN_ADDED_NAME"] = "Товар в корзине";
$MESS["KRAKEN_DEF_CART_BTN_ORDER_NAME"] = "Оформить заказ";
$MESS["KRAKEN_DEF_CART_BTN_FAST_ORDER_NAME"] = "Быстрый заказ в 1 клик";
$MESS["KRAKEN_DEF_CART_COMMENT"] = "Гарантированный подарок каждому, кто оформит заказ на сумму более 500 рублей!";
$MESS["KRAKEN_DEF_CART_MESS_THEME_ADMIN"] = "Ура! Новый заказ № #ID_ORDER# с сайта #SITE_NAME#";
$MESS["KRAKEN_DEF_CART_MESS_ADMIN"] = "Поздравляем! &lt;br/&gt; На вашем сайте &laquo;#SITE_NAME#&raquo; был оформлен заказ через корзину. &lt;br/&gt; Адрес страницы: #URL# &lt;br/&gt; &lt;br/&gt; #FORM_INFO# &lt;br/&gt; #CART_INFO# &lt;br/&gt; &lt;b&gt;Номер заказа:&lt;/b&gt; #ID_ORDER# #CART_TABLE# &lt;br/&gt; #TOTAL_INFO# &lt;br/&gt; &lt;br/&gt; &lt;br/&gt; #PAYMENT_BUTTON#";
$MESS["KRAKEN_DEF_CART_MESS_THEME_USER"] = "Ваш заказ № #ID_ORDER# оформлен";
$MESS["KRAKEN_DEF_CART_MESS_USER"] = "Здравствуйте! &lt;br&gt; Ваш заказ принят. В ближайшее время мы свяжемся с вами для его подтверждения. &lt;br&gt; &lt;br&gt; &lt;b&gt;Номер заказа:&lt;/b&gt; #ID_ORDER# &lt;br&gt; #CART_TABLE# &lt;br&gt; #CART_INFO# &lt;br&gt; &lt;br&gt; #TOTAL_INFO# &lt;br&gt; &lt;br/&gt; &lt;br/&gt; #PAYMENT_BUTTON# &lt;br&gt; &lt;br&gt; ---&lt;br&gt; Благодарим, что выбрали нас!";
$MESS["KRAKEN_DEF_CART_BTN_NAME_CONDITIONS"] = "Условия доставки и оплаты";
$MESS["KRAKEN_DEF_CART_PAGE_TITLE"] = "Ваш заказ готов к оформлению";
$MESS["KRAKEN_DEF_CART_EMPTY_MESS"] = "Увы, в вашей корзине пусто :(&lt;br&gt;&lt;br&gt;&lt;a class=&quot;button-def main-color elips&quot; href=&quot;/catalog/&quot;&gt;Перейти в каталог&lt;/a&gt;";
$MESS["KRAKEN_DEF_CART_ORDER_COMPLITED_MESS"] = "Ваш заказ № #ID_ORDER# успешно оформлен!&lt;br/&gt; Наши менеджеры свяжутся с Вами в ближайшее время!&lt;br/&gt;&lt;br/&gt; &lt;table&gt; &lt;tr&gt; &lt;td style=&quot;padding-right: 20px;&quot;&gt;#PAYMENT_BUTTON#&lt;/td&gt; &lt;td&gt;&lt;a class=&quot;button-def main-color elips&quot; href=&quot;/&quot;&gt;Перейти на главную страницу&lt;/a&gt;&lt;/td&gt; &lt;/tr&gt; &lt;/table&gt;";



$MESS["KRAKEN_DEF_CART_PAY_MESS_THEME_ADMIN"] = "Заказ № #ID_ORDER# оплачен клиентом";
$MESS["KRAKEN_DEF_CART_PAY_MESS_ADMIN"] = "Заказ № #ID_ORDER# успешно оплачен клиентом. &lt;b&gt;Информация об оплаченном заказе:&lt;/b&gt; #CART_TABLE# #CART_INFO# #TOTAL_INFO#";
$MESS["KRAKEN_DEF_CART_PAY_MESS_THEME_USER"] = "Ваш заказ № #ID_ORDER# успешно оплачен";
$MESS["KRAKEN_DEF_CART_PAY_MESS_ADMIN"] = "Ваш заказ № #ID_ORDER# успешно оплачен. &lt;b&gt;Информация о заказе:&lt;/b&gt; #CART_TABLE# #CART_INFO# #TOTAL_INFO#";
?>