<?
$MESS["DELIVERY_1_NAME"] = "Самовывоз";
$MESS["DELIVERY_1_NAME_ON_SITE"] = "Самовывоз";
$MESS["DELIVERY_1_DESCRIPTION"] = "Со склада в Москве по адресу Котельническая набережная, д.18, стр.4";
$MESS["DELIVERY_1_PRICE"] = "0";
$MESS["DELIVERY_1_CURRENCY"] = "RUB";
$MESS["DELIVERY_1_ADD_FIELD"] = "";
$MESS["DELIVERY_1_ADD_FIELD_NAME"] = "";
$MESS["DELIVERY_1_ADD_FIELD_HEIGHT"] = "0";

$MESS["DELIVERY_2_NAME"] = "Курьером внутри МКАД";
$MESS["DELIVERY_2_NAME_ON_SITE"] = "Курьером внутри МКАД";
$MESS["DELIVERY_2_DESCRIPTION"] = "";
$MESS["DELIVERY_2_PRICE"] = "200";
$MESS["DELIVERY_2_CURRENCY"] = "RUB";
$MESS["DELIVERY_2_ADD_FIELD"] = "Y";
$MESS["DELIVERY_2_ADD_FIELD_NAME"] = "Адрес доставки и пояснения для курьера";
$MESS["DELIVERY_2_ADD_FIELD_HEIGHT"] = "5";

$MESS["DELIVERY_3_NAME"] = "Курьером за МКАД";
$MESS["DELIVERY_3_NAME_ON_SITE"] = "Курьером за МКАД";
$MESS["DELIVERY_3_DESCRIPTION"] = "Стоимость будет рассчитана нашим оператором после оформления заказа";
$MESS["DELIVERY_3_PRICE"] = "0";
$MESS["DELIVERY_3_CURRENCY"] = "RUB";
$MESS["DELIVERY_3_ADD_FIELD"] = "Y";
$MESS["DELIVERY_3_ADD_FIELD_NAME"] = "Адрес доставки";
$MESS["DELIVERY_3_ADD_FIELD_HEIGHT"] = "3";

$MESS["DELIVERY_4_NAME"] = "Почтой России";
$MESS["DELIVERY_4_NAME_ON_SITE"] = "Почтой России";
$MESS["DELIVERY_4_DESCRIPTION"] = "Срок доставки 5-10 дней. Стоимость будет рассчитана после оформления заказа.";
$MESS["DELIVERY_4_PRICE"] = "0";
$MESS["DELIVERY_4_CURRENCY"] = "RUB";
$MESS["DELIVERY_4_ADD_FIELD"] = "Y";
$MESS["DELIVERY_4_ADD_FIELD_NAME"] = "Адрес доставки (не забудьте индекс)";
$MESS["DELIVERY_4_ADD_FIELD_HEIGHT"] = "5";
?>