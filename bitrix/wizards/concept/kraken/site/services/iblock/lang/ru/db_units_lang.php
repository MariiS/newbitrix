<?
$MESS["UNIT_1_NAME"] = "Штука";
$MESS["UNIT_1_SYM"] = "шт.";
$MESS["UNIT_1_SYM_PRICE"] = "/ шт.";

$MESS["UNIT_2_NAME"] = "Метр";
$MESS["UNIT_2_SYM"] = "м";
$MESS["UNIT_2_SYM_PRICE"] = "/ м";

$MESS["UNIT_3_NAME"] = "Килограмм";
$MESS["UNIT_3_SYM"] = "кг";
$MESS["UNIT_3_SYM_PRICE"] = "за кг";

$MESS["UNIT_4_NAME"] = "Упаковка";
$MESS["UNIT_4_SYM"] = "уп.";
$MESS["UNIT_4_SYM_PRICE"] = "за уп.";

$MESS["UNIT_5_NAME"] = "Метр квадратный";
$MESS["UNIT_5_SYM"] = "кв.м";
$MESS["UNIT_5_SYM_PRICE"] = "/ кв.м";

$MESS["UNIT_6_NAME"] = "Час";
$MESS["UNIT_6_SYM"] = "ч.";
$MESS["UNIT_6_SYM_PRICE"] = "/ час";
?>