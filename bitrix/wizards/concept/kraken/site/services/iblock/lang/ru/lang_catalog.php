<?
$MESS["KRAKEN_SECTION_1"] = "Базовые настройки";
$MESS["KRAKEN_SECTION_2"] = "Категория активна";
$MESS["KRAKEN_SECTION_3"] = "Показывать в главном меню";
$MESS["KRAKEN_SECTION_4"] = "Отключить фильтр на странице";
$MESS["KRAKEN_SECTION_5"] = "Не показывать товары из подразделов";
$MESS["KRAKEN_SECTION_6"] = "*Название";
$MESS["KRAKEN_SECTION_7"] = "*Символьный код";
$MESS["KRAKEN_SECTION_8"] = "*Шаблон для страницы категории";
$MESS["KRAKEN_SECTION_9"] = "Выберите лендинг";
$MESS["KRAKEN_SECTION_10"] = "Сортировка";
$MESS["KRAKEN_SECTION_11"] = "Преимущества товаров";
$MESS["KRAKEN_SECTION_12"] = "Дизайн и надписи";
$MESS["KRAKEN_SECTION_13"] = "Картинка для шапки";
$MESS["KRAKEN_SECTION_14"] = "Краткое описание";
$MESS["KRAKEN_SECTION_15"] = "Настройки только для категорий первого уровня";
$MESS["KRAKEN_SECTION_16"] = "Показывать на главной странице каталога";
$MESS["KRAKEN_SECTION_17"] = "Изображение для «живой плитки»";
$MESS["KRAKEN_SECTION_18"] = "Размер «живой плитки»";
$MESS["KRAKEN_SECTION_19"] = "Надпись на кнопке перехода к списку товаров";
$MESS["KRAKEN_SECTION_20"] = "Настройки только для подкатегорий";
$MESS["KRAKEN_SECTION_21"] = "Картинка для меню";
$MESS["KRAKEN_SECTION_22"] = "Основное описание категории";
$MESS["KRAKEN_SECTION_23"] = "*Ширина основного описания";
$MESS["KRAKEN_SECTION_24"] = "Вступительное описание категории";
$MESS["KRAKEN_SECTION_25"] = "Дополнительные баннеры";
$MESS["KRAKEN_SECTION_26"] = "Режим показа баннеров в левой колонке";
$MESS["KRAKEN_SECTION_27"] = "Выберите баннеры";
$MESS["KRAKEN_SECTION_28"] = "Настройки баннера-сотрудника";
$MESS["KRAKEN_SECTION_29"] = "Режим показа баннеров в левой колонке";
$MESS["KRAKEN_SECTION_30"] = "Выберите сотрудника";


$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MAIN_MENU"] = "Настройка актуальна только в случае, если в главном меню есть ссылка «на главную страницу каталога».";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_MAIN_CTLG"] = "Категория будет выводиться в виде «живой плитки»";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_TMPL"] = "Если вы хотите вместо страницы категории использовать лендинг, сначала создайте его в разделе «конструктор лендингов». ";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_T_ID"] = "Лендинги создаются и редактируются в разделе «Конструктор лендингов» или в публичной части сайта";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_BNNRS"] = "Баннеры создаются и редактируются в разделе «Доп. баннеры» ";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_BNRS_VIEW"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте настройки баннеров непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_TOP_T"] = "Размещается на странице категории, выше списка товаров и подкатегорий";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_H1"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_TITLE"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_KWORD"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_CTLG_DSCR"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";


$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MAIN_MENU"] = "Показывать в главном меню";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MAIN_CTLG"] = "Показывать на главной странице каталога";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_SIZE"] = "Размер «живой плитки»";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_BTN"] = "Надпись на кнопке перехода к списку товаров";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_PRTXT"] = "Краткое описание";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_TMPL"] = "Что показывать";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_T_ID"] = "Выберите лендинг";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_BNNRS"] = "Выберите баннеры";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_BNRS_VIEW"] = "Режим показа баннеров в левой колонке";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_TXT_P"] = "Ширина основного описания";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_TOP_T"] = "Вступительное описание категории";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_H1"] = "H1";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_TITLE"] = "Title";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_KWORD"] = "Keywords";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_DSCR"] = "Description";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_PICT"] = "Картинка для страницы раздела";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_CTLG_ADV"] = "Преимущества товаров";
$MESS["KRAKEN_SECTION_NAME_UF_HIDE_SUBSECTIONS"] = "Не показывать товары из подразделов";
$MESS["KRAKEN_SECTION_NAME_UF_EMPL_BANNER"] = "Выберите сотрудника";
$MESS["KRAKEN_SECTION_NAME_UF_EMPL_BANNER_TYPE"] = "Режим показа сотрудника в левой колонке";
$MESS["KRAKEN_SECTION_NAME_UF_USE_FILTER"] = "Отключить фильтр на странице";


$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_KRAKEN_MAIN_MENU"] = "1";
$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_KRAKEN_MAIN_CTLG"] = "1";
$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_HIDE_SUBSECTIONS"] = "0";
$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_USE_FILTER"] = "0";
?>

<?


$MESS["KRAKEN_ELEMENT_HINT_ITEM_TEMPLATE"] = "Если вы хотите вместо страницы товара использовать лендинг, сначала создайте его в разделе «конструктор лендингов». ";
$MESS["KRAKEN_ELEMENT_HINT_CHOOSE_LANDING"] = "Лендинги создаются и редактируются в разделе «Конструктор лендингов» или в публичной части сайта";
$MESS["KRAKEN_ELEMENT_HINT_HEADER_PICTURE"] = "Настройка актуальна только при использовании «стандартного шаблона». ";
$MESS["KRAKEN_ELEMENT_HINT_BANNERS_LEFT"] = "Баннеры создаются и редактируются в разделе «Доп. баннеры» ";
$MESS["KRAKEN_ELEMENT_HINT_BANNERS_LEFT_TYPE"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте настройки баннеров непосредственно у самого лендинга.";
$MESS["KRAKEN_ELEMENT_HINT_CHARS"] = "В левой колонке укажите название характеристики, а в правой её значение. Поддерживается html код";
$MESS["KRAKEN_ELEMENT_HINT_FILES"] = "Не рекомендуется загружать файлы с расширениями .exe и .zip, так как они могут быть восприняты браузером как вирусы";
$MESS["KRAKEN_ELEMENT_HINT_PRICE"] = "Введите целое число без пробелов";
$MESS["KRAKEN_ELEMENT_HINT_OLD_PRICE"] = "Введите целое число без пробелов";
$MESS["KRAKEN_ELEMENT_HINT_CURRENCY"] = "Обозначения для валют задаются в общих настройках сайта";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_NAME"] = "Надпись «по умолчанию» задаётся в общих настройках сайта";
$MESS["KRAKEN_ELEMENT_HINT_ADVANTAGES"] = "Преимущества создаются и редактируются в разделе «преимущества для каталога»";
$MESS["KRAKEN_ELEMENT_HINT_VIDEO"] = "Укажите ссылки на страницы с видеороликами на сайте youtube.com. Пример — https://www.youtube.com/watch?v=J0pjng6t7PA";
$MESS["KRAKEN_ELEMENT_HINT_STUFF"] = "Сотрудники добавляются и редактируются в разделе «Сотрудники»";
$MESS["KRAKEN_ELEMENT_HINT_BLOGS"] = "Блоги добавляются и редактируются в разделе «Блоги»";
$MESS["KRAKEN_ELEMENT_HINT_NEWS"] = "Новости добавляются и редактируются в разделе «Новости»";
$MESS["KRAKEN_ELEMENT_HINT_SPECIALS"] = "Акции добавляются и редактируются в разделе «Акции»";
$MESS["KRAKEN_ELEMENT_HINT_FAQ_FORM"] = "Формы добавляются и редактируются в разделе «Формы захвата»";
$MESS["KRAKEN_ELEMENT_HINT_FAQ_QUESTIONS"] = "Сами вопросы добавляются и редактируются в разделе «Вопрос-ответ (FAQ)»";
$MESS["KRAKEN_ELEMENT_HINT_FILES_DESC"] = "Порядок описаний соответствует порядку файлов. В левой колонке укажите название файла, в правок комментарий";
$MESS["KRAKEN_ELEMENT_HINT_ORDER_FORM"] = "Форма «по умолчанию» задаётся в общих настройках сайта";
$MESS["KRAKEN_ELEMENT_HINT_PARENT_ADV"] = "отметьте, если хотите выводить на карточке товара преимущества, заданные в настройках категории";
$MESS["KRAKEN_ELEMENT_HINT_CURR"] = "Отредактировать и добавить значения можно в \"Настройках сайта\"";
$MESS["KRAKEN_ELEMENT_HINT_UNITS"] = "Отредактировать и добавить значения можно в \"Настройках сайта\"";
$MESS["KRAKEN_ELEMENT_HINT_CART_BTN_NAME"] = "Используйте поле только, если вас не устраивают значения по умолчанию, заданные в настройках сайта";
$MESS["KRAKEN_ELEMENT_HINT_CART_MIN_COUNT"] = "Меньше этого количества нельзя добавить в корзину. По умолчанию значение «1»";
$MESS["KRAKEN_ELEMENT_HINT_CART_PRICE_STEP"] = "Количество товара будет изменяться на указанную величину за 1 шаг. По-умолчанию значение «1»";
$MESS["KRAKEN_ELEMENT_HINT_CART_BTN_NAME_ADDED"] = "Используйте поле только, если вас не устраивают значения по умолчанию, заданные в настройках сайта";
$MESS["KRAKEN_ELEMENT_HINT_BTN_FORM"] = "Формы добавляются и редактируются в разделе «Формы захвата»";
$MESS["KRAKEN_ELEMENT_HINT_BTN_LINK"] = "Для перехода к другому блоку, укажите в этом поле  ID блока (только цифру), куда должен произойти переход. Ссылка на другой сайт должна обязательно начинаться с протокола http:// или https:// ";
$MESS["KRAKEN_ELEMENT_HINT_BTN_MODAL"] = "Модальные окна добавляются и редактируются в разделе «Модальные окна»";
$MESS["KRAKEN_ELEMENT_HINT_BTN_QUIZ"] = "Работает при установленном модуле \"Лид-Опросы\" https://goo.gl/n3xa1s ";
$MESS["KRAKEN_ELEMENT_HINT_BTN_ONCLICK"] = "Только для профессионалов. В это поле вы можете добавить полный код события \"onclick\" для создания цели \"нажатия на кнопку\" для систем аналитики";
?>