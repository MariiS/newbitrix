<?
$MESS["KRAKEN_SECTION_1"] = "Базовые настройки";
$MESS["KRAKEN_SECTION_2"] = "Категория активна";
$MESS["KRAKEN_SECTION_3"] = "Показывать в главном меню";
$MESS["KRAKEN_SECTION_4"] = "Показывать в боковом меню раздела";
$MESS["KRAKEN_SECTION_5"] = "*Название";
$MESS["KRAKEN_SECTION_6"] = "*Символьный код";
$MESS["KRAKEN_SECTION_7"] = "*Что показывать";
$MESS["KRAKEN_SECTION_8"] = "Выберите лендинг";
$MESS["KRAKEN_SECTION_9"] = "Сортировка";
$MESS["KRAKEN_SECTION_10"] = "Дизайн и надписи";
$MESS["KRAKEN_SECTION_11"] = "Картинка для шапки";
$MESS["KRAKEN_SECTION_12"] = "Краткое описание";
$MESS["KRAKEN_SECTION_13"] = "Настройки только для подкатегорий";
$MESS["KRAKEN_SECTION_14"] = "Картинка для анонса страницы раздела";
$MESS["KRAKEN_SECTION_15"] = "Основное описание категории";
$MESS["KRAKEN_SECTION_16"] = "*Ширина основного описания";
$MESS["KRAKEN_SECTION_17"] = "Верхний SEO текст";
$MESS["KRAKEN_SECTION_18"] = "Дополнительные баннеры";
$MESS["KRAKEN_SECTION_19"] = "*Режим показа баннеров в левой колонке";
$MESS["KRAKEN_SECTION_20"] = "Выберите баннеры";


$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_NW_TMPL"] = "Если вы хотите вместо страницы категории использовать лендинг, сначала создайте его в разделе «конструктор лендингов»";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_NW_BNNRS"] = "Баннеры создаются и редактируются в разделе «Доп. баннеры» ";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_NW_H1"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_NW_TITLE"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_NW_KWORD"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_NW_DSCR"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте SEO настройки непосредственно у самого лендинга.";
$MESS["KRAKEN_SECTION_HELP_UF_KRAKEN_BNRS_VIEW"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте настройки баннеров непосредственно у самого лендинга.";


$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MAIN_MENU"] = "Показывать в главном меню";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MAIN_NW"] = "Показывать в боковом меню раздела";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_TMPL"] = "Что показывать";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_T_ID"] = "Выберите лендинг";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_BNNRS"] = "Выберите баннеры";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_H1"] = "H1";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_TITLE"] = "Title";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_KWORD"] = "Keywords";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_DSCR"] = "Description";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_NW_PRTXT"] = "Краткое описание";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_SEO_TOP"] = "Вступительное описание категории";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_SEO_BOT"] = "Нижний SEO текст";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_TXT_P"] = "Ширина основного описания";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_BNRS_VIEW"] = "Режим показа баннеров в правой колонке";
$MESS["KRAKEN_SECTION_NAME_UF_KRAKEN_MENU_PICT"] = "Картинка для анонса страницы раздела";


$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_KRAKEN_MAIN_MENU"] = "1";
$MESS["KRAKEN_SECTION_DEFAULT_VALUE_UF_KRAKEN_MAIN_NW"] = "1";
?>

<?


$MESS["KRAKEN_ELEMENT_HINT_NEWS_ELEMENTS_BLOG"] = "Статьи добавляются и редактируются в разделе «Блог»";
$MESS["KRAKEN_ELEMENT_HINT_NEWS_ELEMENTS_NEWS"] = "Новости добавляются и редактируются в разделе «Новости»";
$MESS["KRAKEN_ELEMENT_HINT_NEWS_ELEMENTS_ACTION"] = "Акции добавляются и редактируются в разделе «Акции»";
$MESS["KRAKEN_ELEMENT_HINT_CATALOG"] = "Товары добавляются и редактируются в разделе «Каталог»";
$MESS["KRAKEN_ELEMENT_HINT_BANNERS_RIGHT"] = "Баннеры создаются и редактируются в разделе «Доп. баннеры»";
$MESS["KRAKEN_ELEMENT_HINT_BANNERS_RIGHT_TYPE"] = "Настройка действует только при использовании «стандартного шаблона». При использовании «лендинга», задавайте настройки баннеров непосредственно у самого лендинга.";
$MESS["KRAKEN_ELEMENT_HINT_ITEM_TEMPLATE"] = "Если вы хотите вместо страницы статьи использовать лендинг, сначала создайте его в разделе «конструктор лендингов».";
$MESS["KRAKEN_ELEMENT_HINT_CHOOSE_LANDING"] = "Лендинги создаются и редактируются в разделе «Конструктор лендингов» или в публичной части сайта";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_FORM"] = "Формы добавляются и редактируются в разделе «Формы захвата»";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_LINK"] = "Для перехода к другому блоку, укажите в этом поле ID блока, куда должен произойти переход. Ссылка на другой сайт должна обязательно начинаться с протокола http:// или https://»";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_MODAL"] = "Модальные окна добавляются и редактируются в разделе «Модальные окна»";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_QUIZ"] = "Работает при установленном модуле \"Лид-Опросы\" https://goo.gl/n3xa1s ";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_LAND"] = "Лендинги создаются и редактируются в разделе «Конструктор лендингов» или в публичной части сайта";
$MESS["KRAKEN_ELEMENT_HINT_BUTTON_ONCLICK"] = "Только для профессионалов. В это поле вы можете добавить полный код события \"onclick\" для создания цели \"нажатия на кнопку\" для систем аналитики";
$MESS["KRAKEN_ELEMENT_HINT_FILES_DESC"] = "Порядок описаний соответствует порядку файлов. В левой колонке укажите название файла, в правок комментарий";
$MESS["KRAKEN_ELEMENT_HINT_FILES"] = "Не рекомендуется загружать файлы с расширениями .exe и .zip, так как они могут быть восприняты браузером как вирусы";
?>