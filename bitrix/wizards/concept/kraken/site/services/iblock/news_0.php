<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

set_time_limit(0);

if(!CModule::IncludeModule("iblock"))
	return;

$lang = "ru";
WizardServices::IncludeServiceLang("lang_news.php", $lang);

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/ru/news_0.xml";

$iblockCode = "concept_kraken_site_news";
$iblockCode1 = $iblockCode."_".WIZARD_SITE_ID;
$iblockType = "concept_kraken"."_".WIZARD_SITE_ID;

$iblockID = false; 

$rsIBlock = CIBlock::GetList(array(), array("CODE" => $iblockCode1, "TYPE" => $iblockType));

if($arIBlock = $rsIBlock->Fetch())
	CIBlock::Delete($arIBlock["ID"]); 


$permissions = Array(
	"1" => "X",
	"2" => "R"
);

$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
if($arGroup = $dbGroup -> Fetch())
{
	$permissions[$arGroup["ID"]] = 'W';
};

$iblockID = WizardServices::ImportIBlockFromXML(
	$iblockXMLFile,
	$iblockCode,
	$iblockType,
	WIZARD_SITE_ID,
	$permissions
);

if ($iblockID < 1)
	return;



// iblock user fields


$arProperty = array();
$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
while($arProp = $dbProperty->Fetch())
	$arProperty[$arProp["CODE"]] = $arProp["ID"];

$rsData = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID"=>"IBLOCK_".$iblockID."_SECTION"));
while($arRes = $rsData->Fetch())
    $arUF[$arRes["FIELD_NAME"]] = $arRes["ID"];


// edit form user options
$tabs = 'edit1--#--'.GetMessage("KRAKEN_SECTION_1").'--,--'.'ACTIVE--#--'.GetMessage("KRAKEN_SECTION_2").'--,--'.'UF_KRAKEN_MAIN_MENU--#--'.GetMessage("KRAKEN_SECTION_3").'--,--'.'UF_KRAKEN_MAIN_NW--#--'.GetMessage("KRAKEN_SECTION_4").'--,--'.'NAME--#--'.GetMessage("KRAKEN_SECTION_5").'--,--'.'CODE--#--'.GetMessage("KRAKEN_SECTION_6").'--,--'.'UF_KRAKEN_NW_TMPL--#--'.GetMessage("KRAKEN_SECTION_7").'--,--'.'UF_KRAKEN_NW_T_ID--#--'.GetMessage("KRAKEN_SECTION_8").'--,--'.'SORT--#--'.GetMessage("KRAKEN_SECTION_9").'--;--'.'cedit1--#--'.GetMessage("KRAKEN_SECTION_10").'--,--'.'PICTURE--#--'.GetMessage("KRAKEN_SECTION_11").'--,--'.'UF_KRAKEN_NW_PRTXT--#--'.GetMessage("KRAKEN_SECTION_12").'--,--'.'cedit1_csection1--#----'.GetMessage("KRAKEN_SECTION_13").'--,--'.'UF_KRAKEN_MENU_PICT--#--'.GetMessage("KRAKEN_SECTION_14").'--;--'.'edit5--#--SEO--,--'.'UF_KRAKEN_NW_H1--#--H1--,--'.'UF_KRAKEN_NW_TITLE--#--Title--,--'.'UF_KRAKEN_NW_KWORD--#--Keywords--,--'.'UF_KRAKEN_NW_DSCR--#--Description--,--'.'DESCRIPTION--#--'.GetMessage("KRAKEN_SECTION_15").'--,--'.'UF_KRAKEN_TXT_P--#--'.GetMessage("KRAKEN_SECTION_16").'--,--'.'UF_KRAKEN_SEO_TOP--#--'.GetMessage("KRAKEN_SECTION_17").'--;--'.'cedit2--#--'.GetMessage("KRAKEN_SECTION_18").'--,--'.'UF_KRAKEN_BNRS_VIEW--#--'.GetMessage("KRAKEN_SECTION_19").'--,--'.'UF_KRAKEN_NW_BNNRS--#--'.GetMessage("KRAKEN_SECTION_20").'--;--'.'';
     
CUserOptions::SetOption("form", "form_section_".$iblockID, 
    array(
        "tabs" => $tabs
    ),
    true
);


CUserOptions::SetOption("list", "tbl_iblock_list_".md5($iblockType.".".$iblockID), array ( 'columns' => 'NAME,SORT,ACTIVE,ID', 'by' => 'sort', 'order' => 'asc', 'page_size' => '20', ),
    true
);

foreach($arProperty as $key=>$propID)
{        
    if(strlen(GetMessage("KRAKEN_ELEMENT_HINT_$key")) > 0)
    {
        $arFields = Array("HINT"=>GetMessage("KRAKEN_ELEMENT_HINT_$key"));
        $ibp = new CIBlockProperty;
        $ibp->Update($propID, $arFields);
    }
}

foreach($arUF as $key=>$UFid)
{
    $oUserTypeEntity = new CUserTypeEntity();;
    $oUserTypeEntity->Update($UFid, 
        array(
            'EDIT_FORM_LABEL' => array('ru' => GetMessage("KRAKEN_SECTION_NAME_$key")),
            'HELP_MESSAGE'=> array('ru' => GetMessage("KRAKEN_SECTION_HELP_$key"))
        ) 
    );
}

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/news/index.php', array("IBLOCK_NEWS" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/news/index.php', array("IBLOCK_TYPE" => $iblockType));

?>
