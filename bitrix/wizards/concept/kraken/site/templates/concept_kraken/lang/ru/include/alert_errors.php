<?
$MESS["KRAKEN_ALERT_HEAD"] = "Важная информация";

$MESS["KRAKEN_ALERT_CALLBACK_TITLE"] = "Необходимо выбрать форму захвата для кнопки \"обратного звонка\".";
$MESS["KRAKEN_ALERT_CALLBACK_TEXT_1"] = "Создать/отредактировать форму захвата";
$MESS["KRAKEN_ALERT_CALLBACK_TEXT_2"] = "Выбрать форму в настройках сайта";
$MESS["KRAKEN_ALERT_CALLBACK_COMMENT_2"] = "Настройка находится во вкладке \"Контакты\"";

$MESS["KRAKEN_ALERT_CATALOG_FORM_TITLE"] = "Необходимо выбрать форму захвата \"по умолчанию\" для заказа товаров в каталоге";
$MESS["KRAKEN_ALERT_CATALOG_FORM_TEXT_1"] = "Создать/отредактировать форму захвата";
$MESS["KRAKEN_ALERT_CATALOG_FORM_TEXT_2"] = "Выбрать форму в настройках сайта";
$MESS["KRAKEN_ALERT_CATALOG_FORM_COMMENT_2"] = "Настройка находится во вкладке \"Прочее\"";

$MESS["KRAKEN_ALERT_CATALOG_CART_FORM_TITLE"] = "Необходимо выбрать форму заказа для корзины";
$MESS["KRAKEN_ALERT_CATALOG_CART_FORM_TEXT_1"] = "Создать/отредактировать форму захвата";
$MESS["KRAKEN_ALERT_CATALOG_CART_FORM_TEXT_2"] = "Выбрать форму в настройках сайта";
$MESS["KRAKEN_ALERT_CATALOG_CART_FORM_COMMENT_2"] = "Настройка находится во вкладке \"Настройки магазина\"";


$MESS["KRAKEN_ALERT_BASE_CURR_TITLE"] = "Необходимо выбрать базовую валюту и сохранить настройки";
$MESS["KRAKEN_ALERT_BASE_CURR_TEXT_1"] = "Создать/отредактировать валюту";
$MESS["KRAKEN_ALERT_BASE_CURR_TEXT_2"] = "Выбрать валюту в настройках сайта";
$MESS["KRAKEN_ALERT_BASE_CURR_COMMENT_2"] = "Настройка находится во вкладке \"Настройки магазина\"";
?>