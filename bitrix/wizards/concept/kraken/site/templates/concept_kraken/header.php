<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use \Bitrix\Main\Localization\Loc as Loc;
use \Bitrix\Main\Page\Asset as Asset; 

global $kraken_rights;
global $KRAKEN_TEMPLATE_ARRAY;
global $USER;
global $KrakenCssFullList; 



$moduleID = "concept.kraken";
$kraken_rights = $APPLICATION->GetGroupRight($moduleID);

CModule::IncludeModule($moduleID);
CKraken::krakenOptions(SITE_ID);
Loc::loadMessages(__FILE__);

CKraken::includeCustomMessages();

$bIsMainPage = $APPLICATION->GetCurPage(false) == SITE_DIR;

$KrakenCssListOther = array();
?>


<?if($KRAKEN_TEMPLATE_ARRAY["SITE_BUILD_ON"]['VALUE'][0] == "Y" && !$USER->isAdmin())
{
	if(!$bIsMainPage)
		LocalRedirect(SITE_DIR);


	if(strlen($KRAKEN_TEMPLATE_ARRAY["SITE_BUILD_LINK"]["VALUE"])>0)
		require_once($_SERVER["DOCUMENT_ROOT"].$KRAKEN_TEMPLATE_ARRAY["SITE_BUILD_LINK"]["VALUE"]);
	
	else
		echo htmlspecialcharsBack($KRAKEN_TEMPLATE_ARRAY["SITE_BUILD_TEXT"]["VALUE"]);

	die();
}
?>


<?include_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/functions.php");?>
<?$OS = CKraken::os();?>

<?$GLOBALS["KRAKEN_STR_FOR_CATALOG_CACHE"] = "";?>
<?//CKraken::ClearCacheIBCatalog($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH);?>

<?$GLOBALS["SHOW_KRAKEN_SEO"] = "Y";?>
<?$GLOBALS["IS_CONSTRUCTOR"] = CKraken::IsConstructor("concept_kraken_site_".SITE_ID);?>
<?

	$cur_color = $KRAKEN_TEMPLATE_ARRAY["MAIN_COLOR"]['VALUE'];

	if(strlen($cur_color) <= 0)
	{
	    if(strlen($KRAKEN_TEMPLATE_ARRAY["MAIN_COLOR_STD"]['VALUE']) > 0)
	        $cur_color = $KRAKEN_TEMPLATE_ARRAY["MAIN_COLOR_STD"]['VALUE'];
	}

	if(preg_match("/^rgb/", $cur_color))
	{
		$cur_color = str_replace(array("rgba", "rgb","(",")", ";"), array("","","","",""), $cur_color);
		$cur_color = explode(",", $cur_color);
		$cur_color = CKraken::Krakenrgb2hex($cur_color);
	}

	$name_color = str_replace(array("#"), array(""), $cur_color);



	$file_less = $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/css/main_color.less";

    $dir = SITE_TEMPLATE_PATH."/css/generate_colors/site/";

    $dir_abs = $_SERVER["DOCUMENT_ROOT"].$dir;

    $newfile_css = $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/css/generate_colors/site/main_color_".$name_color.".css";

    $flag = false;



    if(!file_exists($newfile_css))
    {
        DeleteDirFilesEx($dir);
        $flag = true;
    }


    if($flag)
    {

        CheckDirPath($dir_abs);

        require ($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/lessc.inc.php");

        $less = new lessc;
        $less->setVariables(array(
            "color" => $cur_color
        ));

        $less->compileFile($file_less, $newfile_css);

    }


	$KrakenCssListOther[] = SITE_TEMPLATE_PATH."/css/generate_colors/site/main_color_".$name_color.".css";


?>

<!DOCTYPE HTML>
<html lang="<?=LANGUAGE_ID?>" prefix="og: //ogp.me/ns#">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
    
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead();?>

    <?if($KRAKEN_TEMPLATE_ARRAY["SCROLLBAR"]['VALUE'][0]=="Y"):?>
		<style>
		    ::-webkit-scrollbar{ 
		        width: 0px; 
		    }
		</style>
	<?endif;?>

	<?
	
		if(!empty($KRAKEN_TEMPLATE_ARRAY["META"]["VALUE"]))
		{
			foreach($KRAKEN_TEMPLATE_ARRAY["META"]["~VALUE"] as $arMeta){
				echo $arMeta["name"]."\n" ;
			}
		}	

	
		echo $KRAKEN_TEMPLATE_ARRAY["HEAD_SERVICES_JS"];
		echo $KRAKEN_TEMPLATE_ARRAY["INHEAD_JS"]['~VALUE'];
		$APPLICATION->ShowViewContent("service_head");
	?>

	
    
</head>

<?
if(CModule::IncludeModuleEx("concept.kraken") == 3)
{
    include_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/expired.php");
    die();
}
    
?>

<?$APPLICATION->ShowPanel();?>

<?
	$offset = 0;

	if($KRAKEN_TEMPLATE_ARRAY["HEAD_FIXED"]["VALUE"][0] == 'fixed')
		$offset = 150;
?>
<? 
if($KRAKEN_TEMPLATE_ARRAY["FONT_COLOR"]['VALUE'] == '')
	$KRAKEN_TEMPLATE_ARRAY["FONT_COLOR"]['VALUE'] == 'light';
?>
<body class="font-maincolor-<?=$KRAKEN_TEMPLATE_ARRAY["FONT_COLOR"]['VALUE']?> <?=($KRAKEN_TEMPLATE_ARRAY["CAPTCHA"]["VALUE"][0]=="Y" && isset($KRAKEN_TEMPLATE_ARRAY["CAPTCHA_SITE_KEY"]["VALUE"]{0}))? "captcha":"";?>" id="body" data-spy="scroll" data-target="#navigation" data-offset="<?=$offset?>">



	<?require_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/styles_and_scripts.php");?>

	<input class="tmpl_path" name="tmpl_path" value="<?=SITE_TEMPLATE_PATH?>" type="hidden">
	<input class="tmpl" name="tmpl" value="<?=SITE_TEMPLATE_ID?>" type="hidden">
	<input class="site_id" name="site_id" value="<?=SITE_ID?>" type="hidden">
	<input class="LAND_iblockID" name="iblockID" value="<?=$KRAKEN_TEMPLATE_ARRAY['CONSTRUCTOR']["IBLOCK_ID"]?>" type="hidden">
	<input class="LAND_iblockTypeID" name="iblockTypeID" value="<?=$KRAKEN_TEMPLATE_ARRAY['CONSTRUCTOR']["IBLOCK_TYPE"]?>" type="hidden">

	<?if($KRAKEN_TEMPLATE_ARRAY["CAPTCHA"]["VALUE"][0]=="Y" && isset($KRAKEN_TEMPLATE_ARRAY["CAPTCHA_SITE_KEY"]["VALUE"]{0})):?>
		<input class="captcha-site-key" type="hidden" value="<?=$KRAKEN_TEMPLATE_ARRAY["CAPTCHA_SITE_KEY"]["VALUE"]?>">

	<?endif;?>


<?
	echo $KRAKEN_TEMPLATE_ARRAY["BODY_SERVICES_JS"];
	echo $KRAKEN_TEMPLATE_ARRAY["INBODY_JS"]['~VALUE'];


	
	//standart fonts
	$arStandart = array();

	$arStandart[] = "arial";


	//include
	$arInclude = Array();

	$arInclude[] = "lato";
	$arInclude[] = "helvetica";
	$arInclude[] = "segoeUI";


	//Google fonts
	$arGoogle = Array();

	$arGoogle["elmessiri"] = "El+Messiri:400,700";
	$arGoogle["exo2"] = "Exo+2:400,700";
	$arGoogle["ptserif"] = "PT+Serif:400,700";
	$arGoogle["roboto"] = "Roboto:300,400,700";
	$arGoogle["yanonekaffeesatz"] = "Yanone+Kaffeesatz:400,700";
	$arGoogle["firasans"] = "Fira+Sans+Condensed:300,700";
	$arGoogle["arimo"] = "Arimo:400,700";
	$arGoogle["opensans"] = "Open+Sans:400,700";


	if(in_array($KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE'], $arInclude))
	{
	    $font[$KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE']] = true;
	    $KrakenCssListOther[] = SITE_TEMPLATE_PATH."/css/fonts/".$KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE'].".css";
	}
	elseif(!in_array($KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE'], $arStandart) && !in_array($KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE'], $arInclude))
	{
	    $font[$KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE']] = true;
	    $KrakenCssListOther[] = "https://fonts.googleapis.com/css?family=".$arGoogle[$KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE']]."&amp;subset=cyrillic";
	}

	if(in_array($KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE'], $arInclude) && !$font[$KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE']])
	{
	    $KrakenCssListOther[] = SITE_TEMPLATE_PATH."/css/fonts/".$KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE'].".css";
	}
	elseif(!in_array($KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE'], $arStandart) && !in_array($KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE'], $arInclude) && !$font[$KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE']])
	{
	    $KrakenCssListOther[] = "https://fonts.googleapis.com/css?family=".$arGoogle[$KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE']]."&amp;subset=cyrillic";
	}


	$KrakenCssListOther[] = SITE_TEMPLATE_PATH."/css/fonts/title/".$KRAKEN_TEMPLATE_ARRAY['FONTS']['TITLE']['VALUE'].".css";
	$KrakenCssListOther[] = SITE_TEMPLATE_PATH."/css/fonts/text/".$KRAKEN_TEMPLATE_ARRAY['FONTS']['TEXT']['VALUE'].".css";

	$KrakenCssListOther[] = SITE_TEMPLATE_PATH."/css/custom.css";
	$KrakenCssFullList = array_merge($KrakenCssFullList, $KrakenCssListOther);

	foreach($KrakenCssListOther as $css)
	{
		Asset::getInstance()->addCss($css, true);
	}
   

	if(strlen($KRAKEN_TEMPLATE_ARRAY['FAVICON']['VALUE']) > 0)
	{
	    $file = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY['FAVICON']['VALUE'], array('width'=>180, 'height'=>180), BX_RESIZE_IMAGE_EXACT, false);

	    $arFile = CFile::GetFileArray($KRAKEN_TEMPLATE_ARRAY["FAVICON"]["VALUE"]);

	    $APPLICATION->AddHeadString('<link rel="icon" href="'.$file["src"].'" type="'.$arFile["CONTENT_TYPE"].'">', false, true);

	    if($arFile["CONTENT_TYPE"] == "image/x-icon")
	        $APPLICATION->AddHeadString('<link rel="shortcut icon" href="'.$file["src"].'" type="'.$arFile["CONTENT_TYPE"].'">', false, true);
	}
	
	

	if($KRAKEN_TEMPLATE_ARRAY["LAZY_SERVICE"]["VALUE"][0] != "Y")
		echo $KRAKEN_TEMPLATE_ARRAY["BODY_JS"];
?>



<?
global $KRAKEN_MENU;
if($KRAKEN_MENU>0)
$APPLICATION->IncludeComponent(
	"concept:kraken.menu", 
	"open_menu", 
	array(
		"COMPONENT_TEMPLATE" => "open_menu",
		"COMPOSITE_FRAME_MODE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CACHE_USE_GROUPS" => "Y"
	),
	false
);?>

<?if( $KRAKEN_TEMPLATE_ARRAY['SEARCH']['ACTIVE']['VALUE']['ACTIVE'] == "Y" && $KRAKEN_TEMPLATE_ARRAY['SEARCH']['SEARCH_SHOW_IN']['VALUE']['IN_MENU'] == "Y" ):?>
    
    <div class="search-top search-top-js">
        
        <div class="container">
            <div class="close-search-top"></div>


            <?$APPLICATION->IncludeComponent("concept:kraken.search.line", "", Array());?>
            
        </div>

    </div>

<?endif;?>


<div class="xLoader"><div class="google-spin-wrapper"><div class="google-spin"></div></div></div>
<div class="wrapper tone-<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_TONE"]["VALUE"]?>">


	<?if($KRAKEN_TEMPLATE_ARRAY["SHARE_ON"]["VALUE"][0] == 'Y'):?>
	
        <div class="public_shares hidden-xs">
        
            <a class='vkontakte' onclick=""><i class="concept-vkontakte"></i><span><?=GetMessage('KRAKEN_HEADER_SHARE')?></span></a>
            
            <a class='facebook' onclick=""><i class="concept-facebook-1"></i><span><?=GetMessage('KRAKEN_HEADER_SHARE')?></span></a>
            
            <a class='twitter' onclick=""><i class="concept-twitter-bird-1"></i><span><?=GetMessage('KRAKEN_HEADER_SHARE')?></span></a>
            
        </div>
        
    <?endif;?>


	<?
	if($KRAKEN_MENU>0)
	$APPLICATION->IncludeComponent(
		"concept:kraken.menu",
		"mobile_menu",
		Array(
			"COMPONENT_TEMPLATE" => "mobile_menu",
			"COMPOSITE_FRAME_MODE" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CACHE_USE_GROUPS" => "Y"
		)
	);?>

	<?

	$KRAKEN_TEMPLATE_ARRAY["HEADER_BG"] = false;

	$style_header = "";

	if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_BG"]["VALUE"])>0)
	{
		$headBgResize = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY["HEAD_BG"]["VALUE"], array('width'=>2000, 'height'=>2000), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		//$style_header .= "background-image: url(".$headBgResize['src']."); ";
	}

	if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_BG_COLOR"]['VALUE'])>0)
	{
		$arColor = $KRAKEN_TEMPLATE_ARRAY["HEAD_BG_COLOR"]['VALUE'];
		$percent = 1;


		if($KRAKEN_TEMPLATE_ARRAY["HEAD_BG_COLOR"]['VALUE'] == "transparent")
			$style_header .= 'background-color: transparent; ';

		
		else if(preg_match('/^\#/', $arColor))
        {
            $arColor = CKraken::Krakenhex2rgb($arColor);
            $arColor = implode(',',$arColor);

            if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_BG_OPACITY"]['VALUE'])>0)
            $percent = (100 - $KRAKEN_TEMPLATE_ARRAY["HEAD_BG_OPACITY"]['VALUE'])/100;

        
			$style_header .= 'background-color: rgba('.$arColor.', '.$percent.'); ';
        }

        else
        	$header_style .= 'background-color: '.$arColor.'; ';
	}

	if(strlen($style_header)>0)
	{
		$style_header = "style = '".$style_header."'";
		$KRAKEN_TEMPLATE_ARRAY["HEADER_BG"] = true;
	}

	if($KRAKEN_MENU==0)
		$KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"] = 1;

	?>

	<header class="
		tone-<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_TONE"]["VALUE"]?>
		menu-type-<?=$KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"]?>
		menu-view-<?=$KRAKEN_TEMPLATE_ARRAY["MENU_VIEW"]["VALUE"]?>
		head-view-<?=$KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_POSITION"]["VALUE"]?>
		<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_FIXED"]["VALUE"][0]?>
		<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_BG_COVER"]["VALUE"][0]?>
		color_header-<?=$KRAKEN_TEMPLATE_ARRAY["COLOR_HEADER"]["VALUE"]?>
		<?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["MENU_FIXED_VIEW"]["VALUE"]?>
		lazyload" 
		<?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_BG"]["VALUE"])>0):?>data-src="<?=$headBgResize['src']?>"<?endif;?>
		<?if(strlen($style_header)>0) echo $style_header;?>>

		<!-- <div class="shadow"></div> -->

	    <div class="header-top">


	        <div class="container top-line">
	            <div class="row">
                    <table class="wrap hidden-xs">
                        <tr>

                            <td class="col-sm-4 col-xs-1 left">
                                <div class="row">
                                    <table>
                                        <tr>

                                        	<?if($KRAKEN_MENU>0):?>

                                                <td class="menu-burger">
                                                  

                                                    <a class="ic-menu-burger main-color open-main-menu">
														<div class="icon-hamburger-wrap">
														    <span class="icon-bar"></span>
														    <span class="icon-bar"></span>
														    <span class="icon-bar"></span>
														</div>
													</a>
                                                </td>

                                            <?endif;?>



                                            <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["VALUE"]) > 0 || strlen($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_HTML"])>0):?>

                                                <td class="beside-burger">

                                                	<?if($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_POSITION"]["VALUE"] == "center"):?>

	                                                	<?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["VALUE"])>0):?>
		                                                    <div class="main-desciption <?=$KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR_BACKDROP"]["VALUE"][0]?>"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["~VALUE"]?></div>
	                                                    <?endif;?>

                                                    <?endif;?>

                                                    <?if($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_POSITION"]["VALUE"] == "left"):?>

	                                                    <?if(!$bIsMainPage):?><a href="<?=SITE_DIR?>"><?endif;?>
	                                                    	<?=$KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_HTML"]?>
	                                                    <?if(!$bIsMainPage):?></a><?endif;?>

                                                    <?endif;?>
                                                </td>

                                            <?endif;?>

                                        </tr>
                                    </table>
                                </div>
                            </td><!-- /left -->

                            <td class="col-sm-4 col-xs-10 center">

                            	<?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["VALUE"])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_HTML"])>0):?>

                            	<?if($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_POSITION"]["VALUE"] == "center"):?>
                            		<?if(!$bIsMainPage):?><a href="<?=SITE_DIR?>"><?endif;?>
                                		<?=$KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_HTML"]?>
                                	<?if(!$bIsMainPage):?></a><?endif;?>
                                <?endif;?>

                                <?if($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_POSITION"]["VALUE"] == "left"):?>

                                    <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["VALUE"])>0):?>
                                    	<div class="main-desciption <?=$KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR_BACKDROP"]["VALUE"][0]?>"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["~VALUE"]?></div>
                                    <?endif;?>

                                <?endif;?>


                                <?endif;?>
                            </td><!-- /center -->

                            <td class="col-sm-4 col-xs-1 right">
								<?if(!empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"])):?>

									<a class="visible-part phone open_modal_contacts"><span></span></a>

								<?endif;?>

                                <div class="row hidden-xs">
                                    <table class="right-inner">
                                        <tr>
                                            <td >
                                                <div class="main-phone">

                                                	<?$show_list = false;?>

                                                	<?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"][0]['name'])>0 && strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name'])>0):?>

                                                		<div class="visible-part phone"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][0]['name']?></div>
                                                		

                                                		<div class='comment'>
                                                			<a class="visible-part mail" href="mailto:<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?>">
                                                				<span class="bord-bot"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?></span>
                                                			</a>
                                            			</div>

                                                		<?if( strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"][0]['desc'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['desc'])>0 || count($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"]) > 1 || count($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"]) > 1) $show_list = true;?>

                                                	<?elseif(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"][0]['name'])>0):?>

                                                		<div class="visible-part phone"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][0]['name']?></div>

                                                		<?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"][0]['desc'])>0):?>
                                                			<div class="desc_cont"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][0]['desc']?></div>
                                            			<?endif?>

                                            			<?if( count($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"]) > 1 || strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name'])>0) $show_list = true;?>

                                            		<?elseif(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name'])>0):?>

                                            			<div class='comment'>
                                            			
                                                			<a class="visible-part mail" href="mailto:<?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?>"><span class="bord-bot white"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['name']?></span></a>
                                                		</div>

                                            			<?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['desc'])>0):?>
                                                			<div class="desc_cont"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][0]['desc']?></div>
                                                		<?endif;?>

                                                		<?if( count($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"]) > 1) $show_list = true;?>

                                                	<?endif;?>

                                                	<?if($show_list || ($KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][0] == 'Y') && (strlen($KRAKEN_TEMPLATE_ARRAY["VK"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["FB"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["TW"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["YOUTUBE"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["INST"]['VALUE'])>0)):?>

                                                        <div class="ic-open-list-contact open-list-contact"><span></span></div>

                                                        <div class="list-contacts">
                                                            <table>

                                                            	<?foreach($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"] as $key => $val):?>
                                                            	
	                                                                <tr>
	                                                                    <td>
	                                                                        <div class="phone"><span ><?=$val['name']?></span></div>
	                                                                        <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][$key]["desc"]) > 0):?>
	                                                                        	<div class="desc"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["~VALUE"][$key]["desc"]?></div>
	                                                                        <?endif;?>
	                                                                    </td>
	                                                                </tr>

                                                                <?endforeach;?>
                                                                <?foreach($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"] as $key => $val):?>

                                                                	<tr>
	                                                                	<td>
	                                                                        <div class="email"><a href="mailto:<?=$val['name']?>"><span class="bord-bot"><?=$val['name']?></span></a></div>
	                                                                        <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][$key]["desc"]) > 0):?>
	                                                                        	<div class="desc"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_EMAILS"]["VALUE"][$key]["desc"]?></div>
	                                                                        <?endif;?>
	                                                                    </td>
	                                                                </tr>


                                                                <?endforeach;?>

                                                                <?if($KRAKEN_TEMPLATE_ARRAY["GROUP_POS"]["VALUE"][0] == 'Y' && (strlen($KRAKEN_TEMPLATE_ARRAY["VK"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["FB"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["TW"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["YOUTUBE"]['VALUE'])>0 || strlen($KRAKEN_TEMPLATE_ARRAY["INST"]['VALUE'])>0)):?>
	                                                                <tr>
	                                                                    <td>
	                                                                        <?CKraken::CreateSoc($KRAKEN_TEMPLATE_ARRAY)?>
	                                                                    </td>
	                                                                </tr>
	                                                            
	                                                            <?endif;?>

                                                                    
                                                            </table>
                                                        </div>

                                                    <?endif;?>

                                                </div>

                                             
                                            </td>

                                            <?if($KRAKEN_TEMPLATE_ARRAY["SHOW_CALLBACK"]["VALUE"][0] == 'Y' && $KRAKEN_TEMPLATE_ARRAY["FORMS"]["VALUE_CALLBACK"] != "N"):?>
                                                <td>
                                                    <a class="ic-callback main-color call-modal callform" data-header="<?=GetMessage('KRAKEN_HEADER_FORM_HEADER')?>" data-call-modal="form<?=$KRAKEN_TEMPLATE_ARRAY["FORMS"]["VALUE_CALLBACK"]?>"><span></span></a>
                                                </td>
                                            <?endif;?>

                                        </tr>
                                    </table>
                                </div>
                            </td><!-- /right -->

                        </tr>
                    </table>

                    <div class="col-xs-12">

	                    <div class="header-block-mob-wrap visible-xs">

	                    	<?
                                $style = "";
                                if($KRAKEN_TEMPLATE_ARRAY["CART_ON"]["VALUE"][0] == "Y")
                                    $style .= "cart-on ";
                                
                                if(empty($KRAKEN_MENU))
                                    $style .= "no-menu ";

                                if(empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"]))
                                    $style .= "no-contacts ";
                            ?>

	                    	<table class="header-block-mob <?=$style?>">
		                        <tr>
		                        	<?if($KRAKEN_MENU>0):?>
			                            <td class="mob-callmenu">
	                                        <a class="ic-menu-burger main-color open-main-menu">
												<div class="icon-hamburger-wrap">
												    <span class="icon-bar"></span>
												    <span class="icon-bar"></span>
												    <span class="icon-bar"></span>
												</div>
											</a>
			                            </td>
		                            <?endif;?>

	                            	<?if(strlen($KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_HTML"])>0):?>

	                                	<td class="mob-logo">

	                                    	<?if(!$bIsMainPage):?><a href="<?=SITE_DIR?>"><?endif;?>
	                                    		<?=$KRAKEN_TEMPLATE_ARRAY["LOGOTYPE_HTML"]?>
	                                    	<?if(!$bIsMainPage):?></a><?endif;?>

	                                    </td>


	                                <?endif;?>

	                                <?if($KRAKEN_TEMPLATE_ARRAY["CART_ON"]["VALUE"][0] == "Y" && $APPLICATION->GetCurDir() != SITE_DIR."cart/"):?>

	                                	<?/*<td class="mob-cart area_for_mini-widget">
								            
								            	\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area-mini_cart-mini-widget");
								            	$APPLICATION->IncludeComponent(
									                "concept:kraken.mini_cart",
									                "mini-widget",
									                Array(
									                    "CURRENT_SITE" => SITE_ID,
									                    "MESSAGE_404" => "",
									                    "SET_STATUS_404" => "N",
									                    "SHOW_404" => "N",
									                    "MODE" => $KRAKEN_TEMPLATE_ARRAY["CART_MINICART_MODE"]["VALUE"],
									                    "DESC_EMPTY" => $KRAKEN_TEMPLATE_ARRAY["CART_MINICART_DESC_EMPTY"]["VALUE"],
									                    "DESC_NOEMPTY" => $KRAKEN_TEMPLATE_ARRAY["CART_MINICART_DESC_NOEMPTY"]["VALUE"],
									                    "LINK" => $KRAKEN_TEMPLATE_ARRAY["CART_MINICART_LINK_PAGE"]["VALUE"]
									                )
									            );
									            \Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area-mini_cart-mini-widget");
								            
								        </td>*/?>

								        <td class="mob-cart">
											<div class="open-cart-mob <?=$arParams["MODE"]?> basket-count-control-widget-in-public-mob">
												<div class="bg-color"></div>
												<div class="wrap-img-count">
													<table>
														<tr>
															<td><span class="icon"></span></td>
															<td><span class="count basket-count-value">0</span></td>
														</tr>
													</table>
											    </div>
											    
												<a class="cart_link url2Basket hidden" href="<?=SITE_DIR?>cart/"></a>

												<?if(strlen($arParams["LINK"])>0):?>
													<a class="cart_link url2Other hidden scroll" href="<?=$arParams["LINK"]?>"></a>
												<?endif;?>
												
											</div>
								        </td>

							        <?endif;?>

		                
									<?if(!empty($KRAKEN_TEMPLATE_ARRAY["HEAD_CONTACTS"]["VALUE"])):?>
			                            <td class="mob-contacts">
											<a class="visible-part main-color phone open_modal_contacts"><span></span></a>
			                            </td>
		                            <?endif;?>

		                        </tr>
		                    </table>


		                    <?if(strlen($KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["VALUE"])>0):?>
                                <div class="main-desciption-mob"><?=$KRAKEN_TEMPLATE_ARRAY["HEAD_DESCRIPTOR"]["~VALUE"]?></div>
                            <?endif;?>
	                       
	                    </div>

                    </div>
                    <div class="clearfix"></div>

	            </div>
	        </div>


	        <?

	        if($KRAKEN_MENU>0 && ($KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"] == 2 || $KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"] == 3))
	        	$APPLICATION->IncludeComponent(
				"concept:kraken.menu",
				".default",
				Array(
					"COMPONENT_TEMPLATE" => ".default",
					"COMPOSITE_FRAME_MODE" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CACHE_USE_GROUPS" => "Y"
				)
			);?>

	            
	    </div><!-- /div.header-top -->
	    
	</header>