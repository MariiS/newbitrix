var site_id = "";
var cur_pos = 0;
var cart_page = "N";

function getChar(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return null;
    }
    return true;
}



function mobileMenuPositionFooter() {
    if ($('.open-menu-mobile').height() > ($(
            'div.open-menu-mobile div.menu-mobile-inner').outerHeight() + $(
            'div.open-menu-mobile div.foot-wrap').outerHeight(true))) $(
        'div.open-menu-mobile div.foot-wrap').addClass('absolute');
    else {
        $('div.open-menu-mobile div.foot-wrap').removeClass('absolute');
    }
}

function openMenuFooterPos() {
    var totalHeight = $('.open-menu .head-menu-wrap').outerHeight(true) + $(
        '.open-menu .body-menu').outerHeight(true) + $(
        '.open-menu .footer-menu-wrap').outerHeight(true);
    var needHeight = $('.open-menu').height() - totalHeight + $(
        '.open-menu .body-menu').outerHeight();
    if ($(window).height() > totalHeight) $('.open-menu .body-menu').css(
        'min-height', needHeight + 'px');
}

function krakenResizeVideo() {
    var win_height = $('div.video-modal').height();
    var modal = 590;
    if ($(window).width() > 767) {
        if (win_height > modal) $("div.video-modal div.kraken-modal-dialog").addClass(
            'pos-absolute');
        else {
            $("div.video-modal div.kraken-modal-dialog").removeClass('pos-absolute');
        }
    } else {
        $("div.video-modal div.kraken-modal-dialog").addClass('pos-absolute');
    }
}

function scrollToBlock(dist, elem) {

    if(elem)
    {
        $('div.wrapper').removeClass('blur');
        $("body").removeClass("modal-open");
        $("div.modalArea").children().remove();

        var moreDist = 0;
        if ($('header').hasClass("fixed")) moreDist = 70;
        if ($('div').is(".catalog-card-wrap") || $('div').is(".catalog-list-wrap")) {
            moreDist = 40;
            if ($('header').hasClass("fixed")) moreDist = 110;
            if (elem) {
                if (elem.hasClass('review')) moreDist = 130;
            }
        }

        var destinationhref = parseInt($(dist).offset().top - moreDist);
        
        $("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destinationhref
        }, 1000, "linear", function() {

            setTimeout(
                function(){

                    if( 100 < Math.abs(parseInt($(dist).offset().top - moreDist) - destinationhref) )
                    {
                        $("html:not(:animated),body:not(:animated)").animate({
                            scrollTop: parseInt($(dist).offset().top - moreDist)
                        }, 300, "linear");
                    }
                }, 300);
          }
        );

    }
}

function timerCookie(form) {
    var timeout = true;
    var timerVal = form.find('input.timerVal').val();
    var forCookieTime = parseFloat(form.find('input.forCookieTime').val());
    var totalTime = 0;
    var mainTime = new Date().getTime();
    var idElem = form.find("input[name='element']").val();
    var idSect = form.find('input.idSect').val();
    var firstEnter = BX.getCookie('_krakenFirstEnter' + idSect + idElem);
    var firstEnterVal = BX.getCookie('_krakenFirstEnterVal' + idSect + idElem);

    setTimeout(function() {
        form.css('min-height', form.outerHeight());
    }, 100);
    form.find('.krakentimer').addClass('active');

    if(forCookieTime <= 0)
        forCookieTime = 1;
    

    if (typeof(firstEnter) == "undefined" || firstEnterVal != timerVal) {


        BX.setCookie('_krakenFirstEnter' + idSect + idElem, mainTime, {
            expires: forCookieTime * 60 * 60
        });
        BX.setCookie('_krakenFirstEnterVal' + idSect + idElem, timerVal, {
            expires: forCookieTime * 60 * 60
        });
        firstEnter = BX.getCookie('_krakenFirstEnter' + idSect + idElem);
        firstEnterVal = BX.getCookie('_krakenFirstEnterVal' + idSect + idElem);
    }


    totalTime = ((mainTime - firstEnter) / 1000).toFixed();


    if (totalTime < timerVal * 60) timerVal = timerVal * 60 - totalTime;
    else{
        timeout = false;
    }



    if (timeout) form.find('.krakentimer').countdown({
        until: timerVal,
        format: 'HMS',
        onExpiry: liftOff,
        layout: form.find('.krakentimer').html()
    }, $.countdown.regionalOptions['ru']);
    else {
        form.find('.questions').removeClass('active');
        form.find('.timeout_text').addClass('active');
    }

    function liftOff() {
        form.find('.questions').removeClass('active');
        form.find('.timeout_text').addClass('active');
    }
}

function parseCount(newVal, step, minVal) {
    var ost = newVal % step;
    var step_ = step - ost;

    newVal = newVal - minVal;
    newVal -= (newVal % step);

    if(step_ >= ost)
    {
        newVal += minVal;
    }

    else
    {
        newVal += minVal + step;
    }


    if (newVal <= minVal || isNaN(newVal))
        newVal = minVal;

    return newVal;
}

function formatNum(val) {
    val = String(val).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
    return val;
}


function formAttentionScroll(dist, parent) {
    parent.animate({
        scrollTop: dist
    }, 700);
}



$(document).on('change', ".form.send input[type='file']",
    function(){

        var inp = $(this);
        var file_list_name = "";

        $(inp[0].files).each(
            function(key){
                file_list_name += "<span>"+inp[0].files[key].name+"</span><br/>";
            }
        );


        if(!file_list_name.length)
            return;

        inp.parents('form.send').addClass('file-download');
        inp.parents('label').find('.area-files-name').html(file_list_name);
        inp.parents('label').removeClass("area-file");
        inp.closest('.load-file').removeClass("has-error");

    }
);




$(document).on("click", "form.form input[type='checkbox']", function() {
    $(this).parents("div.wrap-agree").removeClass("has-error");
});



function validGroupCheckbox(arParams){

    var error = 0;

    for (key in arParams){

        var cheked = false;

        for (var i = 0; i < arParams[key].length; i++){

            if(arParams[key][i].checked)
                cheked = true;
        }

        if(!cheked)
        {
            if(error == 0)
                error = 1;

            for (var i = 0; i < arParams[key].length; i++){

                $(arParams[key][i]).addClass('has-error');
            }
        }
    }

    return error;
}

function validGroupSelect(arParams){
    var error = 0;

    for (key in arParams){

        var cheked = false;

        for (var i = 0; i < arParams[key].length; i++){

            if(arParams[key][i].checked)
                cheked = true;
        }

        if(!cheked)
        {
            if(error == 0)
                error = 1;

            $(arParams[key][0]).parents(".form-select").addClass('has-error');
        }
    }
    return error;
}


function sendForm(arParams)
{

    var error = 0,
        formSendAll = new FormData(),
        actionGoals = "CALLBACK";

    if(arParams.form_block.attr("id"))
        formSendAll.append("element", arParams.form_block.attr("id").replace("form-", ""));

    formSendAll.append("send", "Y");
    formSendAll.append("tmpl_path", $("input.tmpl_path").val());
    /*formSendAll.append("typeForm", arParams.typeForm);
    formSendAll.append("fromBasket", arParams.fromBasket);*/
    formSendAll.append("site_id", $("input.site_id").val());
    formSendAll.append("url", decodeURIComponent(location.href));


    if(arParams.form_block.hasClass('form-page-cart'))
        actionGoals = "ORDER";
    else if(arParams.form_block.hasClass('fast-order'))
        actionGoals = "FAST_ORDER";




    for (var i = 1; i <= 10; i++){
        if($("input#custom-input-"+i).val().length>0)
            formSendAll.append("custom-input-"+i, $("input#custom-input-"+i).val());
    }


    if (typeof(arParams.agreecheck.val()) != "undefined") {
        if (!arParams.agreecheck.prop("checked")) {
            arParams.agreecheck.parents("div.wrap-agree").addClass("has-error");
            error = 1;
        }
    }
    $("input[type='email'], input[type='text'], input[type='password'], textarea", arParams.form_block).each(
        function(key, value) {
            if ($(this).hasClass("email") && $(this).val().length > 0) {
                if (!(
                        /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
                    ).test($(this).val())) {
                    $(this).parent("div.input").addClass("has-error");
                    error = 1;
                }
            }
            if ($(this).hasClass("require")) {
                if ($(this).val().trim().length <= 0){
                    $(this).val("");
                    $(this).parent("div.input").addClass("has-error");
                    error = 1;
                }
            }
        }
    );


    $("input[type='file']", arParams.form_block).each(function(key, value) {
        if ($(this).hasClass("require")) {
            if ($(this).closest('.load-file').find('span.area-file').hasClass(
                    'area-file')) {
                $(this).closest('.load-file').addClass("has-error");
                error = 1;
            }
        }
    });



    if (!$('.kraken-modal').hasClass('form-modal'))
    {
        var otherHeight = 0;

        if ($('header').hasClass('fixed'))
            otherHeight = $('header .header-top').outerHeight(true);

    }



    var arChecbox = {}, newKey = null;

    $("input.check-require[type='checkbox']", arParams.form_block).each(
        function(key, value)
        {
            newKey = $(this).attr('name').replace("[]", "");

            if(typeof arChecbox[newKey] == "undefined")
                arChecbox[newKey] = [];
            
            arChecbox[newKey].push(this);
        }
    );

    if(newKey)
        error = validGroupCheckbox(arChecbox);

    delete arChecbox;
    newKey = null;

    var arRadio = {};
    $(".select-require input.opinion[type='radio']", arParams.form_block).each(
        function(key, value)
        {
            newKey = $(this).attr('name');

            if(typeof arRadio[newKey] == "undefined")
                arRadio[newKey] = [];
            
            arRadio[newKey].push(this);
        }
    );

    if(newKey)
        error = validGroupSelect(arRadio);

    delete arChecbox;
    newKey = null;



    if (error == 1) {
        if ($('.kraken-modal').hasClass('form-modal'))
        {
            formAttentionScroll(arParams.form_block.find('.has-error:first').offset().top - arParams.form_block.parents(
                '.kraken-modal-dialog').offset().top, arParams.form_block.parents(".kraken-modal"));
        }
        else if (arParams.form_block.hasClass('form-cart') && !arParams.form_block.hasClass('form-page-cart'))
        {
            formAttentionScroll(arParams.form_block.find('.has-error:first').offset().top - arParams.form_block.parents(".body").offset().top, arParams.form_block.parents(".cart-parent"));
        }
        else
        {
            formAttentionScroll(arParams.form_block.find('.has-error:first').offset().top - otherHeight, $("html:not(:animated),body:not(:animated)"));
        }
    }
    if (error == 0) {
        arParams.form_block.css({
            "height": arParams.form_block.outerHeight() + "px"
        });

        form_arr = arParams.form_block.find(':input,select,textarea').serializeArray();

        for (var i = 0; i < form_arr.length; i++)
        {
            formSendAll.append(form_arr[i].name, form_arr[i].value);
        };

        if (arParams.form_block.hasClass('file-download'))
        {
            arParams.form_block.find('input[type=file]').each(function(key)
            {

                var inp = $(this);
                var file_list_name = "";

                $(inp[0].files).each(
                    function(k){
                        formSendAll.append(inp.attr('name'), inp[0].files[k], inp[0].files[k].name);
                    }
                );

            });
        }
        
        if(arParams.captchaToken.length>0)
            formSendAll.append("captchaToken", arParams.captchaToken);

            
        arParams.button.removeClass("active");
        arParams.load.addClass("active");
        setTimeout(function() {
            $.ajax({
                url: arParams.path,
                type: "post",
                contentType: false,
                processData: false,
                data: formSendAll,
                dataType: 'json',
                success: function(json) {
                    /*$("body").append(json);*/
                    

                    if (json.OK == "N") {
                        arParams.button.addClass("active");
                        arParams.load.removeClass("active");
                    }
                    if (json.OK == "Y") 
                    {
                        if (!arParams.form_block.hasClass('form-page-cart')) 
                        {
                            arParams.questions.removeClass("active");
                            arParams.thank.addClass("active");
                        }

                        if(actionGoals=="CALLBACK")
                            addGoal(actionGoals);
                        
                    
                        $.ajax({
                            url: "/",
                            type: "post",
                            success: function(json) {}
                        });

                        setTimeout(function() {
                            if ($('.kraken-modal').hasClass('form-modal'))
                                formAttentionScroll(arParams.form_block.find('.thank').offset().top - arParams.form_block.parents(
                                    '.kraken-modal-dialog').offset().top, arParams.form_block.parents(".kraken-modal"));
                            else if (arParams.form_block.hasClass('form-cart') && !arParams.form_block.hasClass('form-page-cart')) {
                                formAttentionScroll(arParams.form_block.find('.thank').offset().top - arParams.form_block.parents(
                                    ".body").offset().top, arParams.form_block.parents(".cart-parent"));
                            } else {
                                formAttentionScroll(arParams.form_block.find('.thank').offset().top -
                                    otherHeight, $("html:not(:animated),body:not(:animated)"));
                            }
                        }, 300);

                        if (arParams.form_block.hasClass('form-cart')){
                            
                            setTimeout(function()
                            {
                                clearBasket();
                                arParams.form_block.find(".areacart-form").children().remove();
                                arParams.form_block.find(".cart-back").removeClass('active');
                                arParams.form_block.find(".areacart-form").removeClass('active');
                                arParams.form_block.find(".info-table").addClass('active');
                            }, 3000);
                        }

                        if (arParams.form_block.hasClass('form-page-cart')){
                            clearBasket();
                            $('div.google-spin-wrapper').addClass('active');
                            $(".cart-first-block").find(".part-wrap").hide();
                            $(".cart-first-block").find(".form-cart-wrap").hide();
                        }

                        if(typeof json.CART_LINK != "undefined")
                        {
                            if(json.CART_LINK.length>0){
                                setTimeout(function() {
                                    window.location.href = json.CART_LINK;
                                }, 1000);
                            }
                        }

                        if (typeof(arParams.linkHREF) != "undefined") 
                        {
                            setTimeout(function() {
                                window.location.href = arParams.linkHREF;
                            }, 3200);
                        }



                        if (json.SCRIPTS.length > 0)
                            $('body').append("<script>"+json.SCRIPTS+"</script>");

                    }
                }
            });
        }, 1000);
    }
}


$(document).on("click", "button.btn-submit", function(){

    var form = $(this).parents("form.send");
    var path = "/bitrix/tools/kraken/ajax/forms.php";
    if (form.hasClass('form-cart') || form.hasClass('form-page-cart')) 
        path = "/bitrix/components/concept/kraken.cart/order.php";


    paramsForm = {
        form_block: form,
        path: path,
        button: $(this),
        linkHREF: $(this).attr("data-link"),
        header: $("input[name='header']", form),
        agreecheck: $("input.agreecheck", form),
        questions: $("div.questions", form),
        load: $("div.load", form),
        thank: $("div.thank", form),
        captchaToken: ""

    };

    if($("body").hasClass('captcha'))
    {
        grecaptcha.execute($(".captcha-site-key").val(), {action: 'homepage'}).then(function(token) {

            paramsForm.captchaToken = token;
            sendForm(paramsForm);
        });
    }
    else{
        sendForm(paramsForm);
    }
    
    
});
$(document).on("focus", "form input[type='text'], form input[type='email'], form textarea", function() {
    $(this).parent("div.input").removeClass("has-error");
    if ($(this).val().length <= 0 && !$(this).hasClass("phone")) {
        $(this).attr("data-placeholder", $(this).attr("placeholder"));
        $(this).attr("placeholder", "");
    }
});
$(document).on("blur", "form input[type='text'], form textarea", function() {
    if ($(this).val().length <= 0 && !$(this).hasClass("phone")) $(this).attr(
        "placeholder", $(this).attr("data-placeholder"));
});
$(document).on("keypress", "form.form div.count input", function(e) {
    e = e || event;
    if (e.ctrlKey || e.altKey || e.metaKey) return;
    var chr = getChar(e);
    if (chr == null) return false;
});
$(document).on("keyup", "form.form div.count input", function(e) {
    var value = $(this).val().toString();
    var newVal = "";
    for (var i = 0; i < value.length; i++) {
        if (value[i] == "0" || value[i] == "1" || value[i] == "2" || value[i] ==
            "3" || value[i] == "4" || value[i] == "5" || value[i] == "6" || value[i] ==
            "7" || value[i] == "8" || value[i] == "9") newVal += value[i];
    }
    if (newVal == 0) newVal = 1;
    $(this).val(newVal);
    if ($(this).val() == "") $(this).parent().addClass('in-focus');
});
$(document).on("click", "form.form div.count span.plus", function(e) {
    var input = $(this).parent("div.count").find("input");
    var value = parseFloat(input.val());
    if (isNaN(value)) value = 0;
    value += 1;
    input.val(value);
    if ($(this).val() == "") $(this).parent().addClass('in-focus');
});
$(document).on("click", "form.form div.count span.minus", function(e) {
    var input = $(this).parent("div.count").find("input");
    var value = parseFloat(input.val());
    if (isNaN(value)) value = 0;
    value -= 1;
    if (value < 0) value = '';
    if (value == 0) value = 1;
    input.val(value);
});
$(document).on("keypress", ".only-num", function(e) {
    e = e || event;
    if (e.ctrlKey || e.altKey || e.metaKey) return;
    var chr = getChar(e);
    if (chr == null) return false;
});
$(document).on("focus", "input.focus-anim, textarea.focus-anim", function() {
    if ($(this).val() == "") $(this).parent().addClass('in-focus');
});
$(document).on("blur", "input.focus-anim, textarea.focus-anim", function() {
    element = $(this);
    setTimeout(function() {
        if (element.val() == "") element.parent().removeClass('in-focus');
    }, 200);
});


$(document).on('click', '.inp-show-js-padding',
    function(){

        var str = $(this).find(".inp-show-js-length").text().length;

        if(str >= 48)
            $(this).find("textarea").addClass('two-rows');

        if(str >= 78)
            $(this).find("textarea").addClass('three-rows');
  
    }
);
