<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?

foreach($arResult["SECTIONS"] as $key=>$arSection)
{
    
    $arSelect = Array("ID", "UF_*");
    $arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], "ID" => $arSection["ID"]);
    $db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
    $ar_result = $db_list->GetNext();
    
    
    $arSection = array_merge($arSection, $ar_result);
    
    $arResult["SECTIONS"][$key] = $arSection;
    
    if($arSection['UF_KRAKEN_MAIN_CTLG'] == "0")
        unset($arResult["SECTIONS"][$key]);

}
?>