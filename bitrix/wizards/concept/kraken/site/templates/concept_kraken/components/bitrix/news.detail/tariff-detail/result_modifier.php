<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(!empty($arResult))
{
    if(empty($arResult["PROPERTIES"]))
    {
    	$arFilter = Array("ID"=> $arResult["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

        $res = CIBlockElement::GetList(Array("sort" => "asc"), $arFilter);

        while($ob = $res->GetNextElement())
        {
            
            $arResult["PROPERTIES"] = $ob->GetProperties();
           
        }
    }


    $arResult["PICTURE_ALT"] = strip_tags($arResult["~NAME"]);

    if(isset($arResult["PROPERTIES"]["TARIFF_PICTURE"]["VALUE"]))
    {
        $img = CFile::ResizeImageGet($arResult["PROPERTIES"]["TARIFF_PICTURE"]["VALUE"], array('width'=>400, 'height'=>400), BX_RESIZE_IMAGE_EXACT, false, false, false, $KRAKEN_TEMPLATE_ARRAY["PICTURES_QUALITY"]["VALUE"]);

        $arResult["PICTURE_SRC"] = $img["src"];

        if(isset($arResult["PROPERTIES"]["TARIFF_PICTURE"]["DESCRIPTION"]{0}))
            $arResult["PICTURE_ALT"] = strip_tags($arResult["PROPERTIES"]["TARIFF_PICTURE"]["~DESCRIPTION"]);

    }



    if(!empty($arResult["PROPERTIES"]["TARIFF_GALLERY"]["VALUE"]))
    {
        $arWaterMark = Array();

        if($arResult["PROPERTIES"]["TARIFF_WATERMARK"]["VALUE"] > 0){

            $arWaterMark = Array(
                array(
                    "name" => "watermark",
                    "position" => "center",
                    "type" => "image",
                    "size" => "real",
                    "file" => $_SERVER["DOCUMENT_ROOT"].CFile::GetPath($arResult["PROPERTIES"]["TARIFF_WATERMARK"]["VALUE"]), 
                    "fill" => "exact",
                )
            );
        }

        foreach($arResult["PROPERTIES"]["TARIFF_GALLERY"]["VALUE"] as $k => $arImages)
        {
            $file_big = CFile::ResizeImageGet($arImages, array('width'=>1600, 'height'=>1600), BX_RESIZE_IMAGE_PROPORTIONAL, false, $arWaterMark, false, $KRAKEN_TEMPLATE_ARRAY["PICTURES_QUALITY"]["VALUE"]);

            $file_min = CFile::ResizeImageGet($arImages, array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_EXACT, false, false, false, 75);


            $arResult["GALLERY"][] = array(
                "BIG_SRC" => $file_big["src"],
                "SMALL_SRC" => $file_min["src"],
                "ALT" => CKraken::prepareText($arResult["PROPERTIES"]["SERVICE_GALLERY"]["DESCRIPTION"][$k]),
                "TITLE" => CKraken::prepareText($arResult["PROPERTIES"]["SERVICE_GALLERY"]["DESCRIPTION"][$k])
            );
        }

    }
}

?>