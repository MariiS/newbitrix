<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
CModule::IncludeModule('concept.kraken');

global $KRAKEN_TEMPLATE_ARRAY;
?>

<div class="service-detail-block">
	
	<div class="header">

		<?if(isset($arResult["PICTURE_SRC"])):?>

			<div class="header-col wr-image">
				<div class="wr-image-inner">
					<img src="<?=$arResult["PICTURE_SRC"]?>" alt="<?=$arResult["PICTURE_ALT"]?>">
					<?if($arResult["PROPERTIES"]["SERVICE_HIT"]["VALUE"] =="Y"):?><span class="star"></span><?endif;?>
				</div>
			</div>

		<?endif;?>


		<?if(
			isset($arResult["PROPERTIES"]["SERVICE_SUBNAME"]["VALUE"]{0})
			|| isset($arResult["PROPERTIES"]["SERVICE_NAME"]["VALUE"]{0})
			|| isset($arResult["PROPERTIES"]["SERVICE_PREVIEW_TEXT"]["VALUE"]{0})
			|| isset($arResult["PROPERTIES"]["SERVICE_PRICE"]["VALUE"]{0})

		):?>

			<div class="header-col wr-text">

				<?if(isset($arResult["PROPERTIES"]["SERVICE_SUBNAME"]["VALUE"]{0})):?>
	            	<div class="subname"><?=$arResult["PROPERTIES"]["SERVICE_SUBNAME"]["~VALUE"]?></div>
				<?endif?>

				<?if(isset($arResult["PROPERTIES"]["SERVICE_NAME"]["VALUE"]{0})):?>

		            <div class="name main1">
		            	<?=str_replace(array("<br>","<br/>","<br />"), array(" ", " ", " "), $arResult["PROPERTIES"]["SERVICE_NAME"]["~VALUE"])?>
		            </div>

				<?endif?>

				<?if(isset($arResult["PROPERTIES"]["SERVICE_PREVIEW_TEXT"]["VALUE"]{0})):?>

		            <div class="text">
		                <?=$arResult["PROPERTIES"]["SERVICE_PREVIEW_TEXT"]["~VALUE"]?>
		            </div>

				<?endif?>

				<?if(isset($arResult["PROPERTIES"]["SERVICE_PRICE"]["VALUE"]{0})):?>

	            	<div class="visible-sm">
	            		<div class="price main1"><?=$arResult["PROPERTIES"]["SERVICE_PRICE"]["~VALUE"]?></div>
	            		<?if(isset($arResult["PROPERTIES"]["SERVICE_OLD_PRICE"]["VALUE"]{0})):?><div class="old-price main2"><?=$arResult["PROPERTIES"]["SERVICE_OLD_PRICE"]["~VALUE"]?></div><?endif?></div>

	            <?endif?>
			</div>

		<?endif;?>


		<?if(isset($arResult["PROPERTIES"]["SERVICE_PRICE"]["VALUE"]{0})):?>

			<div class="header-col wr-price hidden-sm">
				<div class="price main1"><?=$arResult["PROPERTIES"]["SERVICE_PRICE"]["~VALUE"]?></div>
				<?if(strlen($arResult["PROPERTIES"]["SERVICE_OLD_PRICE"]["VALUE"]) > 0):?>
	            	<div class="old-price main2"><?=$arResult["PROPERTIES"]["SERVICE_OLD_PRICE"]["~VALUE"]?></div>
	            <?endif?>
			</div>

		<?endif?>

		<?if(isset($arResult["PROPERTIES"]["SERVICE_BUTTON_NAME"]["VALUE"]{0}) && isset($arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"]{0})):?>

			<div class="header-col wr-bttn">
				
				<?
		            if($arResult["PROPERTIES"]["SERVICE_BUTTON_FORM"]["VALUE"] > 0)
		                $form_id = $arResult["PROPERTIES"]["SERVICE_BUTTON_FORM"]["VALUE"];

		            if($arResult["PROPERTIES"]["SERVICE_BUTTON_TYPE"]["VALUE_XML_ID"] == "fast_order")
		            {
		                $form_id = $KRAKEN_TEMPLATE_ARRAY['FORMS']['VALUE_CATALOG'];

		                if($arResult["PROPERTIES"]["SERVICE_BUTTON_FORM"]["VALUE"] > 0)
		                    $form_id = $arResult["PROPERTIES"]["SERVICE_BUTTON_FORM"]["VALUE"];
		            }

		            $arClass = array();
		            $arClass=array(
		                "XML_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_TYPE"]["VALUE_XML_ID"],
		                "FORM_ID"=> $form_id,
		                "MODAL_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_MODAL"]["VALUE"],
		                "QUIZ_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_QUIZ"]["VALUE"],
		                "CUR_ELEMENT_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_CATALOG"]["VALUE"]
		            );
		            
		            $arAttr=array();
		            $arAttr=array(
		                "XML_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_TYPE"]["VALUE_XML_ID"],
		                "FORM_ID"=> $form_id,
		                "MODAL_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_MODAL"]["VALUE"],
		                "LINK"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_LINK"]["VALUE"],
		                "BLANK"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_BLANK"]["VALUE_XML_ID"],
		                "HEADER"=> $block_name,
		                "QUIZ_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_QUIZ"]["VALUE"],
		                "LAND_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_LAND"]["VALUE"],
		                "CUR_ELEMENT_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_CATALOG"]["VALUE"],
		                "OFFER_ID"=> $arResult["PROPERTIES"]["SERVICE_BUTTON_OFFER_ID"]["VALUE"]
		            );

		            $b_options = array(
                        "MAIN_COLOR" => "main-color",
                        "STYLE" => ""
                    );

                    if(strlen($arResult["PROPERTIES"]["SERVICE_BUTTON_BG_COLOR"]["VALUE"]))
                    {

                        $b_options = array(
                            "MAIN_COLOR" => "btn-bgcolor-custom",
                            "STYLE" => "background-color: ".$arResult["PROPERTIES"]["SERVICE_BUTTON_BG_COLOR"]["VALUE"].";"
                        );

                    }
	            ?>

	            <a 
	            	<?
	            		if(strlen($arResult["PROPERTIES"]["BUTTON_ONCLICK"]["VALUE"])>0) 
		                {
		                    $str_onclick = str_replace("'", "\"", $arResult["PROPERTIES"]["BUTTON_ONCLICK"]["VALUE"]);

		                    echo "onclick='".$str_onclick."'";

		                    $str_onclick = "";
		                }
	            	?>

	            	class="button-def
	            		<?=$KRAKEN_TEMPLATE_ARRAY["BTN_VIEW"]["VALUE"]?>
	            		big
	            		from-modal
						<?=$b_options["MAIN_COLOR"]?>
						<?=CKraken::buttonEditClass($arClass)?>
						more-modal-info
	            	"

	            	data-element-id="<?=$arResult["ID"]?>" data-element-type = "SRV"

	            	<?if(isset($b_options["STYLE"]{0})):?>
                        style = "<?=$b_options["STYLE"]?>"
                    <?endif;?>

                    <?=CKraken::buttonEditAttr($arAttr)?>
                >

            		<?if($arResult["PROPERTIES"]["TARIFF_BUTTON_TYPE"]["VALUE_XML_ID"] == "add_to_cart"):?>

                        <?
                        
                            $btn_name2 = GetMessage("KRAKEN_ONE_PAGE_CATALOG_BTN_ADDED_NAME");

                            if(strlen($KRAKEN_TEMPLATE_ARRAY["CART_BTN_ADDED_NAME"]["~VALUE"]) > 0)
                                $btn_name2 = $KRAKEN_TEMPLATE_ARRAY["CART_BTN_ADDED_NAME"]["~VALUE"];
                        ?>

                        <span class="first">
                           <?=$arResult["PROPERTIES"]["SERVICE_BUTTON_NAME"]["~VALUE"]?>
                        </span>

                        <span class="second">
                            <?=$btn_name2?>
                        </span> 

                    <?else:?>

                        <?=$arResult["PROPERTIES"]["SERVICE_BUTTON_NAME"]["~VALUE"]?>

                    <?endif;?>
	            </a>
			</div>

		<?endif?>
		
	</div>

	<div class="body">

		<?if(isset($arResult["PROPERTIES"]["SERVICE_DETAIL_TEXT"]["VALUE"]["TEXT"])):?>
			<div class="text-content">
				<?=$arResult["PROPERTIES"]["SERVICE_DETAIL_TEXT"]["~VALUE"]["TEXT"]?>
			</div>
		<?endif;?>

		<?if(isset($arResult["GALLERY"])):?>

			<?if(isset($arResult["PROPERTIES"]["SERVICE_GALLERY_TITLE"]["VALUE"]{0})):?>
	            <div class="section-title main1">
	                <?=$arResult["PROPERTIES"]["SERVICE_GALLERY_TITLE"]["~VALUE"]?>
	            </div>
            <?endif;?>



			<div class="gallery <?if($arResult["PROPERTIES"]["SERVICE_GALLERY_BORDER"]["VALUE"] == "Y"):?>border-on<?endif;?>">

	            <div class="row clearfix">

					<?foreach($arResult["GALLERY"] as $key => $arItem):?>

						<div class="col-sm-2 col-xs-4">
		                	<div class="wr-img">

		                    	<a data-gallery="service-detail-<?=$arResult["ID"]?>" href="<?=$arItem["BIG_SRC"]?>" title="<?=$arItem["TITLE"]?>" class="cursor-loop">

                        		<img class="img-responsive" src="<?=$arItem["SMALL_SRC"]?>" alt="<?=$arItem["ALT"]?>"/></a>
		                    </div>
		                </div>


		                <?if(($k+1) % 6 == 0):?>
		                	<span class="clearfix hidden-xs"></span>
		                <?endif;?>
		                <?if(($k+1) % 3 == 0):?>
		                	<span class="clearfix visible-xs"></span>
		                <?endif;?>

					<?endforeach;?>

				</div>

			</div>

		<?endif;?>
		

	</div>

</div>