<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
global $admin_active;
global $show_setting;
global $KRAKEN_TEMPLATE_ARRAY;
CKraken::includeCustomMessages();

$admin_active = $USER->isAdmin();
$show_setting = $KRAKEN_TEMPLATE_ARRAY["MODE_FAST_EDIT"]['VALUE'][0];


$terminations = Array();

$terminations[] = GetMessage("KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_CNT_1");
$terminations[] = GetMessage("KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_CNT_2");
$terminations[] = GetMessage("KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_CNT_3");
$terminations[] = GetMessage("KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_CNT_4");

?>


<div class="catalog-main-menu big-parent-colls">

    
    <?if($admin_active && $show_setting == "Y"):?>
        <div class="change-colls-info"><?=GetMessage('KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_BLINK_INFO_SAVE')?></div>
    <?endif;?>

    <div class="container">
        <div class="row">
 
            <div class="frame-wrap clearfix">
            
                <?$k = 0;?>
            
                <?foreach($arResult["SECTIONS"] as $arSection):?>
                
                    <?$size = array("width" => 280, "height" => 280);?>
                    <?$cols = 'col-lg-3 col-md-3 col-sm-6 col-xs-12 small';?>

                    <?if($arSection["UF_KRAKEN_CTLG_SIZE_ENUM"]["XML_ID"] == 'middle'):?>
                        <?$size = array("width" => 580, "height" => 280);?>
                        <?$cols = 'col-lg-6 col-md-6 col-sm-6 col-xs-12 middle';?>
                    <?endif;?>                       
                        
                    <?$size2 = array("width" => 400, "height" => 280);?>
                    <?$size3 = array("width" => 350, "height" => 350);?>
                
                    
                    <div class="<?=$cols?> parent-change-cools">

                        <div class="frame-outer<?if(strlen($arSection["UF_KRAKEN_CTLG_PRTXT"]) > 0 || !empty($arSection["SUB"])):?> elem-hover<?endif;?>">
                        
                            <div class="frame-inner elem-hover-height-more">
                            
                                <div class="frame light elem-hover-height">
                                
                                
                                    <?if($admin_active && $show_setting == 'Y'):?>
                                        
                                        <input type="hidden" class='colls_code' value="UF_KRAKEN_CTLG_SIZE" />
                                        <input type="hidden" class='colls_middle' value="<?=$arResult["SIZES"]["middle"]?>" />
                                        <input type="hidden" class='colls_small' value="<?=$arResult["SIZES"]["small"]?>" />
                                    
                                        <span class='change-colls' data-type='section' data-element-id='<?=$arSection["ID"]?>'></span>
                                    <?endif;?>
                                
    
                                    <!-- <a class="wrap-link" href="<?=$arSection["SECTION_PAGE_URL"]?>"></a> -->
                                    

                                    <?if($arSection["PICTURE"] > 0):?>
                                    
                                        <?$img = CFile::ResizeImageGet($arSection["PICTURE"], array('width'=>900, 'height'=>900), BX_RESIZE_IMAGE_EXACT, false);?>
                    
                                                                      
                                        <?$img = CFile::ResizeImageGet($arSection["PICTURE"], $size, BX_RESIZE_IMAGE_EXACT, true);?>  
                                        <img class="img hidden-xs hidden-sm lazyload" data-src="<?=$img["src"]?>"  />
                                        
                                        
                                        <?$img = CFile::ResizeImageGet($arSection["PICTURE"], $size2, BX_RESIZE_IMAGE_EXACT, true);?>  
                                        <img class="img visible-sm lazyload" data-src="<?=$img["src"]?>"  />
                                        
                                        
                                        <?$img = CFile::ResizeImageGet($arSection["PICTURE"], $size3, BX_RESIZE_IMAGE_EXACT, true);?>  
                                        <img class="img visible-xs lazyload" data-src="<?=$img["src"]?>"  />  
                                    
                                    <?endif;?>
                                            
                                    <div class="small-shadow"></div>
                                    <div class="frameshadow"></div>
                                    
                                    <div class="text">
                                    
                                        <div class="cont">
                                            <div class="name bold"><?=$arSection["NAME"]?></div>

                                            <?if($KRAKEN_TEMPLATE_ARRAY['CTLG_HIDE_COUNT_GOODS']['VALUE'][0] != "Y"):?>
                                                <div class="comment"><?=$arSection["ELEMENT_CNT"]?> <?=CKraken::getTermination($arSection["ELEMENT_CNT"], $terminations)?></div>

                                            <?endif;?>
                                        </div>
    
                                        <div class="button">
                                        
                                            <a class="button-def main-color <?=$KRAKEN_TEMPLATE_ARRAY["BTN_VIEW"]['VALUE']?>" href="<?=$arSection["SECTION_PAGE_URL"]?>">
                                                
                                                <?if(strlen($arSection["UF_KRAKEN_CTLG_BTN"]) > 0):?>
                                                    <?=$arSection["~UF_KRAKEN_CTLG_BTN"]?>
                                                <?else:?>
                                                    <?=GetMessage("KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_BUTTON")?>
                                                <?endif;?>
                                                
                                            </a>
                                            
                                        </div>
    
                                        
                                    </div>
                                    
                                    
                                    <?if($admin_active && $show_setting == 'Y'):?>   
                                    
                                        <div class="tool-settings">
                                        
                                            <a href="/bitrix/admin/iblock_section_edit.php?IBLOCK_ID=<?=$arParams["IBLOCK_ID"]?>&type=<?=$arParams["IBLOCK_TYPE_ID"]?>&ID=<?=$arSection["ID"]?>&lang=ru&find_section_section=0" class="tool-settings " data-toggle="tooltip" target="_blank" data-placement="right" title="" data-original-title="<?=GetMessage("KRAKEN_TEMPLATES_CATALOG_SECTION_LIST_SECTION_LIST_ADMIN_EDIT")?> &quot;<?=htmlspecialcharsEx($arSection["NAME"])?>&quot; "></a>
                                        
                                           
                                
                                        </div>
                                    
                                    <?endif;?>
                                    
           
                                </div>
                                
                                
                                <?if(strlen($arSection["UF_KRAKEN_CTLG_PRTXT"]) > 0 || !empty($arSection["SUB"])):?>
                                
                                    <div class="frame-desc-wrap elem-hover-show">
                                        
                                        <?if(!empty($arSection["SUB"])):?>
                                        
                                            <ul class="catalog-link clearfix">
                                                <?foreach($arSection["SUB"] as $arSub):?>
                                                    <li>
                                                        <a href="<?=$arSub["SECTION_PAGE_URL"]?>" title="<?=$arSub["NAME"]?>"><span class="bord-bot"><?=$arSub["~NAME"]?></span></a>
                                                    </li>
                                                <?endforeach;?>
                                            </ul>
                                        
                                        <?endif;?>
                                        
                                        <?if(strlen($arSection["UF_KRAKEN_CTLG_PRTXT"]) > 0):?>
                                        
                                            <div class="frame-desc-wrap-inner">
                                                <?=$arSection["~UF_KRAKEN_CTLG_PRTXT"]?>
                                            </div>
                                        
                                        <?endif;?>
        
                                    </div>
                                
                                <?endif;?>
    
                            </div>
    
                        </div>
                    </div>
                    
                    
                    <?if(($k+1) % 2 == 0 ):?>
                        <div class="clearfix visible-sm"></div>
                    <?endif;?>
                    
                    
                    <?$k++;?>
                    
                <?endforeach;?>

            </div>

        </div>
    </div>
    
</div>