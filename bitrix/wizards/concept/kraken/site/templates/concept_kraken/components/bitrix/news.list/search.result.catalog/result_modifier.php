<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $KRAKEN_TEMPLATE_ARRAY;
CKraken::includeCustomMessages();



if( !empty($arParams["SEARCH_RESULT"]['SECTIONS_ID']) )
{
	$SectList = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=> $arParams["SEARCH_RESULT"]['SECTIONS_ID'], "ACTIVE"=>"Y"), false, array("ID", "NAME", "SECTION_PAGE_URL", "DETAIL_PICTURE", "UF_KRAKEN_MENU_PICT"));
	while ($SectListGet = $SectList->GetNext())
	{
	    $arResult["SECTIONS"][]=$SectListGet;
	}

}



if( !empty($arParams["SEARCH_RESULT"]['ITEMS_ID']) )
{
    $arResult["PAGE_ELEMENT_COUNT"] = 24;

    if($arParams["VIEW"] == "short")
        $arResult["PAGE_ELEMENT_COUNT"] = 4;

    $arResult["SEARCH_ITEMS_ID"] = array();

    if($arParams["VIEW"] == "short")
    {
        foreach ($arParams["SEARCH_RESULT"]['ITEMS_ID'] as $key => $value)
        {
            if($key >= 4)
                break;

            $arResult["SEARCH_ITEMS_ID"][] = $value;
        }
    }
    else
        $arResult["SEARCH_ITEMS_ID"] = $arParams["SEARCH_RESULT"]['ITEMS_ID'];
}