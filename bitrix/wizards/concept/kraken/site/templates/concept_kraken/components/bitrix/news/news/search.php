<?
$arSearchCodes = Array();

$arSearchCodes["CATALOG"] = "concept_kraken_site_catalog";
$arSearchCodes["BLOG"] = "concept_kraken_site_history";
$arSearchCodes["NEWS"] = "concept_kraken_site_news";
$arSearchCodes["ACTIONS"] = "concept_kraken_site_action";


$code = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "CODE");
$code = str_replace("_".SITE_ID, "", $code);


$key = array_search($code, $arSearchCodes);
?>


<?if( $KRAKEN_TEMPLATE_ARRAY['SEARCH']['ACTIVE']['VALUE']['ACTIVE'] == "Y" && $KRAKEN_TEMPLATE_ARRAY['SEARCH']['SEARCH_SHOW_IN']['VALUE'][$key] == "Y" ):?>

	<div class="search-block col-xs-12 clearfix">
		<?$APPLICATION->IncludeComponent("concept:kraken.search.line", "", Array("START_PAGE"=>ToLower($key)));?>
	</div>

<?endif;?>