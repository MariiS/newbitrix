<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


function page404()
{
	global $APPLICATION;

	if (!defined("ERROR_404"))
        define("ERROR_404", "Y");

    \CHTTP::setStatus("404 Not Found");
       
    if ($APPLICATION->RestartWorkarea()) {
       require(\Bitrix\Main\Application::getDocumentRoot()."/404.php");
       die();
    }
}
?>


<?
global $KRAKEN_TEMPLATE_ARRAY, $DB;
$arFields = array();
$arProps = array();
$arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE_DATE" => "", "ACTIVE"=>"Y", "CODE"=>$arResult["VARIABLES"]["ELEMENT_CODE"]);

if($arParams["TYPE"] == "ACT")
	$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE_DATE" => "", "<=DATE_ACTIVE_FROM" => date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime()), "ACTIVE"=>"Y", "CODE"=>$arResult["VARIABLES"]["ELEMENT_CODE"]);

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

if($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
}


if(empty($arFields))
	page404();


$GLOBALS["KRAKEN_CURRENT_DIR"] = "element";
$GLOBALS["KRAKEN_CURRENT_SECTION_ID"] = $arFields["IBLOCK_SECTION_ID"];
$GLOBALS["KRAKEN_ELEMENT_ID"] = $arFields["ID"];
$GLOBALS["KRAKEN_CURRENT_TMPL"] = $arProps["ITEM_TEMPLATE"]["VALUE_XML_ID"];


if(strlen($arProps["ITEM_TEMPLATE"]["VALUE_XML_ID"]) <= 0)
    $arProps["ITEM_TEMPLATE"]["VALUE_XML_ID"] = "default";


$header_back = "";

if($arProps["HEADER_PICTURE"]["VALUE"] > 0)
{
    $img = CFile::ResizeImageGet($arProps["HEADER_PICTURE"]["VALUE"], array('width'=>2560, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, false);  
    $header_back = $img["src"];   
}    
    

$arResult["BANNERS_RIGHT"] = Array();

if(!empty($arProps["BANNERS_RIGHT"]["VALUE"]) && $arProps["BANNERS_RIGHT_TYPE"]["VALUE_XML_ID"] == "own")
    $arResult["BANNERS_RIGHT"] = $arProps["BANNERS_RIGHT"]["VALUE"];



if($arParams["TYPE"] == "ACT")
{
    if($arProps["HEADER_PICTURE"]["VALUE"] <= 0)
    {
        $bg_pic = CFile::ResizeImageGet($KRAKEN_TEMPLATE_ARRAY[$arParams["TYPE"]."_BG_PIC"]["VALUE"], array('width'=>1600, 'height'=>1200), BX_RESIZE_IMAGE_PROPORTIONAL, false);
        $header_back = $bg_pic["src"];
    }
    
    
}
else
{
    $parent_section_id = $arFields["IBLOCK_SECTION_ID"];

    while($parent_section_id != 0)
    {
        $arSelect = Array("ID", "PICTURE", "IBLOCK_SECTION_ID", "UF_*");
        $arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "GLOBAL_ACTIVE"=>"Y", "ID"=>$parent_section_id);
        $db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);

        $activePage = false;
        
        while($ar_res = $db_list->GetNext())
        {
            if($arProps["BANNERS_RIGHT_TYPE"]["VALUE_XML_ID"] == "parent")
            {
                if(empty($arResult["BANNERS_RIGHT"]))
                    $arResult["BANNERS_RIGHT"] = $ar_res["UF_KRAKEN_".$arParams["TYPE"]."_BNNRS"];
            }
            
            
            if(strlen($header_back) <= 0)
            {
           
                if($ar_res["PICTURE"] > 0)
                {
                    $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>3000, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, false);  
                    $header_back = $img["src"];   
                }
            }


            $parent_section_id = $ar_res["IBLOCK_SECTION_ID"];

            if( $activePage == false )
            	$activePage = true;
            
        }

        if(!$activePage)
        	page404();
        
    }
}
?> 

<?if($arProps["ITEM_TEMPLATE"]["VALUE_XML_ID"] == "default"):?>
    <?$GLOBALS["IS_CONSTRUCTOR"] = false;?>


    <?if($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES_MODE"]["VALUE"] == "custom" && isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]{0})):?>
        <style>
            @media (max-width: 767.98px){
                div.header-page{
                    background-image: url('<?=$KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]?>') !important;
                }
            }
        </style>
    <?endif;?>
    
	<div class="header-page new-first-block cover parent-scroll-down <?=$KRAKEN_TEMPLATE_ARRAY["HEAD_TONE"]["VALUE"]?> kraken-firsttype-<?=$KRAKEN_TEMPLATE_ARRAY["MENU_TYPE"]["VALUE"]?> <?=($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES_MODE"]["VALUE"] == "custom" && !isset($KRAKEN_TEMPLATE_ARRAY["ITEMS"]["HEAD_BG_XS_FOR_PAGES"]["SRC"]{0})) ? "def-bg-xs" : "";?>" <?if(strlen($header_back) > 0):?>style="background-image: url('<?=$header_back?>');"<?endif;?>>
	    <div class="shadow"></div>
	    <div class="top-shadow"></div>

	    <div class="container">
	        <div class="row">


	            <div class="new-first-block-table clearfix">
	                                    
	                <div class="new-first-block-cell text-part col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                
	                    <div class="head">

	                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
	                                "COMPONENT_TEMPLATE" => ".default",
	                        		"START_FROM" => "0",
	                        		"PATH" => "",
	                        		"SITE_ID" => SITE_ID,
	                        		"COMPOSITE_FRAME_MODE" => "A",
	                        		"COMPOSITE_FRAME_TYPE" => "AUTO",
	                        	),
	                        	$component
	                        );?>
	                        
	                        <div class="title main1"><h1><?$APPLICATION->ShowTitle(false);?></h1></div>
	                                                                        
	                    </div>
	                    
	                </div>

	                <div class="new-first-block-cell col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">

	                   <div class="wrap-scroll-down hidden-xs">
	                        <div class="down-scrollBig">
	                            <i class="fa fa-chevron-down"></i>
	                        </div>
	                    </div>
	                    
	                </div>

	            </div>



	        </div>

	    </div>
	                                        
	</div>

	<div class="news-list-wrap page_pad_bot detail">

        <div class="container">
            <div class="row clearfix">
            
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs col-lg-push-9 col-md-push-9 col-sm-push-9 col-xs-push-9">
                    <div class="menu-navigation static on-scroll" id="navigation">

                        <div class="menu-navigation-wrap">

                            <div class="menu-navigation-inner">

                                <input type="hidden" id="detail-page" name="detail-page" value="<?=$arParams["IBLOCK_ID"]?>">

                                <?
                                    $tmpl = "";

                                    if($arParams["TYPE"] == "ACT")
                                        $tmpl = "_act";
                                    
                                ?>
                                    
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:catalog.section.list",
                                        "mainsections".$tmpl,
                                        array(
                                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                            "SECTION_ID" => $arFields["IBLOCK_SECTION_ID"],
                                            "SECTION_CODE" => "",
                                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                            "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                                            "TOP_DEPTH" => 1,
                                            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                                            "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                                            "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                                            "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "TYPE"=> $arParams["TYPE"],
                                            "ELEMENT" => "Y"
                                        ),
                                        $component,
                                        array("HIDE_ICONS" => "Y")
                                    );                  
                                ?>

   
                                <?if($arParams["TYPE"] == "ACT"):?>
                                    <?$GLOBALS['arNewsfilter'] = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "!ID" => $arFields["ID"], "ACTIVE_DATE" => "", "ACTIVE" => "Y", "<=DATE_ACTIVE_FROM" => date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime()));?>

                                <?else:?>
                                    <?$GLOBALS['arNewsfilter'] = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "!ID" => $arFields["ID"], "ACTIVE" => "Y", 'SECTION_ID' => $arFields["IBLOCK_SECTION_ID"]);?>

                                <?endif;?>

                                
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:news.list",
                                    "other_news",
                                    Array(
                                        "COMPONENT_TEMPLATE" => "other_news",
                                        "ACTIVE_DATE_FORMAT" => "FULL",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_ADDITIONAL" => "",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_TYPE" => "N",
                                        "CHECK_DATES" => "Y",
                                        "COMPOSITE_FRAME_MODE" => "A",
                                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                                        "DETAIL_URL" => "",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "FIELD_CODE" => array("PREVIEW_PICTURE","ACTIVE"),
                                        "FILTER_NAME" => "arNewsfilter",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "INCLUDE_SUBSECTIONS" => "N",
                                        "MESSAGE_404" => "",
                                        "NEWS_COUNT" => "2",
                                        "PAGER_BASE_LINK_ENABLE" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => ".default",
                                        "PAGER_TITLE" => "",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "PROPERTY_CODE" => array("",""),
                                        "SET_BROWSER_TITLE" => "N",
                                        "SET_LAST_MODIFIED" => "N",
                                        "SET_META_DESCRIPTION" => "N",
                                        "SET_META_KEYWORDS" => "N",
                                        "SET_STATUS_404" => "N",
                                        "SET_TITLE" => "N",
                                        "SHOW_404" => "N",
                                        "SORT_BY1" => "rand",
                                        "SORT_BY2" => "",
                                        "SORT_ORDER1" => "ASC",
                                        "SORT_ORDER2" => "ASC",
                                        "STRICT_SECTION_CHECK" => "N"
                                    ),
                                    
                                    $component
                                    );?>

                                <?if(!empty($arResult["BANNERS_RIGHT"]) > 0):?>
                                    
                                    <?$GLOBALS["arrBannersFilter"]["ID"] = $arResult["BANNERS_RIGHT"];?>
                                    <?CKraken::getIblockIDs(array("CODES"=>array("concept_kraken_site_banners_".SITE_ID),"SITE_ID"=>SITE_ID));?>
                                    
                                    <?$APPLICATION->IncludeComponent(
                                    	"bitrix:news.list", 
                                    	"banners-left", 
                                    	array(
                                    		"COMPONENT_TEMPLATE" => "banners-left",
                                    		"IBLOCK_TYPE" => $KRAKEN_TEMPLATE_ARRAY['BANNERS']["IBLOCK_TYPE"],
                                            "IBLOCK_ID" => $KRAKEN_TEMPLATE_ARRAY['BANNERS']["IBLOCK_ID"],
                                    		"NEWS_COUNT" => "20",
                                    		"SORT_BY1" => "SORT",
                                    		"SORT_ORDER1" => "ASC",
                                    		"SORT_BY2" => "SORT",
                                    		"SORT_ORDER2" => "ASC",
                                    		"FILTER_NAME" => "arrBannersFilter",
                                    		"FIELD_CODE" => array(
                                    			0 => "DETAIL_PICTURE",
                                    			1 => "PREVIEW_PICTURE",
                                    		),
                                    		"PROPERTY_CODE" => array(
                                    			0 => "",
                                    			1 => "BANNER_BTN_TYPE",
                                    			2 => "BANNER_ACTION_ALL_WRAP",
                                    			3 => "BANNER_USER_BG_COLOR",
                                    			4 => "BANNER_UPTITLE",
                                    			5 => "BANNER_BTN_NAME",
                                    			6 => "BANNER_TITLE",
                                    			7 => "BANNER_BTN_BLANK",
                                    			8 => "BANNER_BORDER",
                                    			9 => "BANNER_DESC",
                                    			10 => "BANNER_TEXT",
                                    			11 => "BANNER_LINK",
                                    			12 => "BANNER_COLOR_TEXT",
                                    			13 => "",
                                    		),
                                    		"CHECK_DATES" => "Y",
                                    		"DETAIL_URL" => "",
                                    		"AJAX_MODE" => "N",
                                    		"AJAX_OPTION_JUMP" => "N",
                                    		"AJAX_OPTION_STYLE" => "Y",
                                    		"AJAX_OPTION_HISTORY" => "N",
                                    		"AJAX_OPTION_ADDITIONAL" => "",
                                    		"CACHE_TYPE" => "A",
                                    		"CACHE_TIME" => "36000000",
                                    		"CACHE_FILTER" => "Y",
                                    		"CACHE_GROUPS" => "Y",
                                    		"PREVIEW_TRUNCATE_LEN" => "",
                                    		"ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    		"SET_TITLE" => "N",
                                    		"SET_BROWSER_TITLE" => "N",
                                    		"SET_META_KEYWORDS" => "N",
                                    		"SET_META_DESCRIPTION" => "N",
                                    		"SET_LAST_MODIFIED" => "N",
                                    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    		"ADD_SECTIONS_CHAIN" => "N",
                                    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    		"PARENT_SECTION" => "",
                                    		"PARENT_SECTION_CODE" => "",
                                    		"INCLUDE_SUBSECTIONS" => "N",
                                    		"STRICT_SECTION_CHECK" => "N",
                                    		"DISPLAY_DATE" => "N",
                                    		"DISPLAY_NAME" => "N",
                                    		"DISPLAY_PICTURE" => "N",
                                    		"DISPLAY_PREVIEW_TEXT" => "N",
                                    		"COMPOSITE_FRAME_MODE" => "A",
                                    		"COMPOSITE_FRAME_TYPE" => "AUTO",
                                    		"PAGER_TEMPLATE" => ".default",
                                    		"DISPLAY_TOP_PAGER" => "N",
                                    		"DISPLAY_BOTTOM_PAGER" => "N",
                                    		"PAGER_TITLE" => "",
                                    		"PAGER_SHOW_ALWAYS" => "N",
                                    		"PAGER_DESC_NUMBERING" => "N",
                                    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    		"PAGER_SHOW_ALL" => "N",
                                    		"PAGER_BASE_LINK_ENABLE" => "N",
                                    		"SET_STATUS_404" => "N",
                                    		"SHOW_404" => "N",
                                    		"MESSAGE_404" => ""
                                    	),
                                    	$component
                                    );?>
                                
                                <?endif;?>
                                
                            </div>

                        </div>


                    </div>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 content-inner col-lg-pull-3 col-md-pull-3 col-sm-pull-0 col-xs-pull-0 page">
                    
                    <div class="block small padding-on">

    					<?$ElementID = $APPLICATION->IncludeComponent(
    						"bitrix:news.detail",
    						"",
    						Array(
    							"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
    							"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
    							"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
    							"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
    							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    							"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
    							"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
    							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
    							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
    							"META_KEYWORDS" => $arParams["META_KEYWORDS"],
    							"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
    							"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
    							"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
    							"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
    							"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
    							"SET_TITLE" => $arParams["SET_TITLE"],
    							"MESSAGE_404" => $arParams["MESSAGE_404"],
    							"SET_STATUS_404" => $arParams["SET_STATUS_404"],
    							"SHOW_404" => $arParams["SHOW_404"],
    							"FILE_404" => $arParams["FILE_404"],
    							"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
    							"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
    							"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
    							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    							"CACHE_TIME" => $arParams["CACHE_TIME"],
    							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    							"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
    							"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
    							"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
    							"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
    							"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
    							"PAGER_SHOW_ALWAYS" => "N",
    							"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
    							"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
    							"CHECK_DATES" => $arParams["CHECK_DATES"],
    							"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
    							"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    							"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
    							"USE_SHARE" => $arParams["USE_SHARE"],
    							"SHARE_HIDE" => $arParams["SHARE_HIDE"],
    							"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
    							"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
    							"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
    							"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
    							"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
    							'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
    						),
    						$component
    					);?>

                    </div>


				</div>


			</div>
		</div>

	</div>

<?endif;?>

<?if($arProps["ITEM_TEMPLATE"]["VALUE_XML_ID"] == "landing"):?>
    
    <?if($arProps["CHOOSE_LANDING"]["VALUE"] > 0):?>
   
        <?
        $ar_res = array();
        $arFilter = Array("ID" => $arProps["CHOOSE_LANDING"]["VALUE"]);
        $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
        $ar_res = $db_list->GetNext();
        ?> 

        <?if($ar_res["ACTIVE"] == "Y"):?>

            <?$GLOBALS["IS_CONSTRUCTOR"] = true;?>
    
	        <?$section = $APPLICATION->IncludeComponent(
	        	"concept:kraken.one.page", 
	        	"", 
	        	array(
	        		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	        		"CACHE_TIME" => $arParams["CACHE_TIME"],
	        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	        		"CHECK_DATES" => $arParams["CHECK_DATES"],
	        		"IBLOCK_ID" => $ar_res["IBLOCK_ID"],
	        		"IBLOCK_TYPE" => $ar_res["IBLOCK_TYPE_ID"],
	        		"PARENT_SECTION" => $ar_res["ID"],
	        		"SET_TITLE" => $arParams["SET_TITLE"],
	        		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
	        		"MESSAGE_404" => $arParams["MESSAGE_404"],
	        		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
	        		"SHOW_404" => $arParams["SHOW_404"],
	        		"FILE_404" => $arParams["FILE_404"],
	        		"COMPONENT_TEMPLATE" => ""
	        	),
	        	$component
	        );?>
	    
	        <?$GLOBALS["KRAKEN_CURRENT_SECTION_ID"] = $section;?>

        <?else:?>

        	<?
        	if (!defined("ERROR_404"))
			   define("ERROR_404", "Y");

				\CHTTP::setStatus("404 Not Found");
				   
				if ($APPLICATION->RestartWorkarea()) {
				   require(\Bitrix\Main\Application::getDocumentRoot()."/404.php");
				   die();
				}
	

        	?>

        <?endif;?>
    
    <?endif;?>

<?endif;?>

