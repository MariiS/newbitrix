<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CKraken::includeCustomMessages();
?>

<?if($arParams["ELEMENT"] != "Y"):?>

    <?if(!empty($arResult["MAIN"]["SECTIONS"])):?>

        <ul class="nav">

            <li class="main">
                <a href="<?=$arResult["MAIN"]["SECTION_MAIN"]?>"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_CATALOG_SECTION_LIST_MAINSECTIONS_".$arParams["TYPE"]."_BACK")?></a>
            </li> 
            
            <?foreach($arResult["MAIN"]["SECTIONS"] as $arSection):?>
            
                <li data-id="<?=$arSection["ID"]?>">
                    <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=strip_tags(html_entity_decode($arSection["NAME"]))?></a>
                </li>
            
            <?endforeach;?>
            
        </ul>

    <?endif;?>

<?else:?>

    <ul class="new-detail">

        <li class="back"><a href="<?=$arResult["MAIN"]["SECTION_BACK"]?>"><?=GetMessage("KRAKEN_TEMPLATES_NEWS_CATALOG_SECTION_LIST_MAINSECTIONS_SECTION_BACK_".$arParams["TYPE"]);?></a></li>

    </ul>
    
<?endif;?>