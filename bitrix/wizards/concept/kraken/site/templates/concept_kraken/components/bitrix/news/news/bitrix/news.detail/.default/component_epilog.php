<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
global $KRAKEN_TEMPLATE_ARRAY, $DB_kraken;
CKraken::includeCustomMessages();
?>

    <?if(strlen($arResult["PROPERTIES"]["NEWS_TITLE_CATALOG"]["~VALUE"])>0):?>

        <div class="text-content">
            <h2><?=$arResult["PROPERTIES"]["NEWS_TITLE_CATALOG"]["~VALUE"]?></h2>
        </div>

    <?endif;?>

<?
    if(!empty($arResult["PROPERTIES"]["CATALOG"]["VALUE"]))
    {
        CKraken::getIblockIDs(
            array(
                "CODES" => array("concept_kraken_site_catalog_".SITE_ID), 
                "SITE_ID"=>SITE_ID
            )
        );

        $GLOBALS['arFilterNewsDetailCatalog'] = array("ID" => $arResult["PROPERTIES"]["CATALOG"]["VALUE"]);

        $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        "main",
        array(
            "FILTER_NAME" => "arFilterNewsDetailCatalog",
            "IBLOCK_TYPE" => $KRAKEN_TEMPLATE_ARRAY['CATALOG']["IBLOCK_TYPE"],
            "IBLOCK_ID" => $KRAKEN_TEMPLATE_ARRAY['CATALOG']["IBLOCK_ID"],
            "SIDE_MENU"=> true,
            "MAIN_BLOCK_ID"=>$arResult["ID"],
            
        ),
        $component
        );
    }

?>



<?\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area");?>


<?

if(strlen($GLOBALS["TITLE"]) <= 0)
    $GLOBALS["TITLE"] = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"];

if(strlen($GLOBALS["KEYWORDS"]) <= 0)
    $GLOBALS["KEYWORDS"] = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"];

if(strlen($GLOBALS["DESCRIPTION"]) <= 0)
    $GLOBALS["DESCRIPTION"] = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"];

if(strlen($GLOBALS["H1"]) <= 0)
    $GLOBALS["H1"] = $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
    
?>
<script>
    
    $(document).ready(function($) {
        
        <?foreach ($arResult["SECTIONS_ID"] as $value):?>

            $(".section-menu-id-<?=$value?>").addClass('selected');
            
        <?endforeach;?>

    });

</script>
<?\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area");?>