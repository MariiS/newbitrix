<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $KRAKEN_TEMPLATE_ARRAY, $DB;


	
	$section_id = $arResult["IBLOCK_SECTION_ID"];

	$arResult["SECTIONS_ID"] = array();

	while($section_id != 0)
	{
	    $res = CIBlockSection::GetByID($section_id);
	    if($ar_res = $res->GetNext())
	        $arResult["SECTIONS_ID"][] = $ar_res["ID"];

	    $section_id = $ar_res["IBLOCK_SECTION_ID"];
	}

	$arFilter = Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "ID" => $arResult["ID"]);
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false);

    while($ob = $res->GetNextElement())
    {
        $arResult["PROPERTIES"] = $ob->GetProperties();
    }

	$arNews = array();

	if(!empty($arResult["PROPERTIES"]["NEWS_ELEMENTS_NEWS"]["VALUE"]))
	    $arNews = array_merge($arNews, $arResult["PROPERTIES"]["NEWS_ELEMENTS_NEWS"]["VALUE"]);

	if(!empty($arResult["PROPERTIES"]["NEWS_ELEMENTS_BLOG"]["VALUE"]))
	    $arNews = array_merge($arNews, $arResult["PROPERTIES"]["NEWS_ELEMENTS_BLOG"]["VALUE"]);

	if(!empty($arResult["PROPERTIES"]["NEWS_ELEMENTS_ACTION"]["VALUE"]))
	    $arNews = array_merge($arNews, $arResult["PROPERTIES"]["NEWS_ELEMENTS_ACTION"]["VALUE"]);

	$arResult["PARENT_ON"] = "N";

	if(!empty($arNews))
	{

		$filterOr = array(
            "LOGIC" => "OR",
            array("ACTIVE_DATE"=>"Y"),
            array("<=DATE_ACTIVE_FROM" => date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime())) 
        );
        
		$arSort = Array("ACTIVE_FROM" => "DESC", "ID" => "DESC");
		$arFilter = Array("ID"=> $arNews, "ACTIVE"=>"Y", $filterOr);
		$res = CIBlockElement::GetList($arSort, $arFilter);

		while($ob = $res->GetNextElement())
		{
		    $arElement = Array();
		    
		    $arElement = $ob->GetFields();  
		    $arElement["PROPERTIES"] = $ob->GetProperties();

		    $arResult["ELEMENTS"][] = $arElement;

		    if(strlen($arElement["IBLOCK_SECTION_ID"]) > 0 || $arElement["IBLOCK_CODE"] == "concept_kraken_site_action_".SITE_ID)
            $arResult["PARENT_ON"] = "Y";
		   
		}



        $code = array('concept_kraken_site_history_'.SITE_ID, 'concept_kraken_site_news_'.SITE_ID,'concept_kraken_site_action_'.SITE_ID);

		$SectList = CIBlockSection::GetList(array(), array("IBLOCK_CODE"=>$code, "ACTIVE"=>"Y"), false, array("ID","NAME","SECTION_PAGE_URL"));
		while ($SectListGet = $SectList->GetNext())
		{
		    $arResult["BNA"][$SectListGet["ID"]]=$SectListGet;
		}

	}

	if(!empty($arResult["PROPERTIES"]["NEWS_GALLERY"]["VALUE"]))
	{
		$arResult["GALLERY"] = array();

		$arWaterMark = Array();

        if($arResult["PROPERTIES"]["WATERMARK"]["VALUE"] > 0)
        {

            $arWaterMark = Array(
                array(
                    "name" => "watermark",
                    "position" => "center",
                    "type" => "image",
                    "size" => "big",
                    "file" => $_SERVER["DOCUMENT_ROOT"].CFile::GetPath($arResult["PROPERTIES"]["WATERMARK"]["VALUE"]), 
                    "fill" => "exact",
                )
            );
        }

		foreach($arResult["PROPERTIES"]["NEWS_GALLERY"]["VALUE"] as $k => $arImages)
		{
			$img_lg = CFile::ResizeImageGet($arImages, array('width'=>1600, 'height'=>1600), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false, $arWaterMark, false, 85);

			$arResult["GALLERY"][$k]["SRC_LG"] = $img_lg['src'];

			$img_xs = CFile::ResizeImageGet($arImages, array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_EXACT, false, false, false, 85);


			$arResult["GALLERY"][$k]["SRC_XS"] = $img_xs['src'];


			$arResult["GALLERY"][$k]["DESC"] = (isset($arResult["PROPERTIES"]["NEWS_GALLERY"]["~DESCRIPTION"][$k]{0}))? $arResult["PROPERTIES"]["NEWS_GALLERY"]["~DESCRIPTION"][$k]:"";
		}

		unset($img_big, $img_min);
	}


	if(!empty($arResult["PROPERTIES"]["FILES"]["VALUE"]))
	{
		$arResult["FILES"]=array();

		foreach($arResult["PROPERTIES"]["FILES"]["VALUE"] as $k=>$file)
		{
			$arResult["FILES"][$k]["PATH"] = CFile::GetPath($file);
			$arResult["FILES"][$k]["DESC"] = (isset($arResult["PROPERTIES"]["FILES_DESC"]["VALUE"][$k]{0}))? $arResult["PROPERTIES"]["FILES_DESC"]["~VALUE"][$k]:"";
			$arResult["FILES"][$k]["SUB_DESC"] = (isset($arResult["PROPERTIES"]["FILES_DESC"]["DESCRIPTION"][$k]{0}))? $arResult["PROPERTIES"]["FILES_DESC"]["~DESCRIPTION"][$k]:"";
		}
	}


	/*$host = $_SERVER["HTTP_HOST"];

	if(strlen($_SERVER["HTTP_HOST"])<=0)
	    $host = $_SERVER["SERVER_NAME"];

	$host = explode(":", $host);
	$host = $host[0];*/

	$host = CKrakenHost::getHost($_SERVER);


	   
	$url = $host.$_SERVER["REQUEST_URI"];


	$arResult["SHARE_TITLE"] = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"];
	if(strlen($KRAKEN_TEMPLATE_ARRAY["OG_TITLE"])>0)
		$arResult["SHARE_TITLE"] = $KRAKEN_TEMPLATE_ARRAY["OG_TITLE"];


	$arResult["SHARE_DESCRIPTION"] = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"];
	if(strlen($KRAKEN_TEMPLATE_ARRAY["OG_DESCRIPTION"])>0)
		$arResult["SHARE_DESCRIPTION"] = $KRAKEN_TEMPLATE_ARRAY["OG_DESCRIPTION"];

	$arResult["SHARE_DESCRIPTION"] = str_replace(array("\r\n", "\r", "\n", "<br>", "<br/>"), array(" ", " ", " "," "," "), $arResult["SHARE_DESCRIPTION"]);


	$arResult["SHARE_IMG"] = $host.CFile::GetPath($arResult["PREVIEW_PICTURE"]["ID"]);
	if(strlen($KRAKEN_TEMPLATE_ARRAY["OG_IMAGE"])>0)
		$arResult["SHARE_IMG"] = $host.CFile::GetPath($KRAKEN_TEMPLATE_ARRAY["OG_IMAGE"]);


	$arResult["SHARE_URL"] = $url;
	if(strlen($KRAKEN_TEMPLATE_ARRAY["OG_URL"])>0)
		$arResult["SHARE_URL"] = $KRAKEN_TEMPLATE_ARRAY["OG_URL"];


// $cp = $this->__component; // объект компонента

// if (is_object($cp))
// {
//     $cp->arResult['SECTIONS_ID'] = $arResult["SECTIONS_ID"];
//     $cp->SetResultCacheKeys(array('SECTIONS_ID'));

//     $arResult['SECTIONS_ID'] = $cp->arResult['SECTIONS_ID'];
// }


$this->__component->arResultCacheKeys = array_merge($this->__component->arResultCacheKeys, array("PROPERTIES", "SECTIONS_ID"));
?>