<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<?

if($arParams["PARENT_SECTION"] > 0)
{

    $arSelect = Array("ID", "UF_*");
    $arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], "ID" => $arParams["PARENT_SECTION"]);
    $db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
    $ar_result = $db_list->GetNext();


    if(strlen($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TXT_P"]) > 0)
    {
        $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TXT_P_ENUM"] = CUserFieldEnum::GetList(array(), array(
            "ID" => $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TXT_P"],
        ))->GetNext();


    }

    if(strlen($ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TXT_P_ENUM"]["XML_ID"]) <= 0)
        $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TXT_P_ENUM"]["XML_ID"] = "short";

    $arResult = array_merge($arResult, $ar_result);

}




foreach($arResult["ITEMS"] as $key=>$arItem)
{

    $arFilter = Array("IBLOCK_ID"=>$arItem["IBLOCK_ID"], "ID" => $arItem["ID"]);
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false);

    while($ob = $res->GetNextElement())
    {
        $arResult["ITEMS"][$key]["PROPERTIES"] = $ob->GetProperties();
    }
}



if($arParams["HIDE_SECTIONS"] != "Y")
{
    foreach($arResult["ITEMS"] as $arItem)
    {
        if(strlen($arItem["IBLOCK_SECTION_ID"])>0 || $arItem["IBLOCK_CODE"] != "concept_kraken_site_action_".SITE_ID)
            $arResult["PARENT_ON"] = "Y";
    }

    $code = array('concept_kraken_site_history_'.SITE_ID, 'concept_kraken_site_news_'.SITE_ID,'concept_kraken_site_action_'.SITE_ID);

    $SectList = CIBlockSection::GetList(array(), array("IBLOCK_CODE"=>$code, "ACTIVE"=>"Y"), false, array("ID","NAME","SECTION_PAGE_URL"));
    while ($SectListGet = $SectList->GetNext())
    {
        $arResult["BNA"][$SectListGet["ID"]]=$SectListGet;
    } 

}



$cp = $this->__component;
 
if (is_object($cp))
{
    $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_TITLE'] = $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_TITLE"];
    $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_KWORD'] = $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_KWORD"];
    $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_DSCR'] = $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_DSCR"];
    $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_H1'] = $ar_result["UF_KRAKEN_".$arParams["TYPE"]."_H1"];
    
    $cp->SetResultCacheKeys(array('UF_KRAKEN_'.$arParams["TYPE"].'_TITLE', 'UF_KRAKEN_'.$arParams["TYPE"].'_KWORD', 'UF_KRAKEN_'.$arParams["TYPE"].'_DSCR', 'UF_KRAKEN_'.$arParams["TYPE"].'_H1'));
    
    $arResult['UF_KRAKEN_'.$arParams["TYPE"].'_TITLE'] = $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_TITLE'];
    $arResult['UF_KRAKEN_'.$arParams["TYPE"].'_KWORD'] = $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_KWORD'];
    $arResult['UF_KRAKEN_'.$arParams["TYPE"].'_DSCR'] = $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_DSCR'];
    $arResult['UF_KRAKEN_'.$arParams["TYPE"].'_H1'] = $cp->arResult['UF_KRAKEN_'.$arParams["TYPE"].'_H1'];
}
